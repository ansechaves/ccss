import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtro',
})
export class FiltroPipe implements PipeTransform {
  
  transform(nombramientos: any, buscar: any, adicional: string): any {
    if (buscar === undefined) return nombramientos;
    else {
      return nombramientos.filter(function (p) {
        if (adicional === 'Nombre del puesto'.toString()) return p.nombre_puesto.toString().includes(buscar);
        else if (adicional === 'Nombre del funcionario'.toString()) return p.nombre_funcionario.toLowerCase().includes(buscar.toLowerCase());
        else return p;
      });
    }
  }
}



