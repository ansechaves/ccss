import { Component, OnInit } from '@angular/core';
import {NgbModal, NgbModalOptions} from '@ng-bootstrap/ng-bootstrap';
import {ModificarComponent} from '../modificar/modificar.component';
import {Nombramiento} from '../../models/Nombramiento';
import {NombramientosService} from '../nombramientos.service';
import {CrearNombramientoComponent} from '../crear-nombramiento/crear-nombramiento.component';
import {ToasterManagerService} from '../../../@core/toast/toaster-manager.service';
import 'style-loader!angular2-toaster/toaster.css';
import {configToasterManager} from '../../../@core/toast/config';

// services
import { AuthService } from '../../../auth/auth.service';
import { GlobalesService } from './../../../componentes-globales/globales.service';

@Component({
  selector: 'ngx-nombramiento-mostrar',
  templateUrl: './nombramiento-mostrar.component.html',
  styleUrls: ['./nombramiento-mostrar.component.scss'],
})
export class NombramientoMostrarComponent implements OnInit {
  public servicios: Nombramiento;
  public nombramientos: Nombramiento[];
  public nombramiento_e: Nombramiento;
  public variablita: number;
  public tipoPermisoUsuario: String;
  public idServicio: number;
  // Opciones de busqueda en la barra de buscar
  opciones = ['Nombre del puesto', 'Nombre del funcionario'];
  // Opción por defecto
  opcionSeleccionada: any = 'Nombre del funcionario';
  modalOption: NgbModalOptions = {};

  constructor(private modalService: NgbModal, public nomService: NombramientosService,
              public toasterManagerService: ToasterManagerService, private authService: AuthService,
              private global: GlobalesService ) { }
  config= configToasterManager;

  ngOnInit() {
    this.modalOption.backdrop = 'static';
    this.modalOption.keyboard = false;
    this.obtenerNombramientosSegunUsuario();
  }

  obtenerNombramientosSegunUsuario() {
    this.tipoPermisoUsuario = this.authService.getRoles().ser.split(',');
    let Roles = this.global.variablesGlobales.roles;
    if (this.tipoPermisoUsuario.includes(Roles.JEFE_SERVICIO)) {
      console.log("JefeServicio");
      this.obtenerServicioFuncionario();  //obtiene el servicio y hace el request de nombramientos
    }
    else if(this.tipoPermisoUsuario.includes(Roles.RECURSOS_HUMANOS)) {
      this.puestosCargarLista();
    }
    else {
      console.log("GG");
    }

  }
  /*
  Descripcion: Se encarga de obtener el servicio en el que se encuentra
  laborando actualmente el funcionario, mediante una analisis
  de su nombramiento. No se brindaran permisos de acceso si el
  usuario no tiene un nombramiento actual
  Envia: no aplica
  Recibe: El id del servicio en el que esta
  laborando el funcionario
  */
 obtenerServicioFuncionario() {
  this.nomService.obtenerServicioFuncionario()
    .subscribe(
      success => {
        this.idServicio = success['id_servicio'];
        console.log(`EL id de servicio actual es ${this.idServicio}`)
        if (this.idServicio !== undefined) 
          this.obtenerNombramientosSegunServicio(this.idServicio);
        else {
          //this._router.navigate([this.urlViewPermissionError])};
          console.log(success);
        }
      },
      err => {
        //this._router.navigate([this.urlViewPermissionError]);
        console.error(err);
      },
    );
}

//Obtiene la lista de nombramientos para un servicio determinado
obtenerNombramientosSegunServicio(idServicio): void{
  this.nomService.obtenerNombramientosPorServicio(idServicio)
    .subscribe(
      success => {
        this.nombramientos = success;
      },
      err => {
        //this._router.navigate([this.urlViewPermissionError]);
        console.error(err);
      },
    );
}
  /*
  Descripcion: Funcion que trae la lista de nombramientos para poder llenar la tabla de visualización de todos los
  nombramientos existentes para poder modificarlos o eliminarlo.
  Recibe:
  Envia: Lista con los datos de todos los nombramientos.
  */
  puestosCargarLista(): void {
    this.nomService.getNom()
      .subscribe(
        success => {
          this.nombramientos = success;
        },
        err => {
          window.console.log('error')  // dirigir a view de mensaje de error (FALTA)
          // this._router.navigate(['/error']);
        },
      );
  }

  abrirModal(nombramiento: Nombramiento, id) {
    const modalRef = this.modalService.open(ModificarComponent, this.modalOption);
    modalRef.componentInstance.nom = nombramiento;
    modalRef.componentInstance.Nombramiento = nombramiento;
    modalRef.result.then((result) => {
      this.puestosCargarLista();
    });
  }

  abrirModalCrear() {
    this.modalService.open(CrearNombramientoComponent, this.modalOption).result.then((result) => {
      this.puestosCargarLista();
    });
  }
  eliminarOficial() {
    this.nomService.eliminarNombramiento(this.variablita)
      .subscribe(
        success => {
          this.nombramiento_e = success;
          this.toasterManagerService.makeToast('success', '¡Éxito!', 'Se ha eliminado correctamente el nombramiento.');
          this.puestosCargarLista();
        },
        err => {
          this.toasterManagerService.makeToast('error', '¡Fallo!', 'No se ha eliminado correctamente el nombramiento.');
        },
      );
    this.variablita = 0;
  }
  openWindowCustomClass(content, code) {
    this.variablita = code;
    this.modalService.open(content, { windowClass: 'dark-modal' });
  }

  openSm(content) {
    this.modalService.open(content, { size: 'sm' });
  }

  openLg(content) {
    this.modalService.open(content, { size: 'lg' });
  }
}
