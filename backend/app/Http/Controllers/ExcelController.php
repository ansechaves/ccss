<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Funcionario;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Concerns\ToModel;

class ExcelController extends Controller
{
    public function Import()
    {  
        \Excel::import(new UsersImport, 'DataBaseFuncionarios.xlsx');
        return view('vista2',['name' => 'Carga']);         
    }
}
