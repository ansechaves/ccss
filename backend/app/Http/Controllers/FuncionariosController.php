<?php

namespace App\Http\Controllers;

use App\Funcionario;
use App\Telefono;
use App\Puesto;
use App\Funcionario_Nombramiento;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DateTime;

class FuncionariosController extends Controller
{
    //Traer todos los datos de funcionarios
    public function consultar(){
        $funcionarios = Funcionario::all();
        
        foreach($funcionarios as $funcionario){
            $funcionario->telefono;
        }
        return $funcionarios;
    
    }
    
    //Inserta un nuevo funcionario
    public function insertar(Request $request){
        $funcionario = DB::table('funcionarios')->insert(
            ['cedula' =>  $request->input('cedula'), 
            'nombre' => $request->input('nombre'),
            'apellido1'=>$request->input('apellido1'),
            'apellido2'=>$request->input('apellido2'),
            'fecha_nacimiento'=>$request->input('fecha_nacimiento'),
            'fecha_ingreso'=>$request->input('fecha_ingreso'),
            'numero_tarjeta'=>$request->input('numero_tarjeta'),
            'correo'=>$request->input('correo')
            ]
        );
 
        if((count($request->input('telefono'))>0) &&  !is_null($request->input('telefono')[0]['numero'])  ){
            $telefonos = $request->input('telefono');
            foreach ($telefonos as $telefono){
                Telefono::create(array('cedula' =>  $request->input('cedula'), 
                'numero' =>  $telefono['numero']));
            }
        }

         //Devuelve el funcionario que se acaba de crear
        $fun = Funcionario::where('cedula', '=',$request->input('cedula'))->first();
        $fun->telefono;
        return response ()->json($fun); 
    }

    //Elimina un funcionario existente
    public function borrar($cedula){
        $funcionario=Funcionario::where('cedula',$cedula)->first()->delete();
        //$funcionario->delete();
        return response()->json(['message' => 'Usuario Eliminado'], 200);
    }

    //Modifica un funcionario existente
    public function editar(Request $request){
        
        //Actualizar el funcionario
        DB::table('funcionarios')->where('cedula',$request->input('cedula')) 
        ->update(
        ['nombre' => $request->input('nombre'),
        'apellido1'=>$request->input('apellido1'),
        'apellido2'=>$request->input('apellido2'),
        'fecha_nacimiento'=>$request->input('fecha_nacimiento'),
        'fecha_ingreso'=>$request->input('fecha_ingreso'),
        'numero_tarjeta'=>$request->input('numero_tarjeta'),
        'correo'=>$request->input('correo')
        ]
    );

    //Actualizar el telefono
    $telefonos = $request->input('telefono');
    $telefonosFuncionario = Telefono::where('cedula', $request->input('cedula'))->get();
    $contador=0;
    foreach ($telefonos as $telefono){
    /*    $telefono->updateOrCreate(['id'=>$telefonos[$contador]['id']],[ 'numero'=>$telefonos[$contador]['numero'],'cedula'=>$request->input('cedula')]);
    */

        if ($telefonos[$contador]['id']){
            
            if($telefonos[$contador]['numero']=="-1"){
                Telefono::find($telefonos[$contador]['id'])->delete();
            }
            else{
                $tel=Telefono::find($telefonos[$contador]['id']);
                $tel->numero = $telefonos[$contador]['numero'];
                $tel->save();
            }
        }
        
        else{
            Telefono::create(array('cedula' =>  $request->input('cedula'), 'numero' =>  $telefonos[$contador]['numero']));
        }   
        


        $contador+=1;
    }






    //Devuelve el funcionario que se acaba de modificar
    $fun = Funcionario::where('cedula', '=',$request->input('cedula'))->first();
    $fun->telefono;
    return response ()->json($fun);
    }
    // Metodos de Natalin
    public function funcionarioPorNombre($nombre)
    {
        $funcionario= Funcionario::where('nombre', '=', $nombre)->first();
        return response()->json($funcionario);
        // return $funcionario;
    }

    //lista con las cedulas de todos los funcionarios del sistema
    public function cedulaFuncionarios(){

        $cedulaFuncionariosLista = array();
       // $funcionarioCed = Funcionario::all();
        $funcionarioCed = DB::table('funcionarios')
            ->select('cedula')
            ->get();

        foreach ($funcionarioCed as $funCed){
            array_push($cedulaFuncionariosLista,$funCed->cedula);
        }
        return response()->json($cedulaFuncionariosLista);
    }

    //naty
        public function funcionarioPorNumeroTarjeta($numero)
        {
            $funcionario= Funcionario::where('numero_tarjeta', '=', $numero)->first();
            return response()->json($funcionario);
            // return $funcionario;
        }

        public function funcionarioPorCedula($cedula)
            {
                $funcionario= Funcionario::where('cedula', '=', $cedula)->first();
                return response()->json($funcionario);
                // return $funcionario;
            }

        public function getFuncionarios()
            {
               $funcionario= Funcionario::all();
               return response()->json($funcionario);
            }



    /*
    Se deberá generar un listado que muestre las personas que han sido asignadas a un puesto.
    Mediante una búsqueda por nombre o código de puesto con el siguiente formato: ______________
    Nombre de la persona                                           -->done
    Número de tarjeta (id) con el siguiente formato: ______________ ->done
    Cantidad de días en el puesto                                  -->done
    Cantidad de días en la institución                             -->done
    El sistema debe mostrar más información al seleccionar un funcionario:
    Se muestra la información del informe por persona
    */
    //funcional informePorPuesto
    public function informePorPuestoA($codigoPuesto){

        $nombramientosExistentesEnPuesto = DB::table('nombramientos')
        //->where('id_puesto', 10)
        ->where('id_puesto', $codigoPuesto)
        ->select('nombramientos.id', 'nombramientos.fecha_inicio', 'nombramientos.fecha_fin', 'nombramientos.numero_dias')
        ->get();
        //echo $nombramientosExistentesEnPuesto;

        //los codigos de los nombramientos que tiene asignado un funcionario
        $codigoDeNombramientosPorFuncionario =  array();

        foreach ($nombramientosExistentesEnPuesto as $funcionario) {

            $cedulaFuncionarioNombradoEnPuesto = DB::table('funcionarios_nombramientos')
            ->where('id_nombramiento', intval($funcionario->id))
            ->select('funcionarios_nombramientos.cedula')
            ->get();
            //echo $cedulaFuncionarioNombradoEnPuesto;

            /*echo 'El usuario cedula: ';
            echo $cedulaFuncionarioNombradoEnPuesto[0]->cedula;
            echo ' tiene el nombramiento codigo ';
            echo intval($funcionario->id);*/


            $cedulaFuncionario = intval($cedulaFuncionarioNombradoEnPuesto[0]->cedula);
            $idNombramientoDelFuncionario = intval($funcionario->id);

            $infoFuncionario = DB::table('funcionarios')
            ->where('cedula', $cedulaFuncionario)
            ->select('funcionarios.nombre', 'funcionarios.apellido1', 'funcionarios.apellido2', 'funcionarios.fecha_ingreso')
            ->get();

            /*echo '//inicio  ';
            echo " Cantidad de dias de nombramiento " . $funcionario->numero_dias . " " . "cantidad de dias total: " . $funcionario->numero_dias ;
            echo  "  //fin";*/

            if (in_array($cedulaFuncionario, array_keys($codigoDeNombramientosPorFuncionario))){
            /*
                array_push($codigoDeNombramientosPorFuncionario[$cedulaFuncionario]['infoNombramiento'], array($funcionario->id,
                    $funcionario->fecha_inicio,
                    $funcionario->fecha_fin)); */

                $codigoDeNombramientosPorFuncionario[$cedulaFuncionario]['cantidadDeDiasEnPuesto'] =
                    $funcionario->numero_dias + $codigoDeNombramientosPorFuncionario[$cedulaFuncionario]['cantidadDeDiasEnPuesto'] ;
            }
            else{
                /*
                $codigoDeNombramientosPorFuncionario[$cedulaFuncionario]['infoNombramiento'] = array(array($funcionario->id,
                    $funcionario->fecha_inicio,
                    $funcionario->fecha_fin)); */

                $codigoDeNombramientosPorFuncionario[$cedulaFuncionario]['infoFuncionario'] = array($infoFuncionario[0]->nombre,
                    $infoFuncionario[0]->apellido1,
                    $infoFuncionario[0]->apellido2);
                $codigoDeNombramientosPorFuncionario[$cedulaFuncionario]['cantidadDeDiasEnPuesto'] = $funcionario->numero_dias;


                //cantidad de dias en institucion
                $cantidadDeDiasEnInstitucion = DB::table('funcionarios_nombramientos')
                    ->where('cedula',$cedulaFuncionario)
                    ->join('nombramientos','funcionarios_nombramientos.id_nombramiento', 'nombramientos.id')
                    ->sum('nombramientos.numero_dias');

                $codigoDeNombramientosPorFuncionario[$cedulaFuncionario]['cantidadDeDiasEnInstitucion'] = $cantidadDeDiasEnInstitucion;


                $cant = DB::table('funcionarios_nombramientos')
                    ->where('cedula',$cedulaFuncionario)
                    ->join('nombramientos','funcionarios_nombramientos.id_nombramiento', 'nombramientos.id')
                    //->join('puestos','nombramientos.id_puesto','puestos.id')
                    ->join('puestos',function($join){
                        $join->on('nombramientos.id_puesto', "=", 'puestos.id')
                        ->where('puestos.id', '=', 10);
                    })
                    ->sum('nombramientos.numero_dias');

                $codigoDeNombramientosPorFuncionario[$cedulaFuncionario]['cantidadDeDiasEnPuestoOO'] = $cant;
            }

        }
        //echo '->';
        print_r ($codigoDeNombramientosPorFuncionario);
        //echo ' /// ';
        //echo json_encode($codigoDeNombramientosPorFuncionario);

    }



    function obtenerNombreFuncionario(){  //$cedula  --> por parametro

        $cedulaFuncionario=$_GET['cedulaFuncionario'];
        echo $cedulaFuncionario;

        $nombreFuncionario = DB::table('funcionarios')
            ->where('cedula', $cedulaFuncionario)
            ->select('funcionarios.nombre', 'funcionarios.apellido1', 'funcionarios.apellido2')
            ->get();
            echo $nombreFuncionario;
    }




    //BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBb

    /*
    Se deberá generar un listado que muestre las personas que han sido asignadas a un puesto.
    Mediante una búsqueda por nombre o código de puesto con el siguiente formato: ______________
    Nombre de la persona                                           -->done
    Número de tarjeta (id) con el siguiente formato: ______________ ->done
    Cantidad de días en el puesto                                  -->done
    Cantidad de días en la institución                             -->done
    El sistema debe mostrar más información al seleccionar un funcionario:
    Se muestra la información del informe por persona
    */
    //funcional informePorPuesto
    public function informePorPuesto($codigoPuesto){

        //traduce del codigo al id
        $id_puesto = DB::table('puestos')
            ->where('codigo',$codigoPuesto)
            ->select('id')
            ->get();

        $id_puesto = $id_puesto[0]->id;

        $funcionariosNombrados = array();

        $nombramientosExistentesEnPuesto = DB::table('nombramientos')
        ->where('id_puesto', $id_puesto)
        ->select('nombramientos.id', 'nombramientos.fecha_inicio', 'nombramientos.fecha_fin', 'nombramientos.numero_dias')
        ->get();
        //echo $nombramientosExistentesEnPuesto;

        //los codigos de los nombramientos que tiene asignado un funcionario
        $codigoDeNombramientosPorFuncionario =  array();

        foreach ($nombramientosExistentesEnPuesto as $funcionario) {

            $cedulaFuncionarioNombradoEnPuesto = DB::table('funcionarios_nombramientos')
            ->where('id_nombramiento', intval($funcionario->id))
            ->select('funcionarios_nombramientos.cedula')
            ->get();
            //echo $cedulaFuncionarioNombradoEnPuesto;

            /*echo 'El usuario cedula: ';
            echo $cedulaFuncionarioNombradoEnPuesto[0]->cedula;
            echo ' tiene el nombramiento codigo ';
            echo intval($funcionario->id);*/


            $cedulaFuncionario = intval($cedulaFuncionarioNombradoEnPuesto[0]->cedula);
            $idNombramientoDelFuncionario = intval($funcionario->id);

            $infoFuncionario = DB::table('funcionarios')
            ->where('cedula', $cedulaFuncionario)
            ->select('funcionarios.nombre', 'funcionarios.apellido1', 'funcionarios.apellido2', 'funcionarios.fecha_ingreso')
            ->get();

            $nombreFuncionario = $infoFuncionario[0]->nombre . ' ' .$infoFuncionario[0]->apellido1 .' ' .$infoFuncionario[0]->apellido2;


            if (in_array($cedulaFuncionario, $funcionariosNombrados)){

                foreach ($codigoDeNombramientosPorFuncionario as $key => $value) {

                    //echo $key;
                    //echo ' ';
                    if ($codigoDeNombramientosPorFuncionario[$key]['cedulaFuncionario'] == strval($cedulaFuncionario)){

                        //echo "\n\n  --> vslue ";
                        //print_r ($value);
                        //echo "\n\n";

                    /*
                        echo ' el funcionario cedula ';
                        echo $codigoDeNombramientosPorFuncionario[$key]['cedulaFuncionario'];
                        echo ' tenia ';
                        echo $codigoDeNombramientosPorFuncionario[$key]['cantidadDeDiasEnPuesto'];
                        echo ' se le suman '. $funcionario->numero_dias;
                        echo ' . Ahora tiene : -> ';*/
                        $codigoDeNombramientosPorFuncionario[$key]['cantidadDeDiasEnPuesto'] = $codigoDeNombramientosPorFuncionario[$key]['cantidadDeDiasEnPuesto'] + $funcionario->numero_dias;
                        //echo $codigoDeNombramientosPorFuncionario[$key]['cantidadDeDiasEnPuesto'];
                    }
                }
            }
            else{

                //cantidad de dias en institucion
                $cantidadDeDiasEnInstitucion = DB::table('funcionarios_nombramientos')
                    ->where('cedula',$cedulaFuncionario)
                    ->join('nombramientos','funcionarios_nombramientos.id_nombramiento', 'nombramientos.id')
                    ->sum('nombramientos.numero_dias');

                $cant = DB::table('funcionarios_nombramientos')
                    ->where('cedula',$cedulaFuncionario)
                    ->join('nombramientos','funcionarios_nombramientos.id_nombramiento', 'nombramientos.id')
                    ->join('puestos',function($join) use ($id_puesto) {
                        $join->on('nombramientos.id_puesto', "=", 'puestos.id')
                        ->where('puestos.id', '=', $id_puesto);
                    })
                    ->avg('nombramientos.numero_dias'); //avg - sum


                $lista = array(
                    'NombreFuncionario'=>$nombreFuncionario,
                    'cedulaFuncionario'=> strval($cedulaFuncionario),
                    'cantidadDeDiasEnPuesto'=>$funcionario->numero_dias,
                    'cantidadDeDiasEnInstitucion'=> $cantidadDeDiasEnInstitucion,
                    'cantidadDeDiasEnPuestoOO'=>$cant
                    );

                array_push($codigoDeNombramientosPorFuncionario, $lista);
                array_push($funcionariosNombrados,$cedulaFuncionario);
            }

        }
        //echo '->';
        //print_r ($codigoDeNombramientosPorFuncionario);
        //echo json_encode($codigoDeNombramientosPorFuncionario);
        $codigoDeNombramientosPorFuncionario = json_encode($codigoDeNombramientosPorFuncionario);
        //echo $codigoDeNombramientosPorFuncionario;
        return $codigoDeNombramientosPorFuncionario;

    }


















}


