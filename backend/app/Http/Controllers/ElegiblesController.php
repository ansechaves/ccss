<?php

namespace App\Http\Controllers;


use \Datetime;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class ElegiblesController extends Controller
{
    public function obtenerElegiblesPasivosActivos($idPuesto)
    {
        $fechaUmbralCategoria = date("Y-m-d", strtotime("-6 month"));
        $fechaHoy = date("Y-m-d");

        $servicioSeleccionado = DB::table('puestos') //obtiene el servicio del puesto seleccionado
        ->join('servicios', 'servicios.id', '=', 'puestos.id_servicio')
        ->where('puestos.id','=',$idPuesto)
        ->whereNull('servicios.deleted_at')
        ->whereNull('puestos.deleted_at')
        ->select('servicios.id','servicios.nombre')->first();

        $diasPuesto = DB::table('funcionarios_nombramientos') //obtiene dias en puesto de los elegibles
        ->join('nombramientos', 'nombramientos.id', '=', 'funcionarios_nombramientos.id_nombramiento')
        ->join('puestos', 'puestos.id', '=', 'nombramientos.id_puesto')
        ->where('puestos.id','=',$idPuesto)
        ->select('funcionarios_nombramientos.cedula',DB::raw('SUM(nombramientos.numero_dias) as diasPuesto'))
        ->groupBy('nombramientos.id_puesto','funcionarios_nombramientos.cedula');

        $diasServicio = DB::table('funcionarios_nombramientos') //subQuery
        ->join('nombramientos', 'nombramientos.id', '=', 'funcionarios_nombramientos.id_nombramiento')
        ->join('puestos', 'puestos.id', '=', 'nombramientos.id_puesto')
        ->join('servicios','servicios.id','=','puestos.id_servicio')
        ->where('servicios.id','=',$servicioSeleccionado->id)
        ->select('funcionarios_nombramientos.cedula',DB::raw('SUM(nombramientos.numero_dias) as diasServicio'))
        ->groupBy('servicios.id','funcionarios_nombramientos.cedula');

        $diasHospital = DB::table('funcionarios_nombramientos') //subQuery
        ->join('nombramientos', 'nombramientos.id', '=', 'funcionarios_nombramientos.id_nombramiento')
        ->join('puestos', 'puestos.id', '=', 'nombramientos.id_puesto')
        ->join('servicios','servicios.id','=','puestos.id_servicio')
        ->select('funcionarios_nombramientos.cedula',DB::raw('SUM(nombramientos.numero_dias) as diasHospital'))
        ->groupBy('funcionarios_nombramientos.cedula');

        $disponibilidadYCategoria = DB::table('funcionarios_nombramientos')
        ->join('nombramientos', 'nombramientos.id', '=', 'funcionarios_nombramientos.id_nombramiento')
        ->join('puestos', 'puestos.id', '=', 'nombramientos.id_puesto')
        ->join('servicios','servicios.id','=','puestos.id_servicio')
        ->where('servicios.id','=',$servicioSeleccionado->id)
        ->selectRaw('MAX(nombramientos.fecha_fin) as fecha_fin_ult_nomb,(CASE WHEN (MAX(nombramientos.fecha_fin) > ?) THEN \'Activo\' ELSE \'Pasivo\' END) as categoria,
         funcionarios_nombramientos.cedula, (CASE WHEN (MAX(nombramientos.fecha_fin) > ?) THEN \'0\' ELSE \'1\' END) as disponible', [$fechaUmbralCategoria,$fechaHoy])
        ->groupBy('funcionarios_nombramientos.cedula');

        $elegibles = DB::table('funcionarios') //lista final de elegibles
        ->joinSub($diasServicio, 'diasServicio', function ($join) { 
            $join->on('diasServicio.cedula', '=', 'funcionarios.cedula');
        })
        ->leftJoinSub($diasPuesto, 'diasPuesto', function ($join) {
            $join->on('diasPuesto.cedula', '=', 'funcionarios.cedula');
        })
        ->joinSub($diasHospital, 'diasHospital', function ($join) {
            $join->on('diasHospital.cedula', '=', 'diasServicio.cedula');
        })
        ->joinSub($disponibilidadYCategoria, 'disponibilidadCategoria', function ($join) {
            $join->on('disponibilidadCategoria.cedula', '=', 'diasServicio.cedula');
        })
        ->select('funcionarios.cedula',DB::raw('concat(funcionarios.nombre, \' \' ,funcionarios.apellido1, \' \' ,funcionarios.apellido2) as nombre'),
        'diasPuesto.*','diasServicio.*','diasHospital.*','disponibilidadCategoria.categoria','disponibilidadCategoria.disponible')
        ->orderBy('diasPuesto.diasPuesto','desc')
        ->get();

        return response()->json($elegibles);
    }

    public function obtenerListaElegibles($idPuesto, $fechaInicial, $fechaFinal)
    {
        return DB::select("EXEC obtenerElegibles '" . $idPuesto . "', '" . $fechaInicial . "', '" . $fechaFinal . "'");
    }
    public function obtenerListaNombresFuncionarios()
    {
        $lista = array();
        $funcionarios = DB::table('funcionarios')
            ->whereNull('deleted_at')
            ->select(
                'nombre',
                'apellido1',
                'apellido2'
            )
            ->orderBy('nombre', 'asc')
            ->get();

        if ($funcionarios != null) {
            for ($x = 0; $x < count($funcionarios); $x++) {
                array_push($lista, $funcionarios[$x]->nombre
                    . ' ' . $funcionarios[$x]->apellido1
                    . ' ' . $funcionarios[$x]->apellido2);
            }
        }
        return $lista;
    }

    // TODO: Revisar si esta funcionando bien. Devuelve un objeto vacío para el usuario 1
    public function obtenerServicioFuncionario($cedula)
    {
        $servicioFuncionario = DB::table('funcionarios_nombramientos')
            ->join('nombramientos', 'nombramientos.id', '=', 'funcionarios_nombramientos.id_nombramiento')
            ->join('puestos', 'puestos.id', '=', 'nombramientos.id_puesto')
            ->join('servicios', 'servicios.id', '=', 'puestos.id_servicio')
            ->where('funcionarios_nombramientos.cedula', '=', $cedula)
            ->where('nombramientos.fecha_inicio', '<=', Carbon::now('America/Costa_Rica'))
            ->where('nombramientos.fecha_fin', '>=', Carbon::now('America/Costa_Rica'))
            ->whereNull('nombramientos.deleted_at')
            ->whereNull('servicios.deleted_at')
            ->whereNull('puestos.deleted_at')
            ->whereNull('funcionarios_nombramientos.deleted_at')
            ->select('servicios.nombre as nombre_servicio', 'servicios.id as id_servicio')->first();
        return response()->json($servicioFuncionario);
    }

    public function obtenerPuestosSegunServicio($idServicio)
    {
        $puestos = DB::table('puestos')
            ->join('servicios', 'puestos.id_servicio', '=', 'servicios.id')
            ->where('servicios.id', '=', $idServicio)
            ->whereNull('puestos.deleted_at')
            ->whereNull('servicios.deleted_at')
            ->orderBy('puestos.nombre', 'asc')
            ->select(
                'puestos.*'
            )
            ->get();
        return $puestos;
    }
    public function obtenerServicios()
    {
        $servicios = DB::table('servicios')
            ->whereNull('deleted_at')
            ->select(
                '*'
            )
            ->orderBy('nombre', 'asc')
            ->get();
        return $servicios;
    }
}
