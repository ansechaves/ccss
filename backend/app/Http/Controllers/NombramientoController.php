<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Nombramiento;
use App\Funcionario_Nombramiento;
use App\Puesto;
use App\Funcionario;


class NombramientoController extends Controller
{

    public function obtener_nombramiento_real($id_funcionario_nombramiento)
    {
        $nombramiento_buscado = DB::table('funcionarios_nombramientos')
            ->where('id', '=',$id_funcionario_nombramiento)
            ->select('id_nombramiento')
            ->get();

        foreach ($nombramiento_buscado as $valor) {
            foreach ($valor as $clave2 => $valor2) {
                if ($clave2 == "id_nombramiento") /*declaro una nueva variable fecha actual y le doy el valor correspondiente para poder medir*/
                    $id_nombramiento = $valor2;
            }
        }
        return $id_nombramiento;
    }

    public function inhabilitarNombramiento($id)
    {
        $id_nombramiento_a_borrar= $this->obtener_nombramiento_real($id);
        $nombramientos_funcionarios = Funcionario_Nombramiento::find($id);
        $nombramientos_funcionarios->delete();

        $nombramientos = Nombramiento::find($id_nombramiento_a_borrar);
        $nombramientos->estado = 0;
        $nombramientos->save();
        $nombramientos->delete();
        return $nombramientos;
    }

    //Procedimiento para inhabilitar un nombramiento
    public function buscarPuestos($puestoBuscado)
    {
        $result = DB::table('puestos')
            ->join('nombramientos', 'puestos.id', '=', 'nombramientos.id_puesto')
            ->join('funcionarios_nombramientos', 'nombramientos.id', '=', 'funcionarios_nombramientos.id_nombramiento')
            ->join('funcionarios', 'funcionarios_nombramientos.cedula', '=', 'funcionarios.cedula')
            ->where('puestos.descripcion', $puestoBuscado)->get();
        return $result;
    }

    public function eliminarCorchetes($variable)
    {
        $name = $variable;
        $name = substr($name, 1);
        $myString = trim($name, '}');
        return $myString;
    }

    public function buscarPuestoPorElegible($nombrePuestoBuscado)
    {
        $nombrePuestoBuscado = $this->eliminarCorchetes($nombrePuestoBuscado);
        $funcionarioElegible = DB::table('puestos')
            ->leftJoin('nombramientos', 'nombramientos.id_puesto', '=', 'puestos.id')
            ->join('funcionarios_nombramientos', 'nombramientos.id', '=', 'funcionarios_nombramientos.id_nombramiento')
            ->select('funcionarios.nombre', 'funcionarios_nombramientos.cedula', 'puestos.nombre', DB::raw('sum(nombramientos.numero_dias) as cantidadDiasLaborados'))
            ->join('funcionarios', 'funcionarios.cedula', '=', 'funcionarios_nombramientos.cedula')
            ->where('puestos.nombre', '=', $nombrePuestoBuscado)
            ->groupBy('funcionarios_nombramientos.cedula', 'funcionarios.nombre', 'puestos.nombre')
            ->get();
        return $funcionarioElegible;
    }

    public function index()
    {
        $nombramientos = DB::table('nombramientos')
            ->join('funcionarios_nombramientos', 'nombramientos.id', '=', 'funcionarios_nombramientos.id_nombramiento')
            ->join('funcionarios', 'funcionarios_nombramientos.cedula', '=', 'funcionarios.cedula')
            ->join('puestos', 'nombramientos.id_puesto', '=', 'puestos.id')
            ->where('nombramientos.deleted_at', null)
            ->select('nombramientos.id','puestos.nombre as nombre_puesto','funcionarios.nombre as nombre_funcionario',
            'funcionarios.cedula','nombramientos.tiempo','nombramientos.tipo')
            ->get();
        return $nombramientos;
    }

    //Devuelve los nombramientos de un servicio especificado por ID
    public function nombramientosPorServicio($idServicio){
        $nombramientos = DB::table('nombramientos')
            ->join('funcionarios_nombramientos', 'nombramientos.id', '=', 'funcionarios_nombramientos.id_nombramiento')
            ->join('funcionarios', 'funcionarios_nombramientos.cedula', '=', 'funcionarios.cedula')
            ->join('puestos', 'nombramientos.id_puesto', '=', 'puestos.id')
            ->join('servicios', 'puestos.id_servicio', '=', 'servicios.id')
            ->where('servicios.id','=',$idServicio)
            ->where('nombramientos.deleted_at', null)
            ->select('nombramientos.id','puestos.nombre as nombre_puesto','funcionarios.nombre as nombre_funcionario',
            'funcionarios.cedula','nombramientos.tiempo','nombramientos.tipo')
            ->get();
        return $nombramientos;
    }

    public function llenarListaPuestosPorPersona($cedulaBuscada)
    {
        $puestos = DB::table('puestos')
            ->join('nombramientos', 'nombramientos.id_puesto', '=', 'puestos.codigo')
            ->join('funcionarios_nombramientos', 'funcionarios_nombramientos.id_nombramiento', '=', 'nombramientos.id')
            ->where('cedula', '=', $cedulaBuscada)
            ->select('id_puesto', 'nombre')->GroupBy('id_puesto', 'nombre')->get();
        return $puestos;
    }

    public function buscarInformacionPuesto($cedula, $idPuesto)
    {
        $puestos = DB::table('nombramientos')
            ->join('funcionarios_nombramientos', 'funcionarios_nombramientos.id_nombramiento', '=', 'nombramientos.id')
            ->where('cedula', '=', $cedula)
            ->where('id_puesto', '=', $idPuesto)
            ->select('*')->get();
        return $puestos;
    }

    public function funcionarioNombramiento($id)
    {
        $nombramiento = Funcionario_Nombramiento::where('id_nombramiento', '=', $id)->first();
        return response()->json($nombramiento);
    }

    // return $nombramientos;
    public function editarNombramiento(Request $request, $id)
    {
        $id_nombramiento= $this->obtener_nombramiento_real($id);

        $nombramiento = Nombramiento::find($id_nombramiento);
        // $nombramiento = Nombramiento::find($id);
        $nombramiento->id_puesto = $request->input('id_puesto');
        $nombramiento->fecha_inicio = $request->input('fecha_inicio');
        $nombramiento->fecha_fin = $request->input('fecha_fin');
        $nombramiento->tiempo = $request->input('tiempo');
        $nombramiento->tipo = $request->input('tipo');
        $nombramiento->sustituye = $request->input('sustituye');
        $nombramiento->motivo = $request->input('motivo');
        $funcionarioNombramiento = Funcionario_Nombramiento::where('id_nombramiento', '=', $id_nombramiento)->first();
        $funcionarioNombramiento->cedula = $request->input('cedula');
        $nombramiento->save();
        $funcionarioNombramiento->save();

        return $id_nombramiento;
    }

    public function obtenerListaPuestos()
    {
        $puestos = Puesto::all();
        return response()->json($puestos);
    }

    public function crearNombramiento(Request $request)
    {
        $nombramientos = new Nombramiento;

        $nombramientos->id_puesto = $request->input('id_puesto');
        $nombramientos->fecha_inicio = $request->input('fecha_inicio');
        $nombramientos->fecha_fin = $request->input('fecha_fin');
        $nombramientos->numero_dias = 0;
        $nombramientos->tiempo = $request->input('tiempo');
        $nombramientos->tipo = $request->input('tipo');
        $nombramientos->sustituye = $request->input('sustituye');
        $nombramientos->estado = 1;
        $nombramientos->motivo = $request->input('motivo');

        $nombramientos->save();

        //realiza la insercion de funcionarios_nombramiento en la base de datos
        $funcionarios_nombramientos = new Funcionario_Nombramiento;

        $funcionarios_nombramientos->cedula = $request->input('cedula');
        $funcionarios_nombramientos->id_nombramiento = $nombramientos->id;
        $funcionarios_nombramientos->fecha_nombramiento = $request->input('fecha_fin');

        $funcionarios_nombramientos->save();

        return $nombramientos;
    }

    public function obtenerPuestos()
    {
        $puestos = array();
        $nombramientos = Nombramiento::where('numero_dias', '>', 30)->get();
        //Descomentar para imprimir objeto JSON

        foreach ($nombramientos as $nombramiento) {
            $nombramientos = Nombramiento::where('tipo', '=', 'vac')->get();
            //echo $nombramiento->numero_dias;
            array_push($puestos, $nombramiento);
        }
        //return response()->json($nombramiento);
        foreach ($puestos as $puesto) {
            echo $puesto;
        }
    }

    public function mostrarNombramientos()
    {
        $nombramientos = Nombramiento::all();
        foreach ($nombramientos as $nombramiento) {
            echo $nombramiento->name;
        }
    }

    //Obtener la cedula de los funcionarios del sistema para simplificar el proceso de crear nombramiento
    public function obtenerListaCedulasFuncionarios()
    {
        $funcionariosLista = array();
        $funcionarios = Funcionario::all();

        foreach ($funcionarios as $fun) {
            //echo $fun->cedula;
            //array_push($funcionariosLista,[$fun->cedula, $fun->nombre, $fun->apellido1, $fun->apellido2]);
            array_push($funcionariosLista, $fun->cedula);
        }
        //return response()->json($funcionarios);
        return response()->json($funcionariosLista);
    }

    //Obtener la cedula de los funcionarios del sistema para simplificar el proceso de crear nombramiento
    public function obtenerListaNombresFuncionarios()
    {
        $funcionariosNombreLista = array();
        $funcionarios = Funcionario::all();

        foreach ($funcionarios as $fun) {
            array_push($funcionariosNombreLista, $fun->nombre . " " . $fun->apellido1 . " " . $fun->apellido2);
        }

        return response()->json($funcionariosNombreLista);

    }

    //Obtener el codigo de los puestos a nombrar
    public function obtenerListaCodigoPuestosNombrar()
    {
        $codigoPuestoLista = array();
        $puestos = Puesto::all();

        foreach ($puestos as $pues) {
            array_push($codigoPuestoLista, $pues->codigo);
        }
        return response()->json($codigoPuestoLista);
    }


    public function buscarFuncionarioNombramiento($cedula, $fecha_inicio)
    {
        $ultimo_nombramiento = Funcionario_Nombramiento::where('cedula', '=', $cedula)
            ->orderBy('created_at', 'desc')
            ->first();
        //return $nombramiento->id_nombramiento;
        if ($ultimo_nombramiento == null)
            return 0;
        elseif ($ultimo_nombramiento != null) {
            //Una vez que se tiene el ultimo nombramiento vamos a averiguar si ese nombramiento esta activo o desactivado
            $datos_ultimo_nombramiento = DB::table('funcionarios_nombramientos')
                ->join('nombramientos', 'funcionarios_nombramientos.id_nombramiento', '=', 'nombramientos.id')
                ->where('funcionarios_nombramientos.id_nombramiento', $ultimo_nombramiento->id_nombramiento)
                ->get();
            //return $datos_ultimo_nombramiento;

            $estado_actual = 0;
            $fecha_fin_nombramiento_actual = '2018-01-01';
            // $fecha_inicio_nombramiento_actual = '2018-01-01';

            foreach ($datos_ultimo_nombramiento as $valor) {
                foreach ($valor as $clave2 => $valor2) {
                    if ($clave2 == "estado")
                        $estado_actual = $valor2;
                    elseif ($clave2 == "fecha_fin")
                        $fecha_fin_nombramiento_actual = $valor2;
                    //  elseif ($clave2 == "fecha_inicio")
                    //      $fecha_inicio_nombramiento_actual = $valor2;
                }
            }
            if ($estado_actual == 0 and strtotime($fecha_fin_nombramiento_actual) < strtotime($fecha_inicio))
                return 0;
            elseif ($estado_actual == 1 and strtotime($fecha_fin_nombramiento_actual) < strtotime($fecha_inicio))
                return 0;
            else
                return 1;
        }
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////
    /////////////////////NOMBRAMIENTOS SUPERPUESTOS/////////////////////////
    ////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////


    public function encontrar_puesto($codigo_puesto)
    {
        // voy a retornar el ID
        $puesto_buscado = DB::table('puestos')
            ->where('puestos.codigo', '=',$codigo_puesto)
            ->get();

        foreach ($puesto_buscado as $valor) {
            foreach ($valor as $clave2 => $valor2) {
                if ($clave2 == "id") /*declaro una nueva variable fecha actual y le doy el valor correspondiente para poder medir*/
                    $id_codigo_puesto = $valor2;
            }
        }
        return $id_codigo_puesto;
    }

    public function prueba($id_funcionario_nombramiento)
    {
        return 1;
    }


    /*Descripcion: Recibe el nombramiento en reques (es decir, lo que el usuario escribio en el formulario y procede a
        corroborar si ese usuario tuene o no un nombramiento activo.
      Recibe: Request del formulario de Crear nombramiento
      Envia: Si el valor es 0 -> CrearNombramiento() si es 1-> determinar_mayor_o_menor_nombramiento()
      */
    public function crear_nombramiento_superpuesto(Request $request)
    {
        $funcionarios_nombramientos_id = $request->input('cedula');
        $nombramiento_estado = $this->funcionario_nombramiento_ultimo_encontrado($funcionarios_nombramientos_id);
        /****
         * ACA ES DONDE VA EL PROCESO DE 1 o 0
         * PARA SABER SI EL ESTADO ESTA O NO ACTIVO
         * SI ESTA ACTIVO (1) HACE LA COMPARACION DE FECHAS
         * SI NO ESTA ACTIVO (0) NO HACE NADA
         * /**/
        //print $nombramiento_estado;
        if ($nombramiento_estado == 0) {
            $this->crearNombramiento($request);
        } elseif ($nombramiento_estado == 1) {
            //print "Se identifica un estado con valor de 1.";
            //ACA DEBE DE DAR PIE A UNA ALERTA SOBRE SI DESEA REALIZAR EL NOMBRAMIENTO AUN HABIENTO UNO YA ACTIVO
            $this->determinar_mayor_o_menor_nombramiento($funcionarios_nombramientos_id, $request);
        }
    }


    /*Descripcion: Si el funcionario sí esta nombrado, debe de definir si es un nombramiento superpuesto mayor o menor y
    realizar una serie de procesos para determinar ese nombramiento.
    Recibe: Cedula del usuario que esta siendo nombrado, el Request
    Envia:  Segun el tipo de nombramiento llama una serie de diferentes funciones.
    */
    public function determinar_mayor_o_menor_nombramiento($cedul, Request $request)
    {
        /*Formato de fechas*/
        $to = \Carbon\Carbon::createFromFormat('Y-m-d', $request->input('fecha_inicio'));
        $from = \Carbon\Carbon::createFromFormat('Y-m-d', $request->input('fecha_fin'));

        /*Declaro variables para operaciones*/
        $fecha_fin_nombramiento_superpuesto = $request->input('fecha_fin');
        $fecha_ini_nombramiento_superpuesto = $request->input('fecha_inicio');
        $fecha_fin_nombramiento_actual = $request->input('fecha_fin');//pronto se actualiza, solo de doy un valor para poder llevar el formato correcto a la variable

        /*Ordena la tabla de nombramientos con el ultimo activado y lo almacena en la variable ultimo_nombramiento*/
        $ultimo_nombramiento = Funcionario_Nombramiento::where('cedula', '=', $cedul)
            ->orderBy('created_at', 'desc')
            ->first();
        //Una vez que se tiene el ultimo nombramiento vamos a averiguar si ese nombramiento esta activo o desactivado
        $datos_ultimo_nombramiento = DB::table('funcionarios_nombramientos')
            ->join('nombramientos', 'funcionarios_nombramientos.id_nombramiento', '=', 'nombramientos.id')
            ->where('funcionarios_nombramientos.id_nombramiento', '=', $ultimo_nombramiento->id_nombramiento)
            ->get();
        /*Aca ya tengo el dato de cual nombramiento estoy trabajando*/
        $id_nombramiento_actual = 0;
        $cedula_nombramiento_actual = 0;
        foreach ($datos_ultimo_nombramiento as $valor) {
            foreach ($valor as $clave2 => $valor2) {
                if ($clave2 == "fecha_fin") /*declaro una nueva variable fecha actual y le doy el valor correspondiente para poder medir*/
                    $fecha_fin_nombramiento_actual = $valor2;
                elseif ($clave2 == "id_nombramiento")
                    $id_nombramiento_actual = $valor2;
            }
        }
        //print $fecha_fin_nombramiento_actual;
        /*Si el nombramiento superpuesto es m?s grande que el nombramiento actual del funcionario*/
        /*funcional 100%*/


        if (strtotime($fecha_fin_nombramiento_actual) < strtotime($fecha_fin_nombramiento_superpuesto)) {
           // print "nombramiento superpuesto es mayor a la fecha final actual";
           // print $fecha_fin_nombramiento_actual;
           // print $fecha_fin_nombramiento_superpuesto;
           // print "Entro al if de que la fecha del nombramiento actual es m?s peque?a que la que trae en nombramiento superpuesto";
            $this->cambiar_estado($datos_ultimo_nombramiento);
            $this->cambiar_fecha_fin($datos_ultimo_nombramiento, $fecha_ini_nombramiento_superpuesto);
            return $this->crearNombramiento($request);
        } //Si el nombramiento superpuesto es m?s peque?o que el nombramiento actual del funcionario.
        elseif (strtotime($fecha_fin_nombramiento_actual) >= strtotime($fecha_fin_nombramiento_superpuesto)) {
            // print "Entro al if de que la fecha del nombramiento actual es m?s grande que la que trae en nombramiento superpuesto";
            // $this->cambiar_estado($datos_ultimo_nombramiento);
            $this->cambiar_fecha_fin($datos_ultimo_nombramiento, $fecha_ini_nombramiento_superpuesto);
            //$this->crear_nombramiento_huerfano($datos_ultimo_nombramiento, $fecha_ini_nombramiento_superpuesto, $fecha_fin_nombramiento_superpuesto);
            //Esta funcion crea el nombramiento y ademas lleva la informacion del nombramiento antiguo
            $this->crear_nombramiento_con_nombramiento_antiguo($request, $datos_ultimo_nombramiento);
            $this->nombramiento_mayor_parte_final($cedul, $id_nombramiento_actual, $fecha_fin_nombramiento_superpuesto, $fecha_fin_nombramiento_actual);
            //Funcion que crea un nombramiento sin una persona a cargo, este nombramiento se debe de mostrar en la lista de nombramientos a ofrecer
        }
    }

    /* Descripcion: Cuando el nombramiento es menor a la fecha final del nombramiento actual, se debe de segmentar el nombramiento en dos partes
    Recibe: El Request y toda el json con la información del ultimo nombramiento
    Envia: Retorna el $funcionarios_nombramientos y retorna $nombramientos, cada una tipos de esa misma clase
    */
    public function crear_nombramiento_con_nombramiento_antiguo(Request $request, $ultimo_nombramiento)
    {
        $id_nombramiento_antiguo = 0;
        foreach ($ultimo_nombramiento as $valor) {
            foreach ($valor as $clave2 => $valor2) {
                if ($clave2 == "id_nombramiento") /*declaro una nueva variable fecha actual y le doy el valor correspondiente para poder medir*/
                    $id_nombramiento_antiguo = $valor2;
            }
        }
        $to = \Carbon\Carbon::createFromFormat('Y-m-d', $request->input('fecha_inicio'));
        $from = \Carbon\Carbon::createFromFormat('Y-m-d', $request->input('fecha_fin'));
        $diff_in_days = $to->diffInDays($from);
        //print_r($diff_in_days); // Output: 1

        //realiza la insercion de nombramiento en la base de datos
        $nombramientos = new Nombramiento;

        $nombramientos->id_puesto = $request->input('id_puesto');
        $nombramientos->fecha_inicio = $request->input('fecha_inicio');
        $nombramientos->fecha_fin = $request->input('fecha_fin');
        $nombramientos->numero_dias = $diff_in_days;
        $nombramientos->tiempo = $request->input('tiempo');
        $nombramientos->tipo = $request->input('tipo');
        $nombramientos->sustituye = $request->input('sustituye');
        $nombramientos->estado = 1;
        $nombramientos->nombramiento_antiguo = $id_nombramiento_antiguo;
        $nombramientos->motivo = $request->input('motivo');

        $nombramientos->save();

        //realiza la insercion de funcionarios_nombramiento en la base de datos
        $funcionarios_nombramientos = new Funcionario_Nombramiento;

        $funcionarios_nombramientos->cedula = $request->input('cedula');
        $funcionarios_nombramientos->id_nombramiento = $nombramientos->id;
        $funcionarios_nombramientos->fecha_nombramiento = $request->input('fecha_fin');

        $funcionarios_nombramientos->save();

        return $funcionarios_nombramientos;
        return $nombramientos;
    }


    /*
   Descripcion: Determina si el funcionario tiene o no tiene un nombramiento activo
   Recibe: Cedula del funcionario
   Envia: Si el funcionario tiene un nombramiento activo, envia 1, si no hay nombramientos activos, 0.
   */
    public function funcionario_nombramiento_ultimo_encontrado($cedula)
    {
        //Funcion para obtener el ultimo nombramiento del usuario con la cedula ingresada
        $ultimo_nombramiento = Funcionario_Nombramiento::where('cedula', '=', $cedula)
            ->orderBy('created_at', 'desc')
            ->first();
        //return $nombramiento->id_nombramiento;
        if ($ultimo_nombramiento == null)
            return 0;
        elseif ($ultimo_nombramiento != null) {
            //Una vez que se tiene el ultimo nombramiento vamos a averiguar si ese nombramiento esta activo o desactivado
            $datos_ultimo_nombramiento = DB::table('funcionarios_nombramientos')
                ->join('nombramientos', 'funcionarios_nombramientos.id_nombramiento', '=', 'nombramientos.id')
                ->where('funcionarios_nombramientos.id_nombramiento', $ultimo_nombramiento->id_nombramiento)
                ->get();
            //return $datos_ultimo_nombramiento;

            foreach ($datos_ultimo_nombramiento as $valor) {
                foreach ($valor as $clave2 => $valor2) {
                    if ($clave2 == "estado")
                        if ($valor2 == 1)
                            return 1;
                        else
                            return 0;
                }
            }
        }
    }


    /*
    Descripcion: Se cambia la fecha de final del nombramiento actual
    Recibe: El json del nombramiento y la fecha de inicio del nombramiento superpuesto
    Envia: Nada
    */
    public function cambiar_fecha_fin($nombramiento, $fecha_ini_nombramiento_superpuesto)
    {
        $nuevafecha = strtotime('-1 day', strtotime($fecha_ini_nombramiento_superpuesto));
        $nuevafecha = date('Y-m-j', $nuevafecha);

        $fecha_inicio = '2018-05-01';//Pronto se va a cambiar nuevamente
        $id_nombramiento_a_actualizar = 0;
        foreach ($nombramiento as $valor) {
            foreach ($valor as $clave2 => $valor2) {
                if ($clave2 == "id") {
                    $id_nombramiento_a_actualizar = $valor2;
                } elseif ($clave2 == "fecha_inicio") {
                    $fecha_inicio = $valor2;
                }

            }
        }
        $fecha_fin = $nuevafecha;

        $to = \Carbon\Carbon::createFromFormat('Y-m-d', $fecha_inicio);
        $from = \Carbon\Carbon::createFromFormat('Y-m-d', $fecha_fin);
        $diff_in_days = $to->diffInDays($from);

        $nombramientos = Nombramiento::where('id', '=', $id_nombramiento_a_actualizar)->first();
        $nombramientos->fecha_fin = $nuevafecha;
        $nombramientos->numero_dias = $diff_in_days;
        $nombramientos->save();
    }


    /*
    Descripcion: Cambia el estado de un nombramiento especifico de 1 a 0
    Recibe: un nombramiento
    Envia: Nada
    */
    public function cambiar_estado($nombramiento)
    {
        $id_nombramiento_a_actualizar = 0;
        foreach ($nombramiento as $valor) {
            foreach ($valor as $clave2 => $valor2) {
                if ($clave2 == "id") {
                    $id_nombramiento_a_actualizar = $valor2;
                }
            }
        }
        $nombramientos = Nombramiento::where('id', '=', $id_nombramiento_a_actualizar)->first();
        $nombramientos->estado = 0;
        $nombramientos->save();
        return;
    }


    /*
    Descripcion: Identifica el funcionario que se desea sustituir
    Recibe: El id de la persona que se va a sustituir
    Envia: El json con la informacion de $nombre_persona_nombrada_a_sustituir;
    */
    public function persona_nombrada_a_sustituir($id_persona_que_se_va)
    {
        $id_nombramiento_a_copiar = $id_persona_que_se_va;
        $nombre_persona_nombrada_a_sustituir = "";

        $persona_nombrada_a_sustituir = DB::table('funcionarios_nombramientos')
            ->join('funcionarios', 'funcionarios_nombramientos.cedula', '=', 'funcionarios.cedula')
            ->where('id_nombramiento', '=', $id_nombramiento_a_copiar)
            ->select('nombre')
            ->get();

        foreach ($persona_nombrada_a_sustituir as $valor) {
            foreach ($valor as $clave2 => $valor2) {
                if ($clave2 == "nombre") /*declaro una nueva variable fecha actual y le doy el valor correspondiente para poder medir*/
                    $nombre_persona_nombrada_a_sustituir = $valor2;
            }
        }
        return $nombre_persona_nombrada_a_sustituir;
    }


    /*
    Descripcion: Vamos a crear un nombramiento nuevo, el cual se le va a cargar para nombrar a alguien en ese nuevo puesto
    Estos nombramientos serán los que las secretarias revisaran para saber a quien deben de nombrar.
    Recibe: el json de datos  del ultimo_nombramiento, $fecha_inicio_nombramiento superpuesto, $fecha_fin_nombramiento superpuesto
    Envia: Nada
    */
    public function crear_nombramiento_huerfano($datos_ultimo_nombramiento, $fecha_inicio_superpuesto, $fecha_fin_superpuesto)
    {
        /*obtengo el nombramiento*/
        $id_nombramiento_a_copiar = 0;
        foreach ($datos_ultimo_nombramiento as $valor) {
            foreach ($valor as $clave2 => $valor2) {
                if ($clave2 == "id") {
                    $id_nombramiento_a_copiar = $valor2;
                }
            }
        }
        /*diferencia de fechas*/
        $to = \Carbon\Carbon::createFromFormat('Y-m-d', $fecha_inicio_superpuesto);
        $from = \Carbon\Carbon::createFromFormat('Y-m-d', $fecha_fin_superpuesto);
        $numero_dias_trabajados = $to->diffInDays($from);

        /*saber a quien estar? sustiyendo en el puesto*/
        $persona_nombrada_a_sustituir = $this->persona_nombrada_a_sustituir($id_nombramiento_a_copiar);

        /*objeto nombramiento*/
        $nombramiento_a_copiar = Nombramiento::where('id', '=', $id_nombramiento_a_copiar)->first();

        $nombramiento = new Nombramiento;

        $nombramiento->id_puesto = $nombramiento_a_copiar->id_puesto;
        $nombramiento->fecha_inicio = $fecha_inicio_superpuesto;
        $nombramiento->fecha_fin = $fecha_fin_superpuesto;
        //$nombramiento->nombramiento_antiguo = $id_nombramiento_a_copiar;
        $nombramiento->numero_dias = $numero_dias_trabajados; /*pensar en un -1*/
        $nombramiento->tiempo = $nombramiento_a_copiar->tiempo;
        $nombramiento->tipo = $nombramiento_a_copiar->tipo;
        $nombramiento->estado = 0;
        $nombramiento->motivo = "Funcionario se traslado a otro puesto.";

        $nombramiento->save();
    }


    /*
    Descripcion: Compara una fecha con respecto a la del día actual
    Recibe: La fecha del nombramiento superpuesto
    Envia: 1 si ya vence, 0 si aún no
    */
    public function compara_fecha($fecha_nombramiento_superpuesto)
    {

        $fecha_nombramiento_superpuesto = strtotime('+0 day', strtotime($fecha_nombramiento_superpuesto));
        $fecha_nombramiento_superpuesto = date('Y-m-j', $fecha_nombramiento_superpuesto);

        $hoy = date("Y-m-d");

        if ($fecha_nombramiento_superpuesto < $hoy)
            return 1; //Ya el nombramiento superpuesto ya vence.
        else
            return 0; //El nombramiento sigue un d?a m?s.
    }

    /*
    Descripcion: Guarda la fecha original del nombramiento $fecha_original
    Recibe: El id del nombramiento al que la persona pertenecia
    Envia: $fecha_original
    */
    public function fecha_original_final($id_nombramiento_antiguo)
    {
        $fecha_original = date("Y-m-d");

        $datos_ultimo_nombramiento = DB::table('nombramientos')
            ->join('funcionarios_nombramientos', 'funcionarios_nombramientos.id_nombramiento', '=', 'nombramientos.id')
            ->where('funcionarios_nombramientos.id_nombramiento', '=', $id_nombramiento_antiguo)
            ->select('fecha_nombramiento')->get();


        foreach ($datos_ultimo_nombramiento as $valor) {
            foreach ($valor as $clave2 => $valor2) {
                if ($clave2 == "fecha_nombramiento")
                    $fecha_original = $valor2;
            }
        }
        return $fecha_original;
    }


    /*
    Descripcion: Crea un nombramiento que serán colocadas en el nombramiento final
    Recibe: la cedula de la persona del nombramiento actual, el $id de ese nombramiento, $fecha_fin_nombramiento_superpuesto yfecha_fin_nombramiento_actual
    Envia:
    */
    //Llamar a esta funcion todos los d?as para averiguar si ya la fecha se vencion
    public function nombramiento_mayor_parte_final($cedula_nombramiento_actual, $id, $fecha_fin_nombramiento_superpuesto, $fecha_fin_nombramiento_actual)
    {
        $fecha_inicio_segunda_mitad_nombramiento = strtotime('+1 day', strtotime($fecha_fin_nombramiento_superpuesto));
        $fecha_inicio_segunda_mitad_nombramiento = date('Y-m-j', $fecha_inicio_segunda_mitad_nombramiento);

        $to = \Carbon\Carbon::createFromFormat('Y-m-d', $fecha_inicio_segunda_mitad_nombramiento);
        $from = \Carbon\Carbon::createFromFormat('Y-m-d', $fecha_fin_nombramiento_actual);
        $numero_dias_trabajados = $to->diffInDays($from);

        $nombramientos_con_antiguo = Nombramiento::where('id', '=', $id)
            ->first();
        //Un JSON con todos los nombramientos que tienen un nombramiento antiguo asociado
        //Creo el nombramiento
        $nombramiento = new Nombramiento;

        $nombramiento->id_puesto = $nombramientos_con_antiguo->id_puesto;
        $nombramiento->fecha_inicio = $fecha_inicio_segunda_mitad_nombramiento;
        $nombramiento->fecha_fin = $fecha_fin_nombramiento_actual;
        $nombramiento->nombramiento_antiguo = null;
        $nombramiento->numero_dias = $numero_dias_trabajados; //*pensar en un -1
        $nombramiento->tiempo = $nombramientos_con_antiguo->tiempo;
        $nombramiento->tipo = $nombramientos_con_antiguo->tipo;
        $nombramiento->estado = 1;
        $nombramiento->motivo = "Retorno a puesto oficial.";

        $nombramiento->save();

        $funcionarios_nombramientos = new Funcionario_Nombramiento;

        $funcionarios_nombramientos->cedula = $cedula_nombramiento_actual;
        $funcionarios_nombramientos->id_nombramiento = $nombramiento->id;
        $funcionarios_nombramientos->fecha_nombramiento = $fecha_fin_nombramiento_actual;

        $funcionarios_nombramientos->save();

        // $this->crear_funcionario_nombramiento($nombramiento, $cedula_nombramiento_actual, $fecha_fin_nombramiento_actual);


    }

}