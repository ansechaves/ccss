<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Conteo;
use App\Funcionario;
use App\Puesto;
use Illuminate\Support\Facades\DB;

class ConteosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    /**
     * Muestra todas los conteos por puesto
     *  @return \Iluminate\Http\Response
     * 
     */
    public function mostarPorPuesto($idPuesto)
    {
        // TODO: pasar a un procedimiento almacenado
        $string = 'select C.[id], C.[horas], C.[motivo], C.[justificacion], P.[nombre] as puesto,
        C.[doctor], C.[numeroBoleta], C.[fecha], F1.[nombre] as sustituto, [funcionarios].[nombre] as ausente 
       from [conteos] as C inner join [puestos] AS P on P.[id] = [id_Puesto] 
       left join [funcionarios] as F1 on F1.[cedula] = [Id_sustituto] 
       left join [funcionarios] on [funcionarios].[cedula] = [id_PersonaAusente] 
       where P.[id] = %d and C.[deleted_at] is null and C.[deleted_at] is null 
       and [funcionarios].[deleted_at] is null';

        $conteo = DB::select(sprintf($string, $idPuesto));


        return response()->json($conteo);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function insertar(Request $request)
    {
        $motivo = $request->input('motivo');
        if ($motivo == 'Incapacidad') {
            $conteo = new Conteo;
            $conteo->horas = $request->input('horas');
            $conteo->motivo = $request->input('motivo');
            $conteo->justificacion = $request->input('justificacion');
            $conteo->doctor = $request->input('doctor');
            $conteo->numeroBoleta = $request->input('numeroBoleta');
            $conteo->Id_sustituto = $request->input('Id_sustituto');
            $conteo->fecha = $request->input('fecha');
            $conteo->id_Puesto = $request->input('id_Puesto');
            // $conteo->id_PersonaAusente = $request->input('id_Puesto');
            $conteo->id_PersonaAusente = $request->input('id_persona_ausente');
            $conteo->save();

        } else {
            $conteo = new Conteo;
            $conteo->horas = $request->input('horas');
            $conteo->motivo = $request->input('motivo');
            $conteo->justificacion = $request->input('justificacion');
            $conteo->Id_sustituto = $request->input('Id_sustituto');
            $conteo->fecha = $request->input('fecha');
            $conteo->id_Puesto = $request->input('id_Puesto');
            // $conteo->id_PersonaAusente = $request->input('id_Puesto');
            $conteo->id_PersonaAusente = $request->input('id_persona_ausente');
            $conteo->save();
        }

        // Datos del funcionario ausente
        $funcionario = Funcionario::find($conteo->id_PersonaAusente);
        // Datos del sustituto
        $sustituto = Funcionario::find($conteo->Id_sustituto);
        // Datos del puesto
        $puesto = Puesto::find($conteo->id_Puesto);

        return response()->json(
            array(
                'ausente' => ($funcionario != null) ? $funcionario->nombre : '',
                'sustituto' => ($sustituto != null) ? $sustituto->nombre : '',
                'horas' => $conteo->horas,
                'fecha' => $conteo->fecha,
                'puesto' => $puesto->nombre,
                'motivo' => $conteo->motivo
            ),
            200
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $conteo = Conteo::find($id);
        $conteo = delete();
        return response()->json(['message' => 'Usuario Eliminado'], 200);
    }


    public function porcentajeMotivoPorServicioPorMes($idServicio, $mes, $annio)
    {
        $string = "select c.motivo, p.nombre as puesto, 
        f.nombre as nombre_funcionario, f.apellido1 as apellido1_funcionario,
        f.apellido2 as apellido2_funcionario, f.cedula as cedula_funcionario
        from servicios as s 
        right join puestos as p on s.id = p.id_servicio
        inner join conteos as c on c.id_Puesto = p.id 
        inner join funcionarios f on f.cedula = c.id_PersonaAusente
        where s.deleted_at is null and p.deleted_at is null 
        and c.deleted_at is null 
        and s.id = %d 
        and day(c.fecha) = '%s'
        and year(c.fecha) = '%s'";

        $Data = DB::select(sprintf($string, $idServicio, $mes, $annio));

        return response()->json($Data);


    }

    public function HorasUsadasPorServicio($idServ, $mes, $annio)
    {
        $string = "select s.nombre, SUM(c.horas) as horasTotales from servicios as s left join puestos as p on p.id_servicio = s.id
        right join conteos as c on p.id = c.id_Puesto where s.id = %d and day(c.fecha) = '%s'
        and year(c.fecha) = '%s' Group by s.nombre";

        $Data = DB::select(sprintf($string, $idServ, $mes, $annio));

        return response()->json($Data);
    }

    public function horasAprobadas($mes, $annio, $idServ)
    {
        $string = "select servicios.nombre, sum(he_mensual_puestos.horas_autorizadas) as HorasExtra, sum(he_adicionales.horas) as HorasExtraAdicionales from servicios
        inner join he_mensual_servicios on servicios.id = he_mensual_servicios.id_servicio 
        INNER JOIN he_mensual_puestos on he_mensual_servicios.id = he_mensual_puestos.id_he_mensual_servicio
        left join he_adicionales on he_mensual_servicios.id = he_adicionales.id_he_mensual_servicio
        where he_mensual_servicios.estado = 'A' and month(he_mensual_servicios.fecha) = '%s' and year(he_mensual_servicios.fecha) = '%s' and servicios.id = %d
        group by servicios.nombre";


        $Data = DB::select(sprintf($string, $mes, $annio, $idServ));
        return response()->json($Data);
    }

    public function ausenciasPorPersona($idPersona)
    {

        $string = "select f.cedula as cedula, f.nombre as nombre_funcionario,
         f.apellido1 as apellido1_funcionario, f.apellido2 as apellido2_funcionario,
         c.horas as horas_utilizadas, c.motivo as motivo, c.justificacion as justificacion,
         c.doctor as doctor, c.numeroBoleta as numero_boleta, c.id_Puesto as id_puesto,
         p.nombre as puesto, p.id_servicio as servicio, c.fecha         
         from funcionarios f inner join conteos c on f.cedula = c.id_PersonaAusente inner join puestos p on p.id = c.id_Puesto
            where f.cedula = %d;";

        $Data = DB::select(sprintf($string, $idPersona));
        return response()->json($Data);
    }

    public function HorasExtraPorPersona($idPersona)
    {
        $string = "select nombre,apellido1,apellido2,motivo,horas 
        from conteos inner join funcionarios 
        on funcionarios.cedula = conteos.Id_sustituto where funcionarios.cedula = %d";

        $Data = DB::select(sprintf($string, $idPersona));
        return response()->json($Data);
    }

}
