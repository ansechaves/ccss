<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use App\HE_Adicional;
use App\HE_Mensual_Servicio;
use App\HE_Mensual_Puesto;
use App\Nombramiento;
use Illuminate\Support\Carbon;

class HorasExtraController extends Controller
{
    /*
     *                  funciones globales obtener fechas.
     **/
    //funciones auxiliares que retornan un objeto con la informacion de la fecha. Ya sea un día, mes o año.
    public static function fechaActual()
    {
        return Carbon::now('America/Costa_Rica');
    }

    /*
     *                  Vista Mostrar Realizar Horas Extra
     **/
    //******************************************************************************************************************
    //Función se necesita para realizar horas extra, pasando un id de usuario como parámetro.
    public function mostrarNombreServicioRealizar($idUsuario)
    {
        $nombreServicio = DB::table('users')
            ->join('funcionarios', 'funcionarios.cedula', '=', 'users.cedula')
            ->join('funcionarios_nombramientos', 'funcionarios_nombramientos.cedula', '=', 'funcionarios.cedula')
            ->join('nombramientos', 'nombramientos.id', '=', 'funcionarios_nombramientos.id_nombramiento')
            ->join('puestos', 'puestos.id', '=', 'nombramientos.id_puesto')
            ->join('servicios', 'servicios.id', '=', 'puestos.id_servicio')
            ->join('he_mensual_servicios', 'he_mensual_servicios.id_servicio', '=', 'servicios.id')
            ->where('users.id', '=', $idUsuario)
            ->whereDate('nombramientos.fecha_inicio', '<', $this->fechaActual())
            ->whereDate('nombramientos.fecha_fin', '>', $this->fechaActual())
            ->whereNull('users.deleted_at')
            ->whereNull('funcionarios.deleted_at')
            ->whereNull('funcionarios_nombramientos.deleted_at')
            ->whereNull('nombramientos.deleted_at')
            ->whereNull('puestos.deleted_at')
            ->whereNull('servicios.deleted_at')
            ->whereNull('he_mensual_servicios.deleted_at')
            ->select(
                'servicios.nombre',
                'servicios.id',
                'he_mensual_servicios.observaciones',
                'he_mensual_servicios.retroalimentacion',
                'he_mensual_servicios.id as id_he_mensual_servicio',
                'he_mensual_servicios.fecha'
            )
            ->orderBy('fecha', 'desc')
            ->first();
        return response()->json($nombreServicio);
    }
    //muestra la lista de puestos asociadas a un id de servicio que se envía como parámetro.
    public function mostrarListaPuestosDeServiciosDeUsuarios($idServicio)
    {
        $puestos = DB::table('puestos')
            ->join('servicios', 'servicios.id', '=', 'puestos.id_servicio')
            ->where('puestos.id_servicio', '=', $idServicio)
            ->whereNull('puestos.deleted_at')
            ->whereNull('servicios.deleted_at')
            ->select('puestos.id', 'puestos.nombre', 'puestos.promedio_salarial')->get();
        if (!$puestos) {
            return response()->json(['message' => 'No existen puestos asociados al servicio.', ], 404);
        }
        return $puestos;
    }
    //muestra el total de horas anuales por cada puesto. Se le envía un id de puesto por parámetro.
    public function mostrarTotalHorasAnualesPorCadaPuesto($idPuesto)
    {
        $totalHorasCadaPuesto = DB::table('puestos')
            ->join('he_anuales', 'he_anuales.id_puesto', '=', 'puestos.id')
            ->where('puestos.id', '=', $idPuesto)
            ->where('he_anuales.anyo', '=', self::fechaActual()->year)
            ->whereNull('puestos.deleted_at')
            ->whereNull('he_anuales.deleted_at')
            ->select('he_anuales.horas')->first();
        if (!$totalHorasCadaPuesto) {
            return response()->json('0');
        }
        return response()->json($totalHorasCadaPuesto->horas);
    }
    //muestra el promedio de horas solicitadas por mes de un puesto en específico.
    public function mostrarPromedioHorasSolicitadasMes($idPuesto)
    {
        $totalPromedioHorasSolicitadas = DB::table('puestos')
            ->join('he_mensual_puestos', 'he_mensual_puestos.id_puesto', '=', 'puestos.id')
            ->where('puestos.id', '=', $idPuesto)
            ->whereNull('puestos.deleted_at')
            ->whereNull('he_mensual_puestos.deleted_at')
            ->avg('he_mensual_puestos.horas_solicitadas');
        if (!$totalPromedioHorasSolicitadas) {
            return response()->json('0');
        }
        return response()->json($totalPromedioHorasSolicitadas);
    }
    //muuestra las horas autorizadas de un puesto del mes anterior.
    public function mostrarHorasAutorizadasMesAnterior($idPuesto)
    {
        $totalPromedioHorasAutorizadas = DB::table('puestos')
            ->join('servicios', 'servicios.id', '=', 'puestos.id_servicio')
            ->join('he_mensual_servicios', 'he_mensual_servicios.id_servicio', '=', 'servicios.id')
            ->join('he_mensual_puestos', 'he_mensual_puestos.id_he_mensual_servicio', '=', 'he_mensual_servicios.id')
            ->where('he_mensual_puestos.id_puesto', '=', $idPuesto)
            ->whereMonth('he_mensual_servicios.fecha', '=', self::fechaActual()->month - 1)
            ->whereYear('he_mensual_servicios.fecha', '=', self::fechaActual()->subMonthNoOverflow()->year)
            ->whereNull('puestos.deleted_at')
            ->whereNull('servicios.deleted_at')
            ->whereNull('he_mensual_servicios.deleted_at')
            ->whereNull('he_mensual_puestos.deleted_at')
            ->select('he_mensual_puestos.horas_autorizadas', 'he_mensual_servicios.fecha')->first();
        if (!$totalPromedioHorasAutorizadas || empty($totalPromedioHorasAutorizadas->horas_autorizadas)) {
            return response()->json('0');
        }
        return response()->json($totalPromedioHorasAutorizadas->horas_autorizadas);
    }
    //Vista realizar horas extra. Pasando un id de usuario por parametro.
    public function mostrarRealizarHorasExtra($idUsuario)
    {
        try {
            $servicio = $this->mostrarNombreServicioRealizar($idUsuario);
            $idServicio = $servicio->original->id;
            $listaPuestos = $this->mostrarListaPuestosDeServiciosDeUsuarios($idServicio);
            if (sizeof($listaPuestos) == 0) {
                return response()->json(['message' => 'No hay puestos asociados a este servicio', ], 404);
            }
            for ($i = 0; $i < sizeof($listaPuestos); $i++) {
                $idPuesto = $listaPuestos[$i]->id;
                $original = $this->mostrarTotalHorasAnualesPorCadaPuesto($idPuesto)->original;
                $listaPuestos[$i]->horas_anuales = $original;
                $listaPuestos[$i]->promedio_horas = $this->mostrarPromedioHorasSolicitadasMes($idPuesto)->original;
                $listaPuestos[$i]->horas_autorizadas_mes_anterior = $this->mostrarHorasAutorizadasMesAnterior($idPuesto)->original;
            }
            $servicio->original->puestos = $listaPuestos;
            return response()->json(array('servicio' => $servicio->original));
        } catch (\Exception $e) {
            return $e->getMessage();//response()->json(['message' => 'El usuario no cuenta con un nombramiento en la fecha actual.'],404);
        }
    }

    //fin de funciones de la vista de realizar horas extra
    //******************************************************************************************************************

    /*
     *                  Vista Listar administracion
     **/
    //******************************************************************************************************************
    //Vista Listar administración
    public function listarServiciosSolicitaHorasExtraAdministracion()
    {
        $nombreServicio = DB::table('servicios')
            ->join('he_mensual_servicios', 'he_mensual_servicios.id_servicio', '=', 'servicios.id')
            ->join('he_mensual_puestos', 'he_mensual_puestos.id_he_mensual_servicio', '=', 'he_mensual_servicios.id')
            ->where('he_mensual_servicios.estado', '=', 'PA')
            ->whereMonth('he_mensual_servicios.fecha', '=', self::fechaActual()->month)
            ->whereYear('he_mensual_servicios.fecha', '=', self::fechaActual()->year)
            ->whereNull('servicios.deleted_at')
            ->whereNull('he_mensual_servicios.deleted_at')
            ->whereNull('he_mensual_puestos.deleted_at')
            ->select(
                'servicios.nombre',
                'servicios.id as id_servicio',
                'he_mensual_servicios.id as id_he_mensual_servicio',
                'he_mensual_servicios.jefe_servicio',
                'he_mensual_servicios.fecha'
            )->get();
        foreach ($nombreServicio as $registro) {
            $horasSolicitadas = HE_Mensual_Puesto::where('id_he_mensual_servicio', '=', $registro->id_he_mensual_servicio)
                ->sum('horas_solicitadas');
            $registro->horas_solicitadas = $horasSolicitadas;
        }
        return response()->json(array('listaServicios' => $nombreServicio));
    }
    //fin vista listar administración
    //******************************************************************************************************************

    /*
     *                  Vista administración
     **/
    //******************************************************************************************************************
    //Vista administración
    //obtiene algunos datos necesarios para la vista de administración.
    public function obtenerInformacionAdicionalAdministracion($idPuesto, $idHEMensualServicio)
    {
        $datos = DB::table('servicios')
            ->join('he_mensual_servicios', 'he_mensual_servicios.id_servicio', '=', 'servicios.id')
            ->join('he_mensual_puestos', 'he_mensual_puestos.id_he_mensual_servicio', '=', 'he_mensual_servicios.id')
            ->where('he_mensual_puestos.id_puesto', '=', $idPuesto)
            ->where('he_mensual_servicios.id', '=', $idHEMensualServicio)
            ->whereNull('servicios.deleted_at')
            ->whereNull('he_mensual_servicios.deleted_at')
            ->whereNull('he_mensual_puestos.deleted_at')
            ->select(
                'he_mensual_puestos.cantidad_empleados',
                'he_mensual_puestos.horas_solicitadas',
                'he_mensual_puestos.justificacion',
                'he_mensual_puestos.id as id_he_mensual_puestos'
            )->first();
        return $datos;
    }

    //Función general que retorna toda la información necesaria para la vista de administración.
    public function mostrarInformacionAdministracion($idHEMensualServicio)
    {
        $servicio = $this->obtenerNombreIDServicio($idHEMensualServicio);
        $listaPuestos = $this->muestraListaDePuestos($idHEMensualServicio);
        for ($i = 0; $i < sizeof($listaPuestos); $i++) {
            $idPuesto = $listaPuestos[$i]->id;
            $listaPuestos[$i]->horas_autorizadas_mes_anterior = $this->mostrarHorasAutorizadasMesAnterior($idPuesto)->original;
            $listaPuestos[$i]->promedio_horas = $this->muestraPromedioHorasSolicitadasMes($idPuesto)->original;
            $obtenerInformacion = $this->obtenerInformacionAdicionalAdministracion($idPuesto, $idHEMensualServicio);
            $listaPuestos[$i]->cantidad_empleados = $obtenerInformacion->cantidad_empleados;
            $listaPuestos[$i]->horas_solicitadas = $obtenerInformacion->horas_solicitadas;
            $listaPuestos[$i]->justificacion = $obtenerInformacion->justificacion;
            $listaPuestos[$i]->id_he_mensual_puestos = $obtenerInformacion->id_he_mensual_puestos;
        }
        $servicio->puestos = $listaPuestos;
        return response()->json($servicio);
    }
    //fin vista administración
    //******************************************************************************************************************

    /*
     *                   Vista aprobación
     */
    //******************************************************************************************************************
    //Vista Aprobación
    //listar los servicios que han solicitado aprobaciones
    public function listarServiciosSolicitaHorasExtraAprobacion()
    {
        $nombreServicio = DB::table('servicios')
            ->join('he_mensual_servicios', 'he_mensual_servicios.id_servicio', '=', 'servicios.id')
            ->join('he_mensual_puestos', 'he_mensual_puestos.id_he_mensual_servicio', '=', 'he_mensual_servicios.id')
            ->where('he_mensual_servicios.estado', '=', 'AP')
            ->whereYear('he_mensual_servicios.fecha', '=', self::fechaActual()->year)
            ->whereMonth('he_mensual_servicios.fecha', '=', self::fechaActual()->month)
            ->whereNull('servicios.deleted_at')
            ->whereNull('he_mensual_servicios.deleted_at')
            ->whereNull('he_mensual_puestos.deleted_at')
            ->select(
                'servicios.nombre',
                'servicios.id as id_servicio',
                'he_mensual_servicios.id as id_he_mensual_servicio',
                'he_mensual_servicios.jefe_servicio',
                'he_mensual_servicios.fecha'
            )->get();
        foreach ($nombreServicio as $registro) {
            $horasSolicitadas = HE_Mensual_Puesto::where('id_he_mensual_servicio', '=', $registro->id_he_mensual_servicio)
                ->sum('horas_solicitadas');
            $horasAutorizadas = HE_Mensual_Puesto::where('id_he_mensual_servicio', '=', $registro->id_he_mensual_servicio)
                ->sum('horas_autorizadas');
            $montoReservado = HE_Mensual_Puesto::where('id_he_mensual_servicio', '=', $registro->id_he_mensual_servicio)
                ->sum('monto_reservado');
            $registro->horas_solicitadas = $horasSolicitadas;
            $registro->horas_autorizadas = $horasAutorizadas;
            $registro->monto_reservado = $montoReservado;
        }
        return response()->json(array('listaServicios' => $nombreServicio));
    }
    // fin vista aprobación
    //******************************************************************************************************************

    /*
     *                   Vista listar presupuesto
     */
    //******************************************************************************************************************
    //Listar presupuesto. Lista los servicios que han solicitado presupuesto
    public function listarServiciosSolicitaHorasExtraPresupuesto()
    {
        $nombreServicio = DB::table('servicios')
            ->join('he_mensual_servicios', 'he_mensual_servicios.id_servicio', '=', 'servicios.id')
            ->join('he_mensual_puestos', 'he_mensual_puestos.id_he_mensual_servicio', '=', 'he_mensual_servicios.id')
            ->where('he_mensual_servicios.estado', '=', 'PP')
            ->whereYear('he_mensual_servicios.fecha', '=', self::fechaActual()->year)
            ->whereMonth('he_mensual_servicios.fecha', '=', self::fechaActual()->month)
            ->whereNull('servicios.deleted_at')
            ->whereNull('he_mensual_servicios.deleted_at')
            ->whereNull('he_mensual_puestos.deleted_at')
            ->select(
                'servicios.nombre',
                'servicios.id as id_servicio',
                'he_mensual_servicios.id as id_he_mensual_servicio',
                'he_mensual_servicios.jefe_servicio',
                'he_mensual_servicios.fecha'
            )->get();
        foreach ($nombreServicio as $registro) {
            $horasSolicitadas = HE_Mensual_Puesto::where('id_he_mensual_servicio', '=', $registro->id_he_mensual_servicio)
                ->sum('horas_solicitadas');
            $horasAutorizadas = HE_Mensual_Puesto::where('id_he_mensual_servicio', '=', $registro->id_he_mensual_servicio)
                ->sum('horas_autorizadas');
            $registro->horas_solicitadas = $horasSolicitadas;
            $registro->horas_autorizadas = $horasAutorizadas;
        }
        return response()->json(array('listaServicios' => $nombreServicio));
    }
    //fin vista listar presupuesto
    //******************************************************************************************************************

    /*
     *                   Vista presupuesto
     */
    //******************************************************************************************************************
    //Vista Presupuesto
    //obtiene algunos datos adiciones que son necesarios para la vista de presupuesto
    public function obtenerInformacionAdicionalPresupuesto($idPuesto, $idHEMensualServicio)
    {
        $datos = DB::table('servicios')
            ->join('he_mensual_servicios', 'he_mensual_servicios.id_servicio', '=', 'servicios.id')
            ->join('he_mensual_puestos', 'he_mensual_puestos.id_he_mensual_servicio', '=', 'he_mensual_servicios.id')
            ->where('he_mensual_puestos.id_puesto', '=', $idPuesto)
            ->where('he_mensual_servicios.id', '=', $idHEMensualServicio)
            ->whereNull('servicios.deleted_at')
            ->whereNull('he_mensual_servicios.deleted_at')
            ->whereNull('he_mensual_puestos.deleted_at')
            ->select(
                'he_mensual_puestos.cantidad_empleados',
                'he_mensual_puestos.horas_solicitadas',
                'he_mensual_puestos.justificacion',
                'he_mensual_puestos.horas_autorizadas',
                'he_mensual_puestos.id as id_he_mensual_puestos'
            )->first();
        return $datos;
    }
    //función general que muestra la información que se necesita enviar en la vista presupuesto.
    public function muestraInformacionPresupuesto($idHEMensualServicio)
    {
        $servicio = $this->obtenerNombreIDServicio($idHEMensualServicio);
        $listaPuestos = $this->muestraListaDePuestos($idHEMensualServicio);
        for ($i = 0; $i < sizeof($listaPuestos); $i++) {
            $idPuesto = $listaPuestos[$i]->id;
            $listaPuestos[$i]->horas_autorizadas_mes_anterior = $this->mostrarHorasAutorizadasMesAnterior($idPuesto)->original;
            $listaPuestos[$i]->promedio_horas = $this->muestraPromedioHorasSolicitadasMes($idPuesto)->original;
            $obtenerInformacionAdicionalPresupuesto = $this->obtenerInformacionAdicionalPresupuesto($idPuesto, $idHEMensualServicio);
            $listaPuestos[$i]->cantidad_empleados = $obtenerInformacionAdicionalPresupuesto->cantidad_empleados;
            $listaPuestos[$i]->horas_solicitadas = $obtenerInformacionAdicionalPresupuesto->horas_solicitadas;
            $listaPuestos[$i]->horas_autorizadas = $obtenerInformacionAdicionalPresupuesto->horas_autorizadas;
            $listaPuestos[$i]->justificacion = $obtenerInformacionAdicionalPresupuesto->justificacion;
            $listaPuestos[$i]->id_he_mensual_puestos = $obtenerInformacionAdicionalPresupuesto->id_he_mensual_puestos;
        }
        $servicio->puestos = $listaPuestos;
        return response()->json($servicio);
    }
    //fin vista presupuesto
    //******************************************************************************************************************

    /*
     *                   Vista correciones
     */
    //******************************************************************************************************************
    //Vista correciones
    //Función se necesita para realizar correcciones a la solicitud, pasando un id de usuario como parámetro.
    public function mostrarNombreServicioCorrecciones($idPuesto)
    {
        $nombreServicio = DB::table('puestos')
            ->join('servicios', 'servicios.id', '=', 'puestos.id_servicio')
            ->join('he_mensual_servicios', 'he_mensual_servicios.id_servicio', '=', 'servicios.id')
            ->where('puestos.id', '=', $idPuesto)
            ->whereMonth('he_mensual_servicios.fecha', '=', self::fechaActual()->month)
            ->whereYear('he_mensual_servicios.fecha', '=', self::fechaActual()->year)
            ->whereNull('puestos.deleted_at')
            ->whereNull('servicios.deleted_at')
            ->whereNull('he_mensual_servicios.deleted_at')
            ->select(
                'servicios.nombre',
                'servicios.id',
                'he_mensual_servicios.observaciones',
                'he_mensual_servicios.retroalimentacion',
                'he_mensual_servicios.id as id_he_mensual_servicio',
                'he_mensual_servicios.fecha'
            )->first();
        return response()->json($nombreServicio);
    }

    public function verificarNombramientosHorasExtra($idUsuario)
    {
        $nombramiento = DB::table('users')
            ->join('funcionarios', 'funcionarios.cedula', '=', 'users.cedula')
            ->join('funcionarios_nombramientos', 'funcionarios_nombramientos.cedula', '=', 'funcionarios.cedula')
            ->join('nombramientos', 'nombramientos.id', '=', 'funcionarios_nombramientos.id_nombramiento')
            ->where('users.id', '=', $idUsuario)
            ->whereDate('nombramientos.fecha_inicio', '<', $this->fechaActual())
            ->whereDate('nombramientos.fecha_fin', '>', $this->fechaActual())
            ->whereNull('users.deleted_at')
            ->whereNull('funcionarios.deleted_at')
            ->whereNull('funcionarios_nombramientos.deleted_at')
            ->whereNull('nombramientos.deleted_at')
            ->select('nombramientos.id_puesto')->first();
        return $nombramiento;
    }

    //obtener información adicional necesaria para la vista de correciones.
    public function obtenerInformacionAdicionalCorrecciones($idPuesto)
    {
        $datos = DB::table('servicios')
            ->join('he_mensual_servicios', 'he_mensual_servicios.id_servicio', '=', 'servicios.id')
            ->join('he_mensual_puestos', 'he_mensual_puestos.id_he_mensual_servicio', '=', 'he_mensual_servicios.id')
            ->where('he_mensual_puestos.id_puesto', '=', $idPuesto)
            ->where('he_mensual_servicios.estado', '=', 'D')
            ->whereNull('servicios.deleted_at')
            ->whereNull('he_mensual_servicios.deleted_at')
            ->whereNull('he_mensual_puestos.deleted_at')
            ->select(
                'he_mensual_servicios.id_servicio',
                'he_mensual_puestos.cantidad_empleados',
                'he_mensual_puestos.horas_solicitadas',
                'he_mensual_puestos.id_puesto',
                'he_mensual_puestos.justificacion',
                'he_mensual_puestos.retroalimentacion',
                'he_mensual_puestos.id as id_he_mensual_puestos'
            )->first();
        return $datos;
    }
    //Función general. ESta es la que muestra toda la información de la vista correciones.
    public function muestraCorrecciones($idUsuario)
    {
        $idPuesto = $this->verificarNombramientosHorasExtra($idUsuario);
        if (!$idPuesto) {
            return response()->json(['message' => 'El usuario no cuenta con un nombramiento en la fecha actual.'], 404);
        }
        try {
            $servicio = $this->mostrarNombreServicioCorrecciones($idPuesto->id_puesto);
            $idServicio = $servicio->original->id;
            $listaPuestos = $this->mostrarListaPuestosDeServiciosDeUsuarios($idServicio);
            if (sizeof($listaPuestos) == 0) {
                return response()->json(['message' => 'No hay puestos asociados a este servicio', ], 404);
            }
            for ($i = 0; $i < sizeof($listaPuestos); $i++) {
                $idPuesto = $listaPuestos[$i]->id;
                $original = $this->mostrarTotalHorasAnualesPorCadaPuesto($idPuesto)->original;
                $listaPuestos[$i]->horas_anuales = $original;
                $listaPuestos[$i]->promedio_horas = $this->mostrarPromedioHorasSolicitadasMes($idPuesto)->original;
                $listaPuestos[$i]->horas_autorizadas_mes_anterior = $this->mostrarHorasAutorizadasMesAnterior($idPuesto)->original;
                $obtenerInformacion = $this->obtenerInformacionAdicionalCorrecciones($idPuesto);
                if (!$obtenerInformacion) {
                    return response()->json(['message' => 'La solicitud actual no cuenta con correcciones hasta el momento.'], 404);
                }
                $listaPuestos[$i]->cantidad_empleados = $obtenerInformacion->cantidad_empleados;
                $listaPuestos[$i]->horas_solicitadas = $obtenerInformacion->horas_solicitadas;
                $listaPuestos[$i]->justificacion = $obtenerInformacion->justificacion;
                $listaPuestos[$i]->retroalimentacion = $obtenerInformacion->retroalimentacion;
                $listaPuestos[$i]->id_he_mensual_puestos = $obtenerInformacion->id_he_mensual_puestos;
            }
            $servicio->original->puestos = $listaPuestos;
            return response()->json(array('servicio' => $servicio->original));
        } catch (\Exception $e) {
            return response()->json(['message' => 'Aún no se han realizado solicitudes de horas extras para el mes actual.'], 404);
        }
    }
    //fin vista correciones
    //******************************************************************************************************************

    /*
     *                   Funciones globales.
     */
    //******************************************************************************************************************
    //esta función obtiene el nombre y el id de servicio cuando se le envía un idHEMensualServicio
    public static function obtenerNombreIDServicio($idHEMensualServicio)
    {
        $nombreServicio = DB::table('servicios')
            ->join('he_mensual_servicios', 'he_mensual_servicios.id_servicio', '=', 'servicios.id')
            ->where('he_mensual_servicios.id', '=', $idHEMensualServicio)
            ->whereNull('servicios.deleted_at')
            ->whereNull('he_mensual_servicios.deleted_at')
            ->select(
                'servicios.nombre as nombre_servicio',
                'servicios.id as id_servicio',
                'he_mensual_servicios.observaciones',
                'he_mensual_servicios.jefe_servicio',
                'he_mensual_servicios.fecha'
            )->first();
        return $nombreServicio;
    }

    //muestra la lista de puestos a partir de un idHeMensualServicio
    public static function muestraListaDePuestos($idHEMensualServicio)
    {
        $fechaSolicitud = HE_Mensual_Servicio::where('he_mensual_servicios.id', '=', $idHEMensualServicio)
            ->select('he_mensual_servicios.fecha')
            ->first();
        $anyo = Carbon::parse($fechaSolicitud->fecha)->year;
        $puestos = DB::table('he_anuales')
            ->join('puestos', 'puestos.id', '=', 'he_anuales.id_puesto')
            ->join('he_mensual_puestos', 'he_mensual_puestos.id_puesto', '=', 'puestos.id')
            ->where('he_mensual_puestos.id_he_mensual_servicio', '=', $idHEMensualServicio)
        //    ->where('he_anuales.anyo', '=',$anyo)
            ->whereNull('he_anuales.deleted_at')
            ->whereNull('puestos.deleted_at')
            ->whereNull('he_mensual_puestos.deleted_at')
            ->select(
                'puestos.id',
                'puestos.nombre as nombre_puesto',
                'puestos.promedio_salarial',
                'he_anuales.horas as horas_anuales'
            )->get();
        return $puestos;
    }
    //muestra el promedio de horas solicitadas por mes, enviandole un idPuesto
    public static function muestraPromedioHorasSolicitadasMes($idPuesto)
    {
        $totalPromedioHorasSolicitadas = DB::table('puestos')
            ->join('he_mensual_puestos', 'he_mensual_puestos.id_puesto', '=', 'puestos.id')
            ->where('puestos.id', '=', $idPuesto)
            ->whereNull('puestos.deleted_at')
            ->whereNull('he_mensual_puestos.deleted_at')
            ->avg('he_mensual_puestos.horas_solicitadas');
        if (!$totalPromedioHorasSolicitadas) {
            return response()->json('0');
        }
        return response()->json($totalPromedioHorasSolicitadas);
    }
    //fin funciones globales
    //******************************************************************************************************************

    /*
     *                  Funciones actualizar e insertar
     */
    //******************************************************************************************************************
    //Actualiza la información de presupuesto. (Vista presupuesto)
    public function actualizarPresupuesto(Request $request)
    {
        $he_mensual_servicio = HE_Mensual_Servicio::findOrFail($request->id_he_mensual_servicio);
        $he_mensual_servicio->update(array('estado' => $request->estado));
        $listaHEMensualPuestos = json_decode(json_encode($request->solicitudes), true);
        for ($i = 0; $i < sizeof($listaHEMensualPuestos); $i++) {
            $idHEMensualPuestos = $listaHEMensualPuestos[$i]['id_he_mensual_puestos'];
            $montoReservado = $listaHEMensualPuestos[$i]['monto_reservado'];
            $he_mensual_puestos = HE_Mensual_Puesto::findOrFail($idHEMensualPuestos);
            $he_mensual_puestos->update(array('monto_reservado' => $montoReservado));
        }
        return response()->json(['success' => 'success'], 200);
    }
    //Actualiza la información de las correciones. (Vista correción)
    public function actualizarCorrecciones(Request $request)
    {
        $he_mensual_servicio = HE_Mensual_Servicio::findOrFail($request->id_he_mensual_servicio);
        $he_mensual_servicio->update(array(
            'observaciones' => $request->observaciones,
            'estado' => $request->estado
        ));
        $he_mensual_servicio->save();
        $listaHEMensualPuestos = json_decode(json_encode($request->solicitudes), true);
        for ($i = 0; $i < sizeof($listaHEMensualPuestos); $i++) {
            $idHEMensualPuestos = $listaHEMensualPuestos[$i]['id_he_mensual_puestos'];
            $justificacion = $listaHEMensualPuestos[$i]['justificacion'];
            $horas_solicitadas = $listaHEMensualPuestos[$i]['horas_solicitadas'];
            $cantidad_empleados = $listaHEMensualPuestos[$i]['cantidad_empleados'];
            $he_mensual_puestos = HE_Mensual_Puesto::findOrFail($idHEMensualPuestos);
            $he_mensual_puestos->update(array(
                'justificacion' => $justificacion,
                'horas_solicitadas' => $horas_solicitadas,
                'cantidad_empleados' => $cantidad_empleados
            ));
            $he_mensual_puestos->save();
        }
    }
    //Insertar información en realizar
    public function insertarRealizarHorasExtra(Request $request)
    {
        $existeRegistro = DB::table('he_mensual_servicios')
            ->where('he_mensual_servicios.id_servicio', '=', $request->id_servicio)
            ->whereYear('he_mensual_servicios.fecha', '=', self::fechaActual()->year)
            ->whereMonth('he_mensual_servicios.fecha', '=', self::fechaActual()->month)
            ->whereNull('he_mensual_servicios.deleted_at')
            ->select('he_mensual_servicios.id as id')->first();

        if (!$existeRegistro || (HE_Adicional::where('he_adicionales.id_he_mensual_servicio', '=', $existeRegistro->id)->first())) {
            $he_mensual_servicio = new HE_Mensual_Servicio();
            $he_mensual_servicio->observaciones = $request->observaciones;
            $he_mensual_servicio->fecha = Carbon::now('America/Costa_Rica');
            $he_mensual_servicio->estado = $request->estado;
            $he_mensual_servicio->id_servicio = $request->id_servicio;
            $he_mensual_servicio->jefe_servicio = $request->jefe_servicio;
            $he_mensual_servicio->save();
            $solicitudes = $request->solicitudes;
            $listaHEMensualPuestos = json_decode(json_encode($solicitudes), true);
            for ($i = 0; $i < sizeof($listaHEMensualPuestos); $i++) {
                $puesto = new HE_Mensual_Puesto();
                $puesto->id_he_mensual_servicio = $he_mensual_servicio->id;
                $puesto->id_puesto = $listaHEMensualPuestos[$i]['id_puesto'];
                $puesto->justificacion = $listaHEMensualPuestos[$i]['justificacion'];
                $puesto->horas_solicitadas = $listaHEMensualPuestos[$i]['horas_solicitadas'];
                $puesto->cantidad_empleados = $listaHEMensualPuestos[$i]['cantidad_empleados'];
                $puesto->save();
            }
            return response()->json(['success' => 'success'], 200);
        }
        return response()->json(['message' => 'Ya se ha realizado una solicitud para el mes actual'], 406);
    }

    //Vista administración, aprobar solicitud. Se recibe la cantidad de horas solicitadas para cada puesto
    //además, el estado cambia.
    public function actualizarAprobarSolicitud(Request $request)
    {
        $he_mensual_servicio = HE_Mensual_Servicio::findOrFail($request->id_he_mensual_servicio);
        $he_mensual_servicio->update(array('estado' => $request->estado));
        $listaHEMensualPuestos = json_decode(json_encode($request->solicitudes), true);
        for ($i = 0; $i < sizeof($listaHEMensualPuestos); $i++) {
            $idHEMensualPuestos = $listaHEMensualPuestos[$i]['id_he_mensual_puestos'];
            $horas_autorizadas = $listaHEMensualPuestos[$i]['horas_autorizadas'];
            $he_mensual_puestos = HE_Mensual_Puesto::findOrFail($idHEMensualPuestos);
            $he_mensual_puestos->update(array('horas_autorizadas' => $horas_autorizadas));
        }
        return response()->json(['success' => 'success'], 200);
    }
    //Vista administración, rechazar solicitud. Envia una retroalimentación para cada puesto que se rechaza.
    public function actualizarRechazarSolicitud(Request $request)
    {
        $he_mensual_servicio = HE_Mensual_Servicio::findOrFail($request->id_he_mensual_servicio);
        $he_mensual_servicio->update(array('estado' => $request->estado, 'retroalimentacion' => $request->observaciones));
        $listaHEMensualPuestos = json_decode(json_encode($request->solicitudes), true);
        for ($i = 0; $i < sizeof($listaHEMensualPuestos); $i++) {
            $idHEMensualPuestos = $listaHEMensualPuestos[$i]['id_he_mensual_puestos'];
            if (isset($listaHEMensualPuestos[$i]['retroalimentacion'])) {
                $retroalimentacion = $listaHEMensualPuestos[$i]['retroalimentacion'];
                $he_mensual_puestos = HE_Mensual_Puesto::findOrFail($idHEMensualPuestos);
                $he_mensual_puestos->update(array('retroalimentacion' => $retroalimentacion));
                continue;
            }
        }
        return response()->json(['success' => 'success'], 200);
    }

    public function actualizarEstadoAprobacion(Request $request)
    {
        $he_mensual_servicio = HE_Mensual_Servicio::findOrFail($request->id_he_mensual_servicio);
        $he_mensual_servicio->update(array('estado' => $request->estado));
        return response()->json(['success' => 'success'], 200);
    }
    //fin funciones de actualizar e insertar
    //******************************************************************************************************************
}
