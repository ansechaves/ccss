<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Funcionario;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
class UsersController extends Controller
{
    //Obtener Usuarios
    public function obtener()
    {
        // consulta que combia las tablas de usuario y acceso_ usuario y funcionario.
        $users = User::select(DB::raw("CONCAT(funcionarios.nombre,' ',funcionarios.apellido1,' ',funcionarios.apellido2) as nombre"),'users.id','users.cedula','users.nombre_usuario','acceso_usuarios.STEPI','acceso_usuarios.SER')
        ->join('funcionarios','users.cedula','=','funcionarios.cedula')
        ->leftJoin('acceso_usuarios','users.id','=','acceso_usuarios.id_usuario') 
        ->get();

        return $users;
    }
    //Obtener funcionarios para el select a la hora de crear usuario
    public function funcionarios()
    {
        //consulta que devuelve los nombres de los usuarios con nombre
        $funcionarios = Funcionario::select(DB::raw("CONCAT(nombre,' ',apellido1,' ',apellido2) as nombre"),'cedula')
        ->get();

        return $funcionarios;
    }
    //insertar usuarios
    public function insertar(Request $request)
    {
        $stepi = $request->input('STEPI');
        $ser = $request->input('SER');
        if($request->input('STEPI') == ''){
            $stepi = '';
        }
        if($request->input('SER') == ''){
            $ser = 0;
        }
        // se agrega el usuario a la tabla de usuarios, y se guarda el la variable id, el nuevo id del usuario.
        $id = DB::table('users')->insertGetId(
            ['cedula' =>  $request->input('cedula'), 'nombre_usuario' => $request->input('nombre_usuario'),'password'=>Hash::make($request->input('cedula'))]);
        // se agregan los accesos al usuario recientemente agregado
        DB::table('acceso_usuarios')->insert(['id_usuario' =>$id,'STEPI' =>$stepi,'SER'=>$ser]);
        // se busca el usuario que se acaba de agregar 
        $usuario = User::select(DB::raw("CONCAT(funcionarios.nombre,' ',funcionarios.apellido1,' ',funcionarios.apellido2) as nombre"),'users.id','users.cedula','users.nombre_usuario','acceso_usuarios.STEPI','acceso_usuarios.SER')
        ->join('funcionarios','users.cedula','=','funcionarios.cedula')
        ->leftJoin('acceso_usuarios','users.id','=','acceso_usuarios.id_usuario') 
        ->where('id', '=', $id)
        ->get();
        // se devuelve el usuario que se acaba de agregar
        return  response()->json($usuario);
    }
    public function eliminar($id)
    {
        $usuario=User::find($id);
        $usuario->delete();
        return response()->json(['message'=>'Usuario Eliminado'],200);
    }
    // modificar usuarios
    public function modificar(Request $request)
    {
        $stepi = $request->input('STEPI');
        $ser = $request->input('SER');
        if($request->input('STEPI') == ''){
            $stepi = "0";
        }
        if($request->input('SER') == ''){
            $ser = 0;
        }
        // actualiza la tabla usuarios
        DB::table('users')->where('id',$request->input('id'))
        ->update(['nombre_usuario' => $request->input('nombre_usuario')]);
        // actualiza la tabla de acceso de usuarios
        DB::table('acceso_usuarios')->where('id_usuario',$request->input('id'))
        ->update(['STEPI' => $stepi,'SER'=>$ser]);

        // se busca el usuario que se acaba de modificar en la base de datos
        $usuario = User::select(DB::raw("CONCAT(funcionarios.nombre,' ',funcionarios.apellido1,' ',funcionarios.apellido2) as nombre"),'users.id','users.cedula','users.nombre_usuario','acceso_usuarios.STEPI','acceso_usuarios.SER')
        ->join('funcionarios','users.cedula','=','funcionarios.cedula')
        ->leftJoin('acceso_usuarios','users.id','=','acceso_usuarios.id_usuario') 
        ->where('id', '=', $request->input('id'))
        ->get();
        // se devuelve el usuario que se acaba de modificar
        return  response()->json($usuario);
    }



}
