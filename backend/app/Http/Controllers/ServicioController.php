<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\Servicio;
use App\Puestos;


class ServicioController extends Controller
{
    /*
Descripcion: Funcion que obtiene todos los servicios existentes. Por lo que no recibe nada por
    parametro y retorna todos los servicios*/
    public function mostrarServicios()
    {
        $servicio = Servicio::all();
        return response()->json($servicio);
    }

    public function obtenerServiciosPuestos()
    {
        $puestos = DB::table('servicios')
            ->select('*')
            ->orderBy('nombre', 'asc')
            ->get();

        return $puestos;
    }

    public function agregarServicios($nombre, $codigo, $descripcion)
    {
        $servicio = new Servicio;
        $servicio->nombre = $nombre;
        $servicio->codigo = $codigo;
        $servicio->descripcion = $descripcion;
        $servicio->save();
    }
    /*
Descripcion: Funcion que crea servicios. Recibe por parametro los datos ingresados por el usuario,
    los cuales son, el nombre, el codigo y la descripcion del servicio y guarda los datos ingresados.*/

    public function crearServicios(Request $request)
    {
        $servicio = new Servicio;
        $servicio->nombre = $request->input('nombre');
        $servicio->codigo = $request->input('codigo');
        $servicio->descripcion = $request->input('descripcion');
        $servicio->save();
        return $servicio;
    }
    /*
Descripcion: Funcion que modifica un servicio en especifico. Recibe por parametro el servicio y el id del mismo
    y guarda los datos modificados.*/

    public function modificarServicios(Request $request, $id)
    {
        $servicio = Servicio::where('id', '=', $id)->first();
        $servicio->nombre = $request->input('nombre');
        $servicio->codigo = $request->input('codigo');
        $servicio->descripcion = $request->input('descripcion');
        $servicio->save();
        return $servicio;
    }

    /*
Descripcion: Funcion que elimina un servicio en especifico. Recibe por parametro el id del servicio a eliminar
     y elimina el servicio.*/

    public function eliminarServicios($id)
    {
        $servicio = Servicio::find($id);
        $servicio->delete();
        return $servicio;
    }


    /*
    El sistema mostrará un listado de los puestos pertenecientes a dicho servicio, ordenándolos en orden alfabético.
    Mediante una búsqueda por nombre de servicio o código del servicio con el siguiente formato: ______________
    Nombre del puesto
    Código del puesto con el siguiente formato: ______________
    El sistema debe mostrar más información al seleccionar un puesto:
    Se muestra la información del informe por puesto
     */

    //informe por servicio
    public function informePorServicio($codigoServicio)
    {

        /*
            seleccionar los puestos que pertenezcan al servicio X
         */

        /*
                $nombramientosExistentesEnPuesto = DB::table('puestos')
                        ->where('id_servicio', $codigoServicio)
                        //->join('servicios','puestos.id_servicio','servicios.codigo')
                        ->join('servicios','puestos.id_servicio','servicios.codigo')
                        ->select('puestos.nombre', 'puestos.codigo')
                        ->get(); */

        $id_serv = DB::table('servicios')
            ->where('codigo', $codigoServicio)
            ->select('id')
            ->get();
        //echo $id_serv[0]->id;

        $nombramientosExistentesEnPuesto = DB::table('puestos')
            ->where('id_servicio', $id_serv[0]->id) //$codigoServicio  $id_serv[0]->id
            //->join('servicios','puestos.id_servicio','servicios.codigo')
            //->join('servicios','puestos.id_servicio','servicios.codigo')
            ->select('puestos.nombre', 'puestos.codigo')
            ->get();

        echo $nombramientosExistentesEnPuesto;
        //echo json_encode($informacionPuestos);
    }



    function obtenerNombreYCodigoServicio()
    {

        $listaObtenerNombreYCodigoServicio = DB::table('servicios')
            ->select('servicios.nombre', 'servicios.codigo')
            ->get();

        echo (json_encode($listaObtenerNombreYCodigoServicio));

        //return response()->json($listaObtenerNombreYCodigoPuesto);
    }

}

