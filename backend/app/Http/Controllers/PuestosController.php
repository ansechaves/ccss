<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use App\Puesto;
use Illuminate\Support\Facades\Log;


class PuestosController extends Controller
{
    public function obtenerListaPuestos() {
       
        $puestos = DB::table('puestos')
            ->join('servicios', 'servicios.id', '=', 'puestos.id_servicio')
            ->whereNull('puestos.deleted_at')
            ->select(
                'puestos.id',
                'puestos.nombre',
                'puestos.descripcion',
                'puestos.id_servicio',
                'puestos.codigo',
                'servicios.nombre as nombre_servicio',
                'puestos.promedio_salarial')
            ->orderBy('puestos.nombre', 'asc')
            ->get();
        return $puestos;
    }
    public function obtenerInfoPuesto($id){
        $puesto = Puesto::where('id', $id)->first();
        return $puesto;
    }

    public function crearPuesto(Request $request) {
        $puesto = new Puesto;
        $puesto->id_servicio = $request->input('id_servicio');
        $puesto->nombre = $request->input('nombre');
        $puesto->codigo = $request->input('codigo');
        $puesto->promedio_salarial = $request->input('promedio_salarial');
        $puesto->descripcion = $request->input('descripcion');
        $puesto->save();
        return $puesto;
    }

    public function editarPuesto(Request $request, $id) {
        
        $puesto = Puesto::find($id);
        $puesto->id_servicio = $request->input('id_servicio');
        $puesto->nombre = $request->input('nombre');
        $puesto->codigo = $request->input('codigo');
        $puesto->promedio_salarial = $request->input('promedio_salarial');
        $puesto->descripcion = $request->input('descripcion');
        $puesto->save();
        return $puesto;
    }
    
    public function eliminarPuesto($id) {
        $puesto = Puesto::find($id);
        $puesto->delete();
        return $puesto;
    }

    public function obtenerServicios() {
        $servicios = DB::table('servicios')
        ->whereNull('deleted_at')
        ->select('*')
        ->orderBy('nombre', 'asc')
        ->get();     
        return $servicios;
    }


    function obtenerNombreYCodigoPuesto(){

        $listaObtenerNombreYCodigoPuesto = DB::table('puestos')
            ->select('puestos.nombre', 'puestos.codigo')
            ->get();

        echo(json_encode($listaObtenerNombreYCodigoPuesto));

        //return response()->json($listaObtenerNombreYCodigoPuesto);

    }
}
