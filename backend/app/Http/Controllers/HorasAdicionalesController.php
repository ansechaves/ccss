<?php

namespace App\Http\Controllers;

use App\HE_Adicional;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use App\HE_Mensual_Servicio;
use App\HE_Mensual_Puesto;
use App\Nombramiento;
use Illuminate\Support\Carbon;

class HorasAdicionalesController extends Controller
{
    /*
    *                  funciones globales obtener fechas.
    **/
    //funciones auxiliares que retornan un objeto de tipo date con la informacion de la fecha
    public static function fechaActual(){return Carbon::now('America/Costa_Rica');}

    /*
     *                  Vista Mostrar Realizar Horas Extra
     **/
    //******************************************************************************************************************
    //Función se necesita para realizar horas extra, pasando un id de usuario como parámetro.
    public function mostrarNombreServicioRealizar($idUsuario){
        $nombreServicio = DB::table('users')
            ->join('funcionarios', 'funcionarios.cedula', '=', 'users.cedula')
            ->join('funcionarios_nombramientos', 'funcionarios_nombramientos.cedula', '=', 'funcionarios.cedula')
            ->join('nombramientos', 'nombramientos.id', '=', 'funcionarios_nombramientos.id_nombramiento')
            ->join('puestos', 'puestos.id', '=', 'nombramientos.id_puesto')
            ->join('servicios','servicios.id','=','puestos.id_servicio')
            ->join('he_mensual_servicios','he_mensual_servicios.id_servicio','=','servicios.id')
            ->where('users.id','=',$idUsuario)
            ->whereDate('nombramientos.fecha_inicio','<',$this->fechaActual())
            ->whereDate('nombramientos.fecha_fin','>',$this->fechaActual())
            ->whereNull('users.deleted_at')
            ->whereNull('funcionarios.deleted_at')
            ->whereNull('funcionarios_nombramientos.deleted_at')
            ->whereNull('nombramientos.deleted_at')
            ->whereNull('puestos.deleted_at')
            ->whereNull('servicios.deleted_at')
            ->whereNull('he_mensual_servicios.deleted_at')
            ->select('servicios.nombre','servicios.id','he_mensual_servicios.observaciones',
                'he_mensual_servicios.retroalimentacion')->first();
        return response()->json($nombreServicio);
    }
    //muestra la lista de puestos asociadas a un id de servicio que se envía como parámetro.
    public function mostrarListaPuestosDeServiciosDeUsuarios($idServicio){
        $puestos = DB::table('puestos')
            ->join('servicios', 'servicios.id','=','puestos.id_servicio')
            ->where('puestos.id_servicio','=',$idServicio)
            ->whereNull('puestos.deleted_at')
            ->whereNull('servicios.deleted_at')
            ->select('puestos.id','puestos.nombre','puestos.promedio_salarial')->get();
        if(!$puestos){
            return response()->json(['message' => 'No existen puestos asociados al servicio.',], 404);
        }
        return $puestos;
    }
    //muestra el total de horas anuales por cada puesto. Se le envía un id de puesto por parámetro.
    public function mostrarTotalHorasAnualesPorCadaPuesto($idPuesto){
        $totalHorasCadaPuesto = DB::table('puestos')
            ->join('he_anuales', 'he_anuales.id_puesto', '=', 'puestos.id')
            ->where('puestos.id','=',$idPuesto)
            ->whereNull('puestos.deleted_at')
            ->whereNull('he_anuales.deleted_at')
            ->select('he_anuales.horas')->first();
        if (!$totalHorasCadaPuesto) {
            return response()->json('0');
        }
        return response()->json($totalHorasCadaPuesto->horas);
    }
    //muestra el promedio de horas solicitadas por mes de un puesto en específico.
    public function mostrarPromedioHorasSolicitadasMes($idPuesto){
        $totalPromedioHoras = DB::table('puestos')
            ->join('he_adicionales', 'he_adicionales.id_puesto', '=', 'puestos.id')
            ->where('puestos.id','=',$idPuesto)
            ->whereNull('puestos.deleted_at')
            ->whereNull('he_adicionales.deleted_at')
            ->avg('he_adicionales.horas');
        if (!$totalPromedioHoras) {
            return response()->json('0');
        }
        return  response()->json($totalPromedioHoras);
    }
    //función que muestra la cantidad de personas que tiene un puesto. Dando el ID del puesto
    public function mostrarCantidadEmpleadosPorPuesto($idPuesto){
        $cantidadEmpleados = DB::table('he_mensual_servicios')
            ->join('he_adicionales','he_adicionales.id_he_mensual_servicio','=','he_mensual_servicios.id')
            ->where('he_adicionales.id_puesto','=',$idPuesto)
            ->whereMonth('he_mensual_servicios.fecha','=',self::fechaActual()->month-1)
            ->whereYear('he_mensual_servicios.fecha','=',self::fechaActual()->subMonthNoOverflow()->year)
            ->whereNull('he_mensual_servicios.deleted_at')
            ->whereNull('he_adicionales.deleted_at')
            ->select('he_adicionales.cantidad_empleados')->first();
        if(!$cantidadEmpleados){
            return ('0');
        }
        return ($cantidadEmpleados->cantidad_empleados);
    }
    //muuestra las horas autorizadas de un puesto del mes anterior.
    public function mostrarHorasAutorizadasMesAnterior($idPuesto)
    {
        $totalPromedioHorasAutorizadas = DB::table('puestos')
            ->join('servicios','servicios.id','=','puestos.id_servicio')
            ->join('he_mensual_servicios','he_mensual_servicios.id_servicio','=','servicios.id')
            ->join('he_mensual_puestos','he_mensual_puestos.id_he_mensual_servicio','=','he_mensual_servicios.id')
            ->where('he_mensual_puestos.id_puesto','=',$idPuesto)
            ->whereMonth('he_mensual_servicios.fecha', '=', self::fechaActual()->month-1) //cantidad de horas autorizadas mes actual
            ->whereYear('he_mensual_servicios.fecha','=',self::fechaActual()->subMonthNoOverflow()->year)
            ->whereNull('puestos.deleted_at')
            ->whereNull('servicios.deleted_at')
            ->whereNull('he_mensual_servicios.deleted_at')
            ->whereNull('he_mensual_puestos.deleted_at')
            ->select('he_mensual_puestos.horas_autorizadas')->first();
        if (!$totalPromedioHorasAutorizadas || empty($totalPromedioHorasAutorizadas->horas_autorizadas)){
            return ('0');
        }
        return ($totalPromedioHorasAutorizadas->horas_autorizadas);
    }
    //Vista realizar horas extra. Pasando un id de usuario por parametro.
    public function mostrarRealizarHorasAdicionales($idUsuario)
    {
        try {
            $servicio = $this->mostrarNombreServicioRealizar($idUsuario);
            $idServicio = $servicio->original->id;
            $listaPuestos = $this->mostrarListaPuestosDeServiciosDeUsuarios($idServicio);
            if(sizeof($listaPuestos)==0){
                return response()->json(['message' => 'No hay puestos asociados a este servicio',], 404);
            }
            for ($i=0; $i < sizeof($listaPuestos); $i++) {
                $idPuesto = $listaPuestos[$i]->id;
                $mostrarCantidadEmpleadosPorPuesto= $this->mostrarCantidadEmpleadosPorPuesto($idPuesto);
                $listaPuestos[$i]->cantidad_empleados = $mostrarCantidadEmpleadosPorPuesto;
                $listaPuestos[$i]->horas_anuales = $this->mostrarTotalHorasAnualesPorCadaPuesto($idPuesto)->original;
                $listaPuestos[$i]->promedio_horas = $this->mostrarPromedioHorasSolicitadasMes($idPuesto)->original;
                $listaPuestos[$i]->horas_autorizadas_mes_anterior = $this->mostrarHorasAutorizadasMesAnterior($idPuesto);
            }
            $servicio->original->puestos = $listaPuestos;
            return response()->json(array('servicio'=>$servicio->original));
        }
        catch (\Exception $e) {
            return response()->json(['message' => 'El usuario no cuenta con un nombramiento en la fecha actual.'],404);
        }
    }
    //Insertar información en realizar
    public function insertarRealizarHorasAdicionales(Request $request){
        $existeRegistro = HE_Mensual_Servicio::where('he_mensual_servicios.id_servicio','=',$request->id_servicio)
                    ->join('he_adicionales','he_adicionales.id_he_mensual_servicio','=','he_mensual_servicios.id')
                    ->whereMonth('he_mensual_servicios.fecha','=',self::fechaActual()->month-1)
                    ->whereYear('he_mensual_servicios.fecha','=',self::fechaActual()->subMonthNoOverflow()->year)
                    ->whereNull('he_mensual_servicios.deleted_at')
                    ->whereNull('he_adicionales.deleted_at')
                    ->first();
        if(!$existeRegistro){
            $he_mensual_servicio = new HE_Mensual_Servicio();
            $he_mensual_servicio->observaciones = $request->observaciones;
            // saca la fecha con el mes anterior
            $he_mensual_servicio->fecha = self::fechaActual()->subMonthNoOverflow();
            $he_mensual_servicio->estado = $request->estado;
            $he_mensual_servicio->id_servicio = $request->id_servicio;
            $he_mensual_servicio->jefe_servicio = $request->jefe_servicio;
            $he_mensual_servicio->save();
            $solicitudes = $request->solicitudes;
            $listaHEAdicionales = json_decode(json_encode($solicitudes),true);
            for($i = 0; $i<sizeof($listaHEAdicionales); $i++){
                $he_adicional = new HE_Adicional();
                $he_adicional->cantidad_empleados = $listaHEAdicionales[$i]['cantidad_empleados'];
                $he_adicional->id_puesto = $listaHEAdicionales[$i]['id_puesto'];
                $he_adicional->justificacion = $listaHEAdicionales[$i]['justificacion'];
                $he_adicional->horas = $listaHEAdicionales[$i]['horas'];
                $he_adicional->id_he_mensual_servicio = $he_mensual_servicio->id;
                $he_adicional->save();
            }
            return response()->json(['success' => 'success'], 200);
        }
        return response()->json(['message' => 'Ya se ha realizado una solicitud de horas adicionales para el mes anterior'], 406);
    }
    //fin de funciones de la vista de realizar horas extra
    //******************************************************************************************************************


    /*
     *                  Vista Listar administracion
     **/
    //******************************************************************************************************************
    //Vista Listar administración
    public function listarServiciosSolicitaHorasAdicionalesAdministracion()
    {
        $nombreServicio = DB::table('servicios')
            ->join('he_mensual_servicios','he_mensual_servicios.id_servicio', '=', 'servicios.id')
            ->join('he_adicionales','he.adicionales.id_he_mensual_servicio','=','he_mensual_servicios.id')
            ->where('he_mensual_servicios.estado','=','PA')
            ->whereYear('he_mensual_servicios.fecha', '=', self::fechaActual()->year)
            ->whereMonth('he_mensual_servicios.fecha', '=', self::fechaActual()->month)
            ->whereNull('servicios.deleted_at')
            ->whereNull('he_mensual_servicios.deleted_at')
            ->whereNull('he_adicionales.deleted_at')
            ->select('servicios.nombre','servicios.id as id_servicio',
                'he_mensual_servicios.id as id_he_mensual_servicio',
                'he_mensual_servicios.jefe_servicio',
                'he_mensual_servicios.fecha')->get();
        foreach ($nombreServicio as $registro) {
            $horas = HE_Adicional::where('id_he_mensual_servicio','=',$registro->id_he_mensual_servicio)
                ->sum('horas');
            $registro->horas = $horas;
        }
        return response()->json(array('listaServicios'=>$nombreServicio));
    }
    //fin vista listar administración
    //******************************************************************************************************************

    /*
     *                  Vista administración
     **/
    //******************************************************************************************************************
    //Vista administración
    public function mostrarListaServiciosSolicitanHorasAdicionales(){
        $listaServicios = DB::table('servicios')
            ->join('he_mensual_servicios','servicios.id','=','he_mensual_servicios.id_servicio')
            ->join('he_adicionales','he_adicionales.id_he_mensual_servicio','=','he_mensual_servicios.id')
            ->whereMonth('he_mensual_servicios.fecha','=',self::fechaActual()->month-1)
            ->whereYear('he_mensual_servicios.fecha','=',self::fechaActual()->subMonthNoOverflow()->year)
            ->where('he_mensual_servicios.estado','=','PA')
            ->whereNull('servicios.deleted_at')
            ->whereNull('he_mensual_servicios.deleted_at')
            ->whereNull('he_adicionales.deleted_at')
            ->select('servicios.nombre','he_mensual_servicios.jefe_servicio','he_adicionales.horas','he_mensual_servicios.id as id_he_mensual_servicio')
            ->get();
        return $listaServicios;
    }

    //muestra la lista de puestos a partir de un idHeMensualServicio
    public static function muestraListaDePuestos($idHEMensualServicio){
        $fechaSolicitud = HE_Mensual_Servicio::where('he_mensual_servicios.id','=',$idHEMensualServicio)
                          ->select('he_mensual_servicios.fecha')
                          ->first();
        $anyo = Carbon::parse($fechaSolicitud->fecha)->year;
        $puestos = DB::table('he_anuales')
            ->join('puestos','puestos.id','=','he_anuales.id_puesto')
            ->join('he_adicionales','he_adicionales.id_puesto','=','puestos.id')
            ->where('he_adicionales.id_he_mensual_servicio','=',$idHEMensualServicio)
            ->where('he_anuales.anyo', '=',$anyo)
            ->whereNull('puestos.deleted_at')
            ->whereNull('he_anuales.deleted_at')
            ->whereNull('he_adicionales.deleted_at')
            ->select('puestos.id','puestos.nombre as nombre_puesto','puestos.promedio_salarial',
                'he_anuales.horas as horas_anuales')->get();
        return $puestos;
    }

    //obtiene algunos datos necesarios para la vista de administración.
    public function obtenerInformacionAdicionalAdministracion($idPuesto, $idHEMensualServicio)
    {
        $datos = HE_Mensual_Servicio::where('he_mensual_servicios.id','=',$idHEMensualServicio)
            ->join('he_adicionales','he_adicionales.id_he_mensual_servicio','=','he_mensual_servicios.id')
            ->where('he_adicionales.id_puesto', '=', $idPuesto)
            ->where('he_mensual_servicios.estado','=','PA')
            ->whereNull('he_mensual_servicios.deleted_at')
            ->whereNull('he_adicionales.deleted_at')
            ->select('he_adicionales.cantidad_empleados',
                'he_adicionales.horas',
                'he_adicionales.justificacion',
                'he_adicionales.retroalimentacion',
                'he_adicionales.id_puesto',
                'he_adicionales.id as id_he_adicional')
            ->first();
        return $datos;
    }

    //Función general que retorna toda la información necesaria para la vista de administración.
    public function mostrarInformacionAdministracionAdicional($idHEMensualServicio){
        $servicio =  $this->obtenerNombreIDServicio($idHEMensualServicio);
        $listaPuestos = $this->muestraListaDePuestos($idHEMensualServicio);
        for ($i=0; $i < sizeof($listaPuestos); $i++) {
            $idPuesto = $listaPuestos[$i]->id;
            $listaPuestos[$i]->horas_autorizadas_mes_anterior = $this->mostrarHorasAutorizadasMesAnterior($idPuesto);
            $listaPuestos[$i]->promedio_horas = $this->mostrarPromedioHorasSolicitadasMes($idPuesto)->original;
            $obtenerInformacion = $this->obtenerInformacionAdicionalAdministracion($idPuesto,$idHEMensualServicio);
            $listaPuestos[$i]->cantidad_empleados = $obtenerInformacion->cantidad_empleados;
            $listaPuestos[$i]->horas= $obtenerInformacion->horas;
            $listaPuestos[$i]->justificacion = $obtenerInformacion->justificacion;
            $listaPuestos[$i]->id_he_adicional = $obtenerInformacion->id_he_adicional;
        }
        $servicio->puestos = $listaPuestos;
        return response()->json($servicio);
    }
    //fin vista admistración
    //******************************************************************************************************************


    /*
     *                   Vista presupuesto
     */
    //******************************************************************************************************************
    //obtiene algunos datos necesarios para la vista de administración.
    public function obtenerInformacionAdicionalPresupuesto($idPuesto, $idHEMensualServicio)
    {
        $datos = HE_Mensual_Servicio::where('he_mensual_servicios.id','=',$idHEMensualServicio)
            ->join('he_adicionales','he_adicionales.id_he_mensual_servicio','=','he_mensual_servicios.id')
            ->where('he_adicionales.id_puesto', '=', $idPuesto)
            ->where('he_mensual_servicios.estado','=','PP')
            ->whereNull('he_mensual_servicios.deleted_at')
            ->whereNull('he_adicionales.deleted_at')
            ->select('he_adicionales.cantidad_empleados',
                'he_adicionales.horas',
                'he_adicionales.justificacion',
                'he_adicionales.retroalimentacion',
                'he_adicionales.id_puesto',
                'he_adicionales.id as id_he_adicional')
            ->first();
        return $datos;
    }
    //muestra la información general del presupuesto.
    public function mostrarInformacionPresupuestoAdicional($idHEMensualServicio){
        $servicio =  $this->obtenerNombreIDServicio($idHEMensualServicio);
        $listaPuestos = $this->muestraListaDePuestos($idHEMensualServicio);
        for ($i=0; $i < sizeof($listaPuestos); $i++) {
            $idPuesto = $listaPuestos[$i]->id;
            $original = $this->mostrarTotalHorasAnualesPorCadaPuesto($idPuesto)->original;
            $listaPuestos[$i]->horas_anuales = $original;
            $listaPuestos[$i]->promedio_horas = $this->mostrarPromedioHorasSolicitadasMes($idPuesto)->original;
            $listaPuestos[$i]->horas_autorizadas = $this->mostrarHorasAutorizadasMesAnterior($idPuesto);
            $obtenerInformacion = $this->obtenerInformacionAdicionalPresupuesto($idPuesto,$idHEMensualServicio);
            $listaPuestos[$i]->cantidad_empleados = $obtenerInformacion->cantidad_empleados;
            $listaPuestos[$i]->horas= $obtenerInformacion->horas;
            $listaPuestos[$i]->justificacion = $obtenerInformacion->justificacion;
            $listaPuestos[$i]->id_he_adicional = $obtenerInformacion->id_he_adicional;
        }
        $servicio->puestos = $listaPuestos;
        return response()->json(array('servicio'=>$servicio));
    }
    //fin vista presupuesto
    //******************************************************************************************************************


    /*
     *                   Vista listar presupuesto
     */
    //******************************************************************************************************************
    //Vista listar Presupuesto
    //obtiene algunos datos adiciones que son necesarios para la vista de presupuesto
    public function listarServiciosSolicitaHorasAdicionalesPresupuesto()
    {
        $nombreServicio = DB::table('servicios')
            ->join('he_mensual_servicios','he_mensual_servicios.id_servicio', '=', 'servicios.id')
            ->join('he_adicionales','he.adicionales.id_he_mensual_servicio','=','he_mensual_servicios.id')
            ->where('he_mensual_servicios.estado','=','PP')
            ->whereYear('he_mensual_servicios.fecha', '=', self::fechaActual()->year)
            ->whereMonth('he_mensual_servicios.fecha', '=', self::fechaActual()->month)
            ->whereNull('servicios.deleted_at')
            ->whereNull('he_mensual_servicios.deleted_at')
            ->whereNull('he_adicionales.deleted_at')
            ->select('servicios.nombre','servicios.id as id_servicio',
                'he_mensual_servicios.id as id_he_mensual_servicio',
                'he_mensual_servicios.jefe_servicio',
                'he_mensual_servicios.fecha')->get();
        foreach ($nombreServicio as $registro) {
            $horas= HE_Adicional::where('id_he_mensual_servicio','=',$registro->id_he_mensual_servicio)
                ->sum('horas');
            $registro->horas = $horas;
        }
        return response()->json(array('listaServicios'=>$nombreServicio));
    }

    public function mostrarListaServiciosSolicitanHorasAdicionalesPresupuesto(){
        $listaServicios = DB::table('servicios')
            ->join('he_mensual_servicios','servicios.id','=','he_mensual_servicios.id_servicio')
            ->join('he_adicionales','he_adicionales.id_he_mensual_servicio','=','he_mensual_servicios.id')
            ->whereMonth('he_mensual_servicios.fecha','=',self::fechaActual()->month-1)
            ->whereYear('he_mensual_servicios.fecha','=',self::fechaActual()->subMonthNoOverflow()->year)
            ->where('he_mensual_servicios.estado','=','PP')
            ->whereNull('servicios.deleted_at')
            ->whereNull('he_mensual_servicios.deleted_at')
            ->whereNull('he_adicionales.deleted_at')
            ->select('servicios.nombre','he_mensual_servicios.jefe_servicio','he_adicionales.horas','he_mensual_servicios.id as id_he_mensual_servicio')
            ->get();
        return $listaServicios;
    }

    //fin vista listar presupuesto
    //******************************************************************************************************************

    /*
     *                   Vista aprobación
     */
    //******************************************************************************************************************
    //Vista Aprobación
    //listar los servicios que han solicitado aprobaciones
    public function listarServiciosSolicitaHorasAdicionalesAprobacion(){
        $nombreServicio = DB::table('servicios')
            ->join('he_mensual_servicios','he_mensual_servicios.id_servicio', '=', 'servicios.id')
            ->join('he_adicionales','he_adicionales.id_he_mensual_servicio','=','he_mensual_servicios.id')
            ->where('he_mensual_servicios.estado','=','AP')
            ->whereYear('he_mensual_servicios.fecha','=',self::fechaActual()->subMonthNoOverflow()->year)
            ->whereMonth('he_mensual_servicios.fecha', '=', self::fechaActual()->month-1)
            ->whereNull('servicios.deleted_at')
            ->whereNull('he_mensual_servicios.deleted_at')
            ->whereNull('he_adicionales.deleted_at')
            ->select('servicios.nombre','servicios.id as id_servicio',
                'he_mensual_servicios.id as id_he_mensual_servicio',
                'he_mensual_servicios.jefe_servicio',
                'he_mensual_servicios.fecha')->get();
        foreach ($nombreServicio as $registro) {
            $horas = HE_Adicional::where('id_he_mensual_servicio','=',$registro->id_he_mensual_servicio)
                ->sum('horas');
            $montoReservado = HE_Adicional::where('id_he_mensual_servicio','=',$registro->id_he_mensual_servicio)
                ->sum('monto_reservado');
            $registro->horas = $horas;
            $registro->monto_reservado = $montoReservado;
        }
        return response()->json(array('listaServicios'=>$nombreServicio));
    }
    // fin vista aprobación
    //******************************************************************************************************************


    /*
     *                   Vista correciones
     */
    //******************************************************************************************************************
    //Vista correciones
    //Función se necesita para realizar correcciones a la solicitud, pasando un id de usuario como parámetro.
    public function mostrarNombreServicioCorreccionesAdicionales($idPuesto){
        $nombreServicio = DB::table('puestos')
            ->join('servicios','servicios.id','=','puestos.id_servicio')
            ->join('he_mensual_servicios','he_mensual_servicios.id_servicio','=','servicios.id')
            ->join('he_adicionales','he_adicionales.id_he_mensual_servicio','=','he_mensual_servicios.id')
            ->where('puestos.id','=',$idPuesto)
            ->whereMonth('he_mensual_servicios.fecha','=',self::fechaActual()->month-1)
            ->whereYear('he_mensual_servicios.fecha','=',self::fechaActual()->subMonthNoOverflow()->year)
            ->whereNull('puestos.deleted_at')
            ->whereNull('servicios.deleted_at')
            ->whereNull('he_mensual_servicios.deleted_at')
            ->whereNull('he_adicionales.deleted_at')
            ->select('servicios.nombre','servicios.id','he_mensual_servicios.observaciones',
                'he_mensual_servicios.retroalimentacion', 'he_mensual_servicios.fecha','he_mensual_servicios.id as id_he_mensual_servicio')
           ->first();
        return response()->json($nombreServicio);
    }
    //verifica si hay nombramientos para la fecha actual.
    public function verificaNombramientoHorasAdicionales($idUsuario){
        $nombramiento = DB::table('users')
            ->join('funcionarios', 'funcionarios.cedula', '=', 'users.cedula')
            ->join('funcionarios_nombramientos', 'funcionarios_nombramientos.cedula', '=', 'funcionarios.cedula')
            ->join('nombramientos', 'nombramientos.id', '=', 'funcionarios_nombramientos.id_nombramiento')
            ->where('users.id','=',$idUsuario)
            ->whereDate('nombramientos.fecha_inicio','<',$this->fechaActual())
            ->whereDate('nombramientos.fecha_fin','>',$this->fechaActual())
            ->whereNull('users.deleted_at')
            ->whereNull('funcionarios.deleted_at')
            ->whereNull('funcionarios_nombramientos.deleted_at')
            ->whereNull('nombramientos.deleted_at')
            ->select('nombramientos.id_puesto')->first();
        return $nombramiento;
    }
    //obtiene algunos datos necesarios para la vista de administración.
    public function obtenerInformacionAdicionalCorrecciones($idPuesto, $idHEMensualServicio)
    {
        $datos = HE_Mensual_Servicio::where('he_mensual_servicios.id','=',$idHEMensualServicio)
            ->join('he_adicionales','he_adicionales.id_he_mensual_servicio','=','he_mensual_servicios.id')
            ->where('he_adicionales.id_puesto', '=', $idPuesto)
            ->where('he_mensual_servicios.estado','=','D')
            ->whereNull('he_mensual_servicios.deleted_at')
            ->whereNull('he_adicionales.deleted_at')
            ->select('he_adicionales.cantidad_empleados',
                'he_adicionales.horas',
                'he_adicionales.justificacion',
                'he_adicionales.retroalimentacion',
                'he_adicionales.id_puesto',
                'he_adicionales.id as id_he_adicional')
            ->first();
        return $datos;
    }
    //Función general. ESta es la que muestra toda la información de la vista correciones.
    public function muestraCorreccionesAdicionales($idUsuario){
        $idPuesto = $this->verificaNombramientoHorasAdicionales($idUsuario);
        if(!$idPuesto){
            return response()->json(['message' => 'El usuario no cuenta con un nombramiento en la fecha actual.'],404);
        }
        try{
            $servicio = $this->mostrarNombreServicioCorreccionesAdicionales($idPuesto->id_puesto);
            $idServicio = $servicio->original->id;
            $listaPuestos = $this->mostrarListaPuestosDeServiciosDeUsuarios($idServicio);
            if(sizeof($listaPuestos)==0){
                return response()->json(['message' => 'No hay puestos asociados a este servicio',], 404);
            }
            for ($i=0; $i < sizeof($listaPuestos); $i++) {
                $idPuesto = $listaPuestos[$i]->id;
                $original = $this->mostrarTotalHorasAnualesPorCadaPuesto($idPuesto)->original;
                $listaPuestos[$i]->horas_anuales = $original;
                $listaPuestos[$i]->promedio_horas = $this->mostrarPromedioHorasSolicitadasMes($idPuesto)->original;
                $listaPuestos[$i]->horas_autorizadas_mes_anterior = $this->mostrarHorasAutorizadasMesAnterior($idPuesto);
                $obtenerInformacion = $this->obtenerInformacionAdicionalCorrecciones($idPuesto,$servicio->original->id_he_mensual_servicio);
                if(!$obtenerInformacion){
                    return response()->json(['message' => 'La solicitud actual no cuenta con correcciones hasta el momento.'],404);
                }
                $listaPuestos[$i]->cantidad_empleados = $obtenerInformacion->cantidad_empleados;
                $listaPuestos[$i]->horas = $obtenerInformacion->horas;
                $listaPuestos[$i]->justificacion = $obtenerInformacion->justificacion;
                $listaPuestos[$i]->retroalimentacion =$obtenerInformacion->retroalimentacion;
                $listaPuestos[$i]->id_he_adicional = $obtenerInformacion->id_he_adicional;
            }
            $servicio->original->puestos = $listaPuestos;
            return response()->json(array('servicio'=>$servicio->original));
        }
        catch (\Exception $e) {
            return response()->json(['message' => 'Aún no se han realizado solicitudes de horas adicionales para el mes anterior.'],404);
        }
    }
    //fin vista correciones
    //******************************************************************************************************************


    /*
    *                   Funciones globales.
     */
    //******************************************************************************************************************
    //esta función obtiene el nombre y el id de servicio cuando se le envía un idHEMensualServicio
    public static function obtenerNombreIDServicio($idHEMensualServicio)
    {
        $nombreServicio = DB::table('servicios')
            ->join('he_mensual_servicios', 'he_mensual_servicios.id_servicio', '=', 'servicios.id')
            ->where('he_mensual_servicios.id', '=', $idHEMensualServicio)
            ->whereNull('servicios.deleted_at')
            ->whereNull('he_mensual_servicios.deleted_at')
            ->select('servicios.nombre','servicios.id as id_servicio',
                'he_mensual_servicios.observaciones','he_mensual_servicios.jefe_servicio','he_mensual_servicios.fecha')->first();
        return $nombreServicio;
    }



    /*
    *                   Funciones de insertar y actualizar.
     */
    //******************************************************************************************************************
    //a partir de un request enviado desde el front end, actualiza las correciones de horas adicionales.
    public function actualizarCorreccionesAdicionales(Request $request){
        $he_mensual_servicio = HE_Mensual_Servicio::findOrFail($request->id_he_mensual_servicio);
        $he_mensual_servicio->update(array('observaciones'=>$request->observaciones, 'estado'=>$request->estado));
        $he_mensual_servicio->save();
        $listaHEAdicionales = json_decode(json_encode($request->solicitudes),true);
        for($i = 0; $i<sizeof($listaHEAdicionales); $i++){
            $idHEAdicional = $listaHEAdicionales[$i]['id_he_adicional'];
            $justificacion = $listaHEAdicionales[$i]['justificacion'];
            $cantidad_empleados = $listaHEAdicionales[$i]['cantidad_empleados'];
            $horas =$listaHEAdicionales[$i]['horas'];
            $HE_Adicional = HE_Adicional::findOrFail($idHEAdicional);
            $HE_Adicional->update(array('justificacion'=>$justificacion,'horas'=>$horas,'cantidad_empleados'=>$cantidad_empleados));
            $HE_Adicional->save();
        }
    }
    //a partir de un request enviado desde el front end, actualizael presupuesto de horas adicionales.
    public function actualizarPresupuestoAdicionales(Request $request){
        $he_mensual_servicio = HE_Mensual_Servicio::findOrFail($request->id_he_mensual_servicio);
        $he_mensual_servicio->update(array('estado'=>$request->estado));
        $listaHEAdicionales = json_decode(json_encode($request->solicitudes),true);
        for($i=0;$i<sizeof($listaHEAdicionales);$i++){
            $idHEAdicionales = $listaHEAdicionales[$i]['id_he_adicional'];
            $montoReservado = $listaHEAdicionales[$i]['monto_reservado'];
            $he_adicional = HE_Adicional::findOrFail($idHEAdicionales);
            $he_adicional->update(array('monto_reservado'=>$montoReservado));
        }
        return response()->json(['success' => 'success'], 200);
    }
    //Vista administración, rechazar solicitud. Envia una retroalimentación para cada puesto que se rechaza.
    public function actualizarRechazarOAprobarSolicitudAdicionales(Request $request){
        $he_mensual_servicio = HE_Mensual_Servicio::findOrFail($request->id_he_mensual_servicio);
        $he_mensual_servicio->update(array('estado'=>$request->estado, 'retroalimentacion'=>$request->retroalimentacion));
        $listaHEAdicional = json_decode(json_encode($request->solicitudes),true);
        for($i=0;$i<sizeof($listaHEAdicional);$i++){
            $idHEAdicional = $listaHEAdicional[$i]['id_he_adicional'];
            if (isset($listaHEAdicional[$i]['retroalimentacion'])) {
                $retroalimentacion = $listaHEAdicional[$i]['retroalimentacion'];
                $he_adicional = HE_Adicional::findOrFail($idHEAdicional);
                $he_adicional->update(array('retroalimentacion'=>$retroalimentacion));
                continue;
            }
        }
        return response()->json(['success' => 'success'], 200);
    }
    //función que actualiza la aprobación final de horas adicionales..
    public function actualizarEstadoAprobacionAdicionales(Request $request){
        $he_mensual_servicio = HE_Mensual_Servicio::findOrFail($request->id_he_mensual_servicio);
        $he_mensual_servicio->update(array('estado'=>$request->estado));
        return response()->json(['success' => 'success'], 200);
    }

}
