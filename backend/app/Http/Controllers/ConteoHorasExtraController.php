<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Funcionario;
use App\Funcionario_Nombramiento;
use App\HE_Diaria;
use App\HE_Diaria_Funcionario_Nombramiento;
use App\Nombramiento;
use Illuminate\Support\Carbon;

class ConteoHorasExtraController extends Controller
{
    /**
     *                  funciones globales obtener fechas.
     **/
    public static function diaActual()
    {
        return Carbon::now('America/Costa_Rica')->day;
    } //retorna el dia actual. Ej: 5

    public static function diaAnterior()
    {
        return Carbon::now('America/Costa_Rica')->day - 1;
    } //retorna el dia anterior.

    public static function mesAnterior()
    {
        return Carbon::now('America/Costa_Rica')->month - 1;
    } //retorna el numero de mes anterior Ej: 4

    public static function mesActual()
    {
        return Carbon::now('America/Costa_Rica')->month;
    } //retorna el numero del mes actual

    public static function annoActual()
    {
        return Carbon::now('America/Costa_Rica')->year;
    }  //retorna el numero del año actual. Ej: 2018

    public static function annoAnterior()
    {
        return Carbon::now('America/Costa_Rica')->year - 1;
    } //retorna el numero del año anterior. Ej: 2017

    //******************************************************************************************************************

    /**
     *                   Funciones globales.
     */
    //******************************************************************************************************************
    //******************************************************************************************************************
    //******************************************************************************************************************
    public static function obtenerIDServicioPorCedula($cedula)
    {
        //Falta validación de nombramiento activo
        $nombreServicio = DB::table('funcionarios_nombramientos')
            ->join('nombramientos', 'nombramientos.id', '=', 'funcionarios_nombramientos.id_nombramiento')
            ->join('puestos', 'puestos.id', '=', 'nombramientos.id_puesto')
            ->join('servicios', 'servicios.id', '=', 'puestos.id_servicio')
            ->where('funcionarios_nombramientos.cedula', '=', $cedula)
            ->where('nombramientos.estado', '=', true)
            ->select('servicios.nombre as nombre_servicio', 'servicios.id as id_servicio')->first();
        return response()->json($nombreServicio);
    }

    public static function obtenerIDServicioPorCedula_2($cedula)
    {
        $servicio = Funcionario::find($cedula)->nombramiento()->activo()->firstOrFail()->puesto->servicio;
        return response()->json(array('nombre_servicio' => $servicio->nombre, 'id_servicio' => $servicio->id));

    }

    public static function obtenerListaDeFuncionariosPorServicioPorCedula($cedula)
    {
        //Validar que sean nombramientos activos
        $idServicio = self::obtenerIDServicioPorCedula($cedula)->original->id_servicio;
        $funcionarios = DB::table('puestos')
            ->join('nombramientos', 'puestos.id', '=', 'nombramientos.id_puesto')
            ->join('funcionarios_nombramientos', 'nombramientos.id', '=', 'funcionarios_nombramientos.id_nombramiento')
            ->join('funcionarios', 'funcionarios_nombramientos.cedula', '=', 'funcionarios.cedula')
            ->where('puestos.id_servicio', '=', $idServicio)
            ->whereNull('funcionarios_nombramientos.deleted_at')
            ->whereDate('nombramientos.fecha_fin', '>', Carbon::now())
            ->select(
                'funcionarios.cedula as cedula',
                'funcionarios.nombre as nombre',
                'funcionarios.numero_tarjeta as numero_tarjeta',
                'funcionarios_nombramientos.id as id_funcionario_nombramiento',
                'puestos.id as id_puesto'
            )->get();
        return $funcionarios;
    }

    /*public static function obtenerListaDeFuncionariosPorServicioPorCedula_2($cedula)
    {
        $nombramiento = Funcionario::find($cedula)->nombramiento()->activo()->firstOrFail();
        $puesto_id = $nombramiento->puesto->id;
        $funcionario_nombramiento_id = $nombramiento->funcionario_nombramiento->where()

        /*$puesto_id = Funcionario::find($cedula)->nombramiento()->activo()
            ->firstOrFail()->id_puesto;*
        $funcionario = Funcionario::find($cedula);

        return response()->json(array(
            'cedula' => $cedula,
            'nombre' => $funcionario->nombre . ' ' . $funcionario->apellido1 . ' ' . $funcionario->apellido2,
            'numero_tarjeta' => $funcionario->numero_tarjeta,
            'id_funcionario_nombramiento' => $funcionario_nombramiento_id,
            'id_puesto' => $puesto->id));
    }*/

    public static function obtenerListaRegistroConteoHorasExtraPorCedula($cedula)
    {
        //Faltan muchas validaciones de softDelete (Solución: usar Eloquent)
        $idServicio = self::obtenerIDServicioPorCedula($cedula)->original->id_servicio;
        $registros = DB::table('he_diarias_funcionarios_nombramientos')
            ->join('funcionarios_nombramientos', 'funcionarios_nombramientos.id', '=', 'he_diarias_funcionarios_nombramientos.id_funcionario_nombramiento')
            ->join('nombramientos', 'nombramientos.id', '=', 'funcionarios_nombramientos.id_nombramiento')
            ->join('funcionarios', 'funcionarios.cedula', '=', 'funcionarios_nombramientos.cedula')
            ->join('he_diarias', 'he_diarias.id', '=', 'he_diarias_funcionarios_nombramientos.id_he_diaria')
            ->join('puestos', 'puestos.id', '=', 'he_diarias.id_puesto')
            ->where('puestos.id_servicio', '=', $idServicio)
            ->where('nombramientos.fecha_fin', '>', Carbon::now())
            ->whereNull('nombramientos.deleted_at')
            ->whereNull('funcionarios_nombramientos.deleted_at')
            ->whereNull('he_diarias_funcionarios_nombramientos.deleted_at')
            ->whereNull('funcionarios.deleted_at')
            ->whereNull('he_diarias.deleted_at')
            ->where('he_diarias.fecha', '=', Carbon::now()->toDateString())
            ->select(
                'funcionarios.cedula as cedula',
                'funcionarios.nombre as nombre',
                'funcionarios.numero_tarjeta as numero_tarjeta',
                'funcionarios_nombramientos.id as id_funcionario_nombramiento',
                'puestos.id as id_puesto',
                'he_diarias_funcionarios_nombramientos.horas as horas',
                'he_diarias_funcionarios_nombramientos.id as id_diarias_funcionarios_nombramientos'
            )->get();
        return $registros;

    }

    public static function obtenerIdFuncionarios_Nombramientos($cedula)
    {
        $idFuncionarios_Nombramientos = DB::table('funcionarios_nombramientos')
            ->where('cedula', '=', $cedula)
            ->whereNull('deleted_at')
            ->select('id')
            ->get();
        return $idFuncionarios_Nombramientos;
    }
    /*public static function obtenerIdFuncionarios_Nombramientos_2($cedula)
    {
        $idFuncionarios_Nombramientos = Funcionario::get($cedula)->nombramiento_activo->funcionario_nombramiento->where('cedula','=',$cedula)->first()->id;
    }*/
    /*public static function obtenerIdHE_Mensual_Puesto($cedula)
    {
        $idHE_Mensual_Puesto = DB::table('he_mensual_puestos')
            ->join('he_mensual_servicios','he_mensual_servicios.id','=','he_mensual_puestos.id_he_mensual_servicio')
            ->join('servicios','servicios.id')
    }*/
    //Lista de registros de horas extras de un funcionario específico del día actual (No útil)
    public static function muestraListaFuncionarioRegistrosPorDia($id)
    {
        $registros = DB::table('he_diarias_funcionarios_nombramientos')
            ->join('he_diarias', 'he_diarias.id', '=', 'he_diarias_funcionarios_nombramientos.id_he_diaria')
            ->where('he_diarias_funcionarios_nombramientos.id_funcionario_nombramiento', '=', $id)
            ->where('he_diarias.fecha', '=', Carbon::now())
            ->select('he_diarias_funcionarios_nombramientos.id as id', 'he_diarias_funcionarios_nombramientos.id_funcionario_nombramiento as id_funcionario_nombramiento', 'he_diarias_funcionarios_nombramientos.id_he_diaria as id_he_diaria', 'he_diarias_funcionarios_nombramientos.horas as horas')
            ->get();
        return $registros;

    }

    /*public function crearHorasExtraDiarias($id_he_mensual_puesto) //Revisar todos los puestos a los que tiene acceso el funcionario Jefe de Servicio
    {
        $hed = HE_Diarias::firstOrCreate(array('id_he_mensual_puesto' => $id_he_mensual_puesto, 'fecha' => Carbon::now()->toDateString()));
        return $hed->id;
        //Crear un registro por cada HE_Mensual_Puesto de este mes
    }*/

    public function insertarHorasExtraFuncionario(Request $request)
        //Request debe tener los datos pertinentes para todas las conexiones de llaves foráneas
    {
        /*if ($request->id_he_mensual_puesto == null) {

        }*/
        //request: horas, id_funcionario_nombramiento, id_puesto
        $hed = HE_Diaria::firstOrCreate(array('id_puesto' => $request->id_puesto, 'fecha' => Carbon::now()->toDateString()));

        $he_diaria_funcionario_nombramiento = new HE_Diaria_Funcionario_Nombramiento();
        $he_diaria_funcionario_nombramiento->id_funcionario_nombramiento = $request->id_funcionario_nombramiento;
        $he_diaria_funcionario_nombramiento->id_he_diaria = $hed->id;
        $he_diaria_funcionario_nombramiento->horas = $request->horas;
        $he_diaria_funcionario_nombramiento->save();

        //echo('[');
        $funcionario = Funcionario_Nombramiento::find($request->id_funcionario_nombramiento)->funcionario;
        //echo ($funcionario);

        //echo($funcionario->cedula);
        //echo($funcionario->nombre);
        //echo($funcionario->numero_tarjeta);
        return response()->json(
            array(
                'cedula' => $funcionario->cedula,
                'nombre' => $funcionario->nombre,
                'numero_tarjeta' => $funcionario->numero_tarjeta,
                'id_funcionario_nombramiento' => $he_diaria_funcionario_nombramiento->id_funcionario_nombramiento,
                'id_puesto' => $request->id_puesto,
                'horas' => $request->horas
            ),
            200
        );
        //echo (']');
    }

    public function actualizarHorasExtraFuncionario(Request $request)
    {
        //request: horas, id_funcionario_nombramiento, id_puesto
        $hed = HE_Diaria::where('id_puesto', $request->id_puesto)->where('fecha', Carbon::now()->toDateString())->firstOrFail();

        $he_diaria_funcionario_nombramiento = HE_Diaria_Funcionario_Nombramiento::where(
            'id_funcionario_nombramiento',
            $request->id_funcionario_nombramiento
        )->where(
            'id_he_diaria',
            $hed->id
        )->firstOrFail();
        $he_diaria_funcionario_nombramiento->horas = $request->horas;
        $he_diaria_funcionario_nombramiento->save();

        //echo('[');
        $funcionario = Funcionario_Nombramiento::find($request->id_funcionario_nombramiento)->funcionario;
        //echo ($funcionario);

        //echo($funcionario->cedula);
        //echo($funcionario->nombre);
        //echo($funcionario->numero_tarjeta);
        return response()->json(
            array(
                'cedula' => $funcionario->cedula,
                'nombre' => $funcionario->nombre,
                'numero_tarjeta' => $funcionario->numero_tarjeta,
                'id_funcionario_nombramiento' => $he_diaria_funcionario_nombramiento->id_funcionario_nombramiento,
                'id_puesto' => $request->id_puesto,
                'horas' => $request->horas
            ),
            200
        );
        //echo (']');
    }

    public function eliminarHorasExtraFuncionario(Request $request)
    {
        //request: horas, id_funcionario_nombramiento, id_puesto
        $hed = HE_Diaria::where('id_puesto', $request->id_puesto)->where('fecha', Carbon::now()->toDateString())->firstOrFail();

        $he_diaria_funcionario_nombramiento = HE_Diaria_Funcionario_Nombramiento::where(
            'id_funcionario_nombramiento',
            $request->id_funcionario_nombramiento
        )->where(
            'id_he_diaria',
            $hed->id
        )->firstOrFail()->delete();

        return response()->json(['success' => 'success'], 200);
    }
}
