<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Funcionario_Nombramiento extends Model
{
    use SoftDeletes;

    protected $table = 'funcionarios_nombramientos';
    protected $guarded = [];
    public $primaryKey ='id';
    protected $events = [
        'deleted' => 'deletedFunc',
        'restored' => 'restoredFunc'
    ];
    
    //public function getDateFormat()
    //{
    //    return 'd/m/Y H:i:s';
    //}

    public function funcionario()
    {
        return $this->belongsTo('App\Funcionario','cedula','cedula');
    }

    public function nombramiento()
    {
        return $this->belongsTo('App\Nombramiento','id_nombramiento','id');
    }

    public function he_diaria_funcionario_nombramiento()
    {
        return $this->hasMany('App\HE_Diaria_Funcionario_Nombramiento','id_funcionario_nombramiento','id');
    }

    protected static function boot()
    {
        parent::boot();

        static::deleted(function($item)
        {
//            $item->nombramiento->delete();
//            $item->funcionario->delete();
        });

        static ::restored(function ($item)
        {
//            $item->nombramiento->withTrashed()->restore();
//            $item->funcionario->withTrashed()->restore();
        });
    }
}
