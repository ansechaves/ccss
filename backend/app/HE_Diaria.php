<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HE_Diaria extends Model
{
    use SoftDeletes;

    protected $table = 'he_diarias';
    protected $guarded = [];
    
    public function he_mensual_puesto()
    {
        return $this->belongsTo('App\Puesto','id_puesto','id');
    }

    public function he_diaria_funcionario_nombramiento()
    {
        return $this->hasMany('App\HE_Diaria_Funcionario_Nombramiento','id_he_diaria','id');
    }
}
