<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Servicio extends Model
{
    use SoftDeletes;

    protected $table = 'servicios';
    protected $guarded = [];

    public function puesto()
    {
        return $this->hasMany('App\Puesto','id_servicio','id');
    }

    public function he_mensual_servicio()
    {
        return $this->hasMany('App\HE_Mensual_Servicio','id_servicio','id');
    }

    protected static function boot()
    {
        parent::boot();

        static::deleted(function($item)
        {
            $item->puesto->each->delete();
            $item->he_mensual_servicio->each->delete();
        });

        static ::restored(function ($item)
        {
            $item->puesto->withTrashed()->each->restore();
            $item->he_mensual_servicio->withTrashed()->each->restore();
        });
    }
}
