<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Database\Eloquent\Model;

class User extends Authenticatable  implements JWTSubject
{
    use Notifiable, SoftDeletes;
    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    /*
    protected $fillable = ['contrasenya', 'nombre_usuario',];
    */

//    protected $events = [
//        'deleted' => 'deletedFunc',
//        'restored' => 'restoredFunc'
//    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    //Rest omitted for brevity
    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function funcionario()
    {
        return $this->belongsTo('App\Funcionario','cedula','cedula');
    }

    public function acceso_usuario()
    {
        return $this->hasOne('App\Acceso_Usuarios','id_usuario','id');
    }

//    private function deletedFunc($item)
//    {
//        $item->acceso_usuario->delete();
//    }
//
//    private function restoredFunc($item)
//    {
//        $item->acceso_usuario->withTrashed()->restore();
//    }
    protected static function boot() {
        parent::boot();

        static::deleted(function($item) {
            $item->acceso_usuario->delete();
        });
        static ::restored(function ($item){
            $item->acceso_usuario->withTrashed()->restore();
        });
    }
}
