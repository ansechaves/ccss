<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HE_Mensual_Puesto extends Model
{
    use SoftDeletes;

    protected $table = 'he_mensual_puestos';

    protected $guarded = [];
    
    public function he_mensual_servicio()
    {
        return $this->belongsTo('App\HE_Mensual_Servicio','id_he_mensual_servicio','id');
    }

    public function puesto()
    {
        return $this->belongsTo('App\Puesto','id_puesto','id');
    }

    public function he_diaria()
    {
        return $this->hasMany('App\HE_Diaria','id_he_mensual_puesto','id');
    }

    public function ausencia()
    {
        return $this->hasMany('App\Ausencia','id_he_mensual_puesto','id');
    }

    protected static function boot()
    {
        parent::boot();

        static::deleted(function($item)
        {
            $item->ausencia->each->delete();
            //$item->he_diaria->delete();
            //$item->he_adicional->delete();
        });

        static ::restored(function ($item)
        {
            $item->ausencia->withTrashed()->each->restore();
            //$item->he_diaria->withTrashed()->restore();
            //$item->he_adicional->withTrashed()->restore();
        });
    }
}
