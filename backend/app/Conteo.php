<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Conteo extends Model
{
    use SoftDeletes;

    protected $table = 'conteos';
    protected $guarded = [];

    public function puesto()
    {
        return $this->belongsTo('App\Puesto', 'id_Puesto', 'id');
    }

    public function funcionarioSustituto()
    {
        return $this->hasOne('App\Funcionario', 'Id_sustituto', 'cedula');
    }
    public function funcionarioAusente()
    {
        return $this->hasOne('App\Funcionario', 'id_PersonaAusente', 'cedula');
    }
}
