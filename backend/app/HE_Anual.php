<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HE_Anual extends Model
{
    use SoftDeletes;

    protected $table = 'he_anuales';
    protected $guarded = [];
    
    public function puesto()
    {
        return $this->belongsTo('App\Puesto','id_puesto','id');
    }
}
