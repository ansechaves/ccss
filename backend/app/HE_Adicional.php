<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HE_Adicional extends Model
{
    use SoftDeletes;

    protected $table = 'he_adicionales';
    protected $guarded = [];
    
    public function he_mensual_servicio()
    {
        return $this->belongsTo('App\HE_Mensual_Servicio','id_he_mensual_servicio','id');
    }

    public function puesto()
    {
        return $this->belongsTo('App\Puesto','id_puesto','id');
    }
}
