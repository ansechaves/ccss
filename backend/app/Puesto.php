<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Puesto extends Model
{
    use SoftDeletes;

    protected $table = 'puestos';
    protected $guarded = [];
    
    public function servicio()
    {
        return $this->belongsTo('App\Servicio','id_servicio','id');
    }

    public function he_anual()
    {
        return $this->hasMany('App\HE_Anual','id_puesto','id');
    }

    public function he_mensual_puesto()
    {
        return $this->hasMany('App\HE_Mensual_Puesto','id_puesto','id');
    }

    public function nombramiento()
    {
        return $this->hasMany('App\Nombramiento','id_puesto','id');
    }

    public function he_adicional()
    {
        return $this->hasMany('App\HE_Adicional','id_puesto','id');
    }
    
    public function conteo(){
        return $this->hasMany('App\Conteo','id_Puesto','id');
    }

    protected static function boot()
    {
        parent::boot();

        static::deleted(function($item)
        {
            $item->he_anual->each->delete();
            $item->he_mensual_puesto->each->delete();
            $item->nombramiento->each->delete();
            $item->conteo->each->delete();
            try
            {
                $item->he_adicional->each->delete();
            }
            catch(Exception $e)
            {
            }
            
        });

        static ::restored(function ($item)
        {
            $item->he_anual->withTrashed()->each->restore();
            $item->he_mensual_puesto->withTrashed()->each->restore();
            $item->nombramiento->withTrashed()->restore();
            try
            {
                $item->he_adicional->each->withTrashed()->restore();
            }
            catch(Exception $e)
            {
            }

        });
    }
}
