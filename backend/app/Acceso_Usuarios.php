<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Acceso_Usuarios extends Model
{
    use SoftDeletes;
    protected $table = 'acceso_usuarios';
    protected $guarded = [];
    protected $primaryKey='id_usuario';

    public function user()
    {
        return $this->belongsTo('App\User','id_usuario');
    }
}
