<?php

namespace App\Imports;

use App\Funcionario;
use Maatwebsite\Excel\Concerns\ToModel;

class UsersImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        
        $FuncionarioNuevo = new Funcionario([]);

        $FuncionarioNuevo->cedula = $row[0];
        $FuncionarioNuevo->nombre = $row[3].' '.$row[4].' '.$row[5].' '.$row[6].' '.$row[7];
        $FuncionarioNuevo->apellido1 = $row[1];
        $FuncionarioNuevo->apellido2 = $row[2];
       
        $FuncionarioNuevo->fecha_nacimiento = $row[8].'-'.$row[9].'-'.$row[10];
        $FuncionarioNuevo->fecha_ingreso = $row[11].'-'.$row[12].'-'.$row[13];
        $FuncionarioNuevo->numero_tarjeta = $row[14];
        $FuncionarioNuevo->correo = $row[15];

        return $FuncionarioNuevo;
    }
}
