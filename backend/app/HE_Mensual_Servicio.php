<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HE_Mensual_Servicio extends Model
{
    use SoftDeletes;

    protected $table = 'he_mensual_servicios';
    protected $guarded = [];
    
    public function servicio()
    {
        return $this->belongsTo('App\Servicio','id_servicio','id');
    }

    public function he_mensual_puesto()
    {
        return $this->hasMany('App\HE_Mensual_Puesto','id_he_mensual_servicio','id');
    }

    public function he_adicional()
    {
        return $this->hasMany('App\HE_Adicional','id_he_mensual_servicio','id');
    }

    protected static function boot()
    {
        parent::boot();

        static::deleted(function($item)
        {
            $item->he_mensual_puesto->each->delete();
            $item->he_adicional->each->delete();
        });

        static ::restored(function ($item)
        {
            $item->he_mensual_puesto->withTrashed()->each->restore();
            $item->he_adicional->withTrashed()->each->restore();
        });
    }
}
