<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

class Nombramiento extends Model
{
    use SoftDeletes;

    protected $table = 'nombramientos';
    protected $guarded = [];
    
    public function puesto()
    {
        return $this->belongsTo('App\Puesto','id_puesto','id');
    }

    public function funcionario_nombramiento()
    {
        return $this->hasMany('App\Funcionario_Nombramiento','id_nombramiento','id');
    }

    public function scopeActivo($query)
    {
        return $query->where('fecha_inicio','<',Carbon::now())->where('fecha_fin','>',Carbon::now());
    }

    protected static function boot()
    {
        parent::boot();

        static::deleted(function($item)
        {
            $item->funcionario_nombramiento->each->delete();
        });

        static ::restored(function ($item)
        {
            $item->funcionario_nombramiento->withTrashed()->each->restore();
        });
    }
}
