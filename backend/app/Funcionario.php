<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Funcionario extends Model
{
    use SoftDeletes;

    protected $table = 'funcionarios';
    protected $guarded = [];
//    protected $events = [
//    'deleted' => 'deletedFunc',
//    'restored' => 'restoredFunc'
//    ];
    protected $primaryKey='cedula';
    public $incrementing = false;

    public function funcionario_nombramiento()
    {
        return $this->hasMany('App\Funcionario_Nombramiento','cedula','cedula');
    }

    public function nombramiento()
    {
        return $this->hasManyThrough('App\Nombramiento',
            'App\Funcionario_Nombramiento', 'cedula', 'id','cedula','id_nombramiento');
    }

    public function usuario()
    {
        return $this->hasMany('App\User','cedula','cedula');
    }

    public function telefono()
    {
        return $this->hasMany('App\Telefono','cedula','cedula');
    }

//    private function deletedFunc($item)
//    {
//        $item->funcionario_nombramiento->delete();
//        $item->usuario->delete();
//        $item->telefono->delete();
//    }
//    private function restoredFunc($item)
//    {
//        $item->funcionario_nombramiento->withTrashed()->restore();
//        $item->usuario->withTrashed()->restore();
//        $item->telefono->withTrashed()->restore();
//    }
    protected static function boot()
    {
        parent::boot();

        static::deleted(function($item)
        {
            $item->funcionario_nombramiento->each->delete();
            $item->usuario->each->delete();
            $item->telefono->each->delete();
        });

        static::restored(function ($item)
        {
            $item->funcionario_nombramiento->withTrashed()->each->restore();
            $item->usuario->withTrashed()->restore();
            $item->telefono->withTrashed()->each->restore();
        });
    }
}
