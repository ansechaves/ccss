<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HE_Diaria_Funcionario_Nombramiento extends Model
{
    use SoftDeletes;
    protected $table = 'he_diarias_funcionarios_nombramientos';
    protected $guarded = [];
    protected $primaryKey='id';

    public function funcionario_nombramiento()
    {
        return $this->belongsTo('App\Funcionario_Nombramiento','id_funcionario_nombramiento');
    }

    public function he_diarias()
    {
        return $this->belongsTo('App\HE_Diarias','id_he_diaria');
    }
}
