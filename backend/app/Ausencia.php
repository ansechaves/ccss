<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ausencia extends Model
{
    use SoftDeletes;

    protected $table = 'ausencias';
    protected $guarded = [];

    public function he_mensual_puesto()
    {
        return $this->belongsTo('App\HE_Mensual_Puesto','id_he_mensual_puesto');
    }
}
