<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Telefono extends Model
{
    use SoftDeletes;

    protected $table = 'telefonos';
    protected $guarded = [];
    
    public function funcionario()
    {
        return $this->belongsTo('App\Funcionario','cedula','cedula');
    }
}
