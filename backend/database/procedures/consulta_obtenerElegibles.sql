CREATE PROCEDURE obtenerElegibles(
								@idPuesto INT,
								@fechaSolicitadaInicio DATE,
								@fechaSolicitadaFinal DATE
								)
as
DECLARE
	@idServicio INT
BEGIN
	set @idServicio = (select id_servicio from puestos as p where p.id = @idPuesto);

	SELECT 
		disponibilidadFuncionarios.disponible,
		f.numero_tarjeta,
		f.cedula,
		f.nombre,
		f.apellido1,
		f.apellido2,
		diasPuesto.cantidadDiasPuesto,
		diasServicio.cantidadDiasServicio,
		diasInstitucion.cantidadDiasInstitucion
			FROM 
				funcionarios as f
			inner join 
				(SELECT 
					f.cedula, 
					SUM(diasPuesto.cantidadDiasPuesto) as 'cantidadDiasPuesto'
						FROM
							funcionarios as f
						inner join	 
							(select 
									case
										when
											nomb.fecha_fin >= GETDATE() and nomb.fecha_inicio <= GETDATE()
												then
													SUM(DATEDIFF(DAY, nomb.fecha_inicio, GETDATE()))
										when
											nomb.fecha_fin >= GETDATE() and nomb.fecha_inicio > GETDATE()
												then
													SUM(0)
										when
											nomb.fecha_fin <= GETDATE()
												then
													SUM(DATEDIFF(DAY, nomb.fecha_inicio, nomb.fecha_fin))
									end as 'cantidadDiasPuesto',
									func.cedula
								from 
									funcionarios as func
								inner join
									funcionarios_nombramientos as func_nomb 
										on(func.cedula = func_nomb.cedula)	
								inner join 
									nombramientos as nomb
										on(func_nomb.id_nombramiento = nomb.id)
								inner join
									puestos as p
											on(p.id = nomb.id_puesto)
								where 
									p.id = @idPuesto 
									and p.deleted_at is null
									and func_nomb.deleted_at is null
									and nomb.deleted_at is null
									and func.deleted_at is null
								group by nomb.fecha_fin, func.cedula, fecha_inicio) 
							as diasPuesto
						on(f.cedula = diasPuesto.cedula)
						group by f.cedula) as diasPuesto
				on(f.cedula = diasPuesto.cedula)

			inner join

				(SELECT 
					f.cedula, 
					SUM(diasServicio.cantidadDiasServicio) as 'cantidadDiasServicio'
						FROM
							funcionarios as f
						inner join	 
							(select 
									case
										when
											nomb.fecha_fin >= GETDATE() and nomb.fecha_inicio <= GETDATE()
												then
													SUM(DATEDIFF(DAY, nomb.fecha_inicio, GETDATE()))
										when
											nomb.fecha_fin >= GETDATE() and nomb.fecha_inicio > GETDATE()
												then
													SUM(0)
										when
											nomb.fecha_fin <= GETDATE()
												then
													SUM(DATEDIFF(DAY, nomb.fecha_inicio, nomb.fecha_fin))
									end as 'cantidadDiasServicio',
									func.cedula
								from 
									funcionarios as func
								inner join
									funcionarios_nombramientos as func_nomb 
										on(func.cedula = func_nomb.cedula)	
								inner join 
									nombramientos as nomb
										on(func_nomb.id_nombramiento = nomb.id)
								inner join
									puestos as p
											on(p.id = nomb.id_puesto)
								inner join
									servicios as s
											on(p.id_servicio = s.id)
								where 
									s.codigo = @idServicio
									and s.deleted_at is null
									and func.deleted_at is null
									and func_nomb.deleted_at is null
									and p.deleted_at is null
									and nomb.deleted_at is null
								group by nomb.fecha_fin, func.cedula, fecha_inicio) 
							as diasServicio
						on(diasServicio.cedula = f.cedula)
					group by f.cedula) as diasServicio
				on(f.cedula = diasServicio.cedula)
			inner join
				(select 
					f.cedula, 
					f.nombre as 'nombreFuncionario',
					case
						WHEN EXISTS (
									select 
										* 
										from (Select f.cedula, f.nombre,reportesFechas.fecha_inicio,reportesFechas.fecha_fin
													from 
														(select * from nombramientos as n
															where 
															n.deleted_at is null
															
															and

															@fechaSolicitadaInicio BETWEEN n.fecha_inicio AND n.fecha_fin 
															OR @fechaSolicitadaFinal  BETWEEN n.fecha_inicio AND n.fecha_fin 
															OR n.fecha_inicio BETWEEN @fechaSolicitadaInicio and @fechaSolicitadaFinal 
															OR n.fecha_fin BETWEEN @fechaSolicitadaInicio and @fechaSolicitadaFinal
														) as reportesFechas
													inner join 
														funcionarios_nombramientos as fn
															on(reportesFechas.id = fn.id_nombramiento)
													inner join 
														funcionarios as f
															on(f.cedula = fn.cedula)
													where
														f.deleted_at is null
														and f.deleted_at is null) as funcionariosNoDisponibles	
										where 
											funcionariosNoDisponibles.cedula = f.cedula)
							then
								0
							else
								1
					end as 'disponible'
						from 
							funcionarios as f) as disponibilidadFuncionarios
				on(disponibilidadFuncionarios.cedula = f.cedula)
			inner join 
				(SELECT 
					f.cedula, 
					SUM(diasServicio.cantidadDiasInstitucion) as 'cantidadDiasInstitucion'
						FROM
							funcionarios as f
						inner join	 
							(select 
									case
										when
											nomb.fecha_fin >= GETDATE() and nomb.fecha_inicio <= GETDATE()
												then
													SUM(DATEDIFF(DAY, nomb.fecha_inicio, GETDATE()))
										when
											nomb.fecha_fin >= GETDATE() and nomb.fecha_inicio > GETDATE()
												then
													SUM(0)
										when
											nomb.fecha_fin <= GETDATE()
												then
													SUM(DATEDIFF(DAY, nomb.fecha_inicio, nomb.fecha_fin))
									end as 'cantidadDiasInstitucion',
									func.cedula
								from 
									funcionarios as func
								inner join
									funcionarios_nombramientos as func_nomb 
										on(func.cedula = func_nomb.cedula)	
								inner join 
									nombramientos as nomb
										on(func_nomb.id_nombramiento = nomb.id)
								inner join
									puestos as p
											on(p.id = nomb.id_puesto)
								inner join
									servicios as s
											on(p.id_servicio = s.id)
								where
									func.deleted_at is null
									and func_nomb.deleted_at is null
									and nomb.deleted_at is null
									and p.deleted_at is null
									and s.deleted_at is null
								group by nomb.fecha_fin, func.cedula, fecha_inicio) 
							as diasServicio
						on(diasServicio.cedula = f.cedula)
					group by f.cedula) as diasInstitucion
				on(diasInstitucion.cedula = f.cedula)
			order by diasPuesto.cantidadDiasPuesto desc, diasServicio.cantidadDiasServicio desc, diasInstitucion.cantidadDiasInstitucion desc;
END;
