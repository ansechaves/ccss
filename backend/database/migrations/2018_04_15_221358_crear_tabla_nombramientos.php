<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaNombramientos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nombramientos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_puesto');
            $table->date('fecha_inicio')->nullable(false);
            $table->date('fecha_fin')->nullable(false);
            $table->integer('nombramiento_antiguo')->nullable(true);
            $table->unsignedInteger('numero_dias')->nullable(true)->default(0);
            $table->string('tiempo',50)->nullable(false);
            $table->string('tipo',30)->nullable(false);
            $table->string('sustituye',70)->nullable();
            $table->string('motivo',50)->nullable();
            $table->boolean('estado')->nullable(false);
            //$table->timestamps();
            $table->date('created_at')->default(Carbon::now());
            $table->date('updated_at')->default(Carbon::now());
            $table->date('deleted_at')->nullable(true);

            $table->foreign('id_puesto')->references('id')->on('puestos');
        });
        DB::statement('ALTER TABLE nombramientos ADD CONSTRAINT chk_nombramientos_rango_fechas CHECK(fecha_inicio < fecha_fin);'); //Condición no probada
        //Talvés requiera más constraints
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nombramientos');
    }
}
