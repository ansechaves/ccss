<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaHeAdicionales extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('he_adicionales', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_puesto');
            $table->unsignedInteger('id_he_mensual_servicio');
            $table->unsignedInteger('horas')->nullable(false)->default(0);
          //  $table->char('estado',2);
            $table->text('justificacion',300)->nullable();
            $table->text('retroalimentacion',300)->nullable();
            $table->unsignedInteger('monto_reservado')->nullable();
            $table->unsignedInteger('cantidad_empleados')->nullable(false)->default(0);

            //$table->timestamps();
            $table->date('created_at')->default(Carbon::now());
            $table->date('updated_at')->default(Carbon::now());
            $table->date('deleted_at')->nullable(true);

            $table->foreign('id_puesto')->references('id')->on('puestos');
            $table->foreign('id_he_mensual_servicio')->references('id')->on('he_mensual_servicios');
        });
      //  DB::statement("ALTER TABLE he_adicionales ADD CONSTRAINT chk_he_adicionales_estado CHECK(estado IN('PA','PP','AP','AA','A','D'));");
        /************************************************************
         * PA: Pendiente-Administración, PP: Pendiente-Presupuesto  *
         * AP: Aprobado-Presupuesto, AA: Aprobado-Administracion    *
         * A: Aprobado, D: Denegado                                 *
         ************************************************************/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('he_adicionales');
    }
}
