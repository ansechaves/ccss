<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaTelefonos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('telefonos', function (Blueprint $table) {
            //debe de cambiarse mas adelante a llave compuesta
            $table->increments('id');
            $table->string('cedula',30);
            $table->string('numero',20)->nullable(false)->unique();
            //$table->timestamps();
            $table->date('created_at')->default(Carbon::now());
            $table->date('updated_at')->default(Carbon::now());
            $table->date('deleted_at')->nullable(true);

            $table->foreign('cedula')->references('cedula')->on('funcionarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('telefonos');
    }
}
