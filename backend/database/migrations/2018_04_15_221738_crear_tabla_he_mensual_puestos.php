<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaHeMensualPuestos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('he_mensual_puestos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_he_mensual_servicio');
            $table->unsignedInteger('id_puesto');
            $table->text('justificacion',300)->nullable();
            $table->unsignedInteger('horas_autorizadas')->nullable();
            $table->unsignedInteger('horas_solicitadas')->nullable(false);
            $table->unsignedInteger('monto_reservado')->nullable();
           // $table->char('estado',2)->nullable(false);
            $table->text('retroalimentacion',300)->nullable();
            $table->unsignedInteger('cantidad_empleados')->nullable(false)->default(0);
            //$table->timestamps();
            $table->date('created_at')->default(Carbon::now());
            $table->date('updated_at')->default(Carbon::now());
            $table->date('deleted_at')->nullable(true);

            $table->foreign('id_he_mensual_servicio')->references('id')->on('he_mensual_servicios');
            $table->foreign('id_puesto')->references('id')->on('puestos');
        });
        //constraint de estado (check)

        DB::statement('ALTER TABLE he_mensual_puestos ADD CONSTRAINT chk_he_mensual_puestos_horas_solicitadas CHECK(horas_solicitadas > 0);'); //Condición sin probar
      //  DB::statement("ALTER TABLE he_mensual_puestos ADD CONSTRAINT chk_he_mensual_puestos_estado CHECK(estado IN('PA','PP','AP','AA','A','D'));");
        /************************************************************
         * PA: Pendiente-Administración, PP: Pendiente-Presupuesto  *
         * AP: Aprobado-Presupuesto, AA: Aprobado-Administracion    *
         * A: Aprobado, D: Denegado                                 *
         ************************************************************/
        //DB::statement('ALTER TABLE he_mensual_puestos ADD CONSTRAINT chk_he_mensual_puestos_cantidad_empleados CHECK(cantidad_empleados > 0);');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('he_mensual_puestos');
    }
}
