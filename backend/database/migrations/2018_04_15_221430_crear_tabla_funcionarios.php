<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaFuncionarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('funcionarios', function (Blueprint $table) {
//            $table->increment('id_funcionario');
            $table->string('cedula',30)->primary();
            $table->string('nombre',70)->nullable(false);
            $table->string('apellido1',70)->nullable(false);
            $table->string('apellido2',70)->nullable(true);
            $table->date('fecha_nacimiento')->nullable(false);
            $table->date('fecha_ingreso')->nullable(false);
            $table->unsignedInteger('numero_tarjeta')->nullable(false)->unique();
            $table->string('correo',100)->nullable(false)->unique();
            //realizar constraint sobre formato
            $table->date('created_at')->default(Carbon::now());
            $table->date('updated_at')->default(Carbon::now());
            $table->date('deleted_at')->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('funcionarios');
    }
}
