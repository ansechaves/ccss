<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaPuestos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('puestos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_servicio');
            $table->string('nombre',70)->nullable(false);
            $table->string('codigo',20)->nullable(false)->unique();
            $table->unsignedInteger('promedio_salarial')->nullable(false);
            $table->string('descripcion',150)->nullable();
            //$table->timestamps();
            $table->date('created_at')->default(Carbon::now());
            $table->date('updated_at')->default(Carbon::now());
            $table->date('deleted_at')->nullable(true);
            
            $table->foreign('id_servicio')->references('id')->on('servicios');
            //DB::statement('ALTER TABLE [tabla] ADD CONSTRAINT [nombreConstraint] CHECK ([condicion]);');
        });
        DB::statement('ALTER TABLE puestos ADD CONSTRAINT chk_puestos_promedio_salarial CHECK(promedio_salarial > 0);');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('puestos');
    }
}
