<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cedula',30);
            $table->string('nombre_usuario')->nullable(false)->unique();
            $table->string('password')->nullable(false);
            $table->rememberToken();
            //Falta definir los valores a tomar y su significado
            //$table->unsignedInteger('nivel_acceso');
//            $table->timestamps();
            $table->date('created_at')->default(Carbon::now());
            $table->date('updated_at')->default(Carbon::now());
            $table->date('deleted_at')->nullable(true);


            $table->foreign('cedula')->references('cedula')->on('funcionarios');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
