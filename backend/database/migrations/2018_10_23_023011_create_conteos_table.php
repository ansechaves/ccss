<?php


use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConteosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conteos', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('horas')->nullable(false);
            $table->string('motivo', 100)->nullable(false);
            $table->string('justificacion', 500)->nullable(false);
            $table->string('doctor', 100)->nullable(true);
            $table->unsignedInteger('numeroBoleta')->nullable(true);
            $table->string('Id_sustituto', 30)->nullable(true);
            $table->date('fecha')->default(Carbon::now());
            $table->unsignedInteger('id_Puesto')->nuallable(false);
            $table->string('id_PersonaAusente', 30)->nullable(true);

            $table->date('created_at')->default(Carbon::now());
            $table->date('updated_at')->default(Carbon::now());
            $table->date('deleted_at')->nullable(true);


            $table->foreign('id_Puesto')->references('id')->on('puestos');
            $table->foreign('Id_sustituto')->references('cedula')->on('funcionarios');
            $table->foreign('id_PersonaAusente')->references('cedula')->on('funcionarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conteos');
    }
}
