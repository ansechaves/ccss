<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaFuncionariosNombramientos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('funcionarios_nombramientos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cedula',30);
            $table->unsignedInteger('id_nombramiento');
            $table->date('fecha_nombramiento')->nullable(false);
            //$table->timestamps();
            $table->date('created_at')->default(Carbon::now());
            $table->date('updated_at')->default(Carbon::now());
            $table->date('deleted_at')->nullable(true);

            //Laravel no soporta llaves compuestas, así que dejo la tabla sin llave primaria
            $table->foreign('cedula')->references('cedula')->on('funcionarios');
            $table->foreign('id_nombramiento')->references('id')->on('nombramientos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('funcionarios_nombramientos');
    }
}
