<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaHeDiariasFuncionariosNombramientos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('he_diarias_funcionarios_nombramientos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_funcionario_nombramiento');
            $table->unsignedInteger('id_he_diaria');
            $table->unsignedInteger('horas')->nullable(false);
            $table->date('created_at')->default(Carbon::now());
            $table->date('updated_at')->default(Carbon::now());
            $table->date('deleted_at')->nullable(true);

            $table->foreign('id_funcionario_nombramiento')->references('id')->on('funcionarios_nombramientos');
            $table->foreign('id_he_diaria')->references('id')->on('he_diarias');
        });

        DB::statement('ALTER TABLE he_diarias_funcionarios_nombramientos ADD CONSTRAINT chk_he_diarias_funcionario_nombramiento CHECK(horas >= 0);');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('he_diarias_funcionarios_nombramientos', function (Blueprint $table) {
            Schema::dropIfExists('he_diarias_funcionarios_nombramientos');
        });
    }
}
