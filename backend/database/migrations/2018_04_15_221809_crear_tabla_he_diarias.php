<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaHeDiarias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('he_diarias', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_puesto'); //id_puesto nunca será null a diferencia de id_he_mensual_puesto
            $table->date('fecha')->nullable($value=false)->default(Carbon::now());
            //$table->unsignedInteger('horas')->nullable(false)->default(0);
            //$table->timestamps();
            $table->date('created_at')->default(Carbon::now());
            $table->date('updated_at')->default(Carbon::now());
            $table->date('deleted_at')->nullable(true);

            $table->foreign('id_puesto')->references('id')->on('puestos'); //Cambio de llave foránea para mayor control en las conexiones de los registros
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('he_diarias');
    }
}
