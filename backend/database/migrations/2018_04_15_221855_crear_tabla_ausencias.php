<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaAusencias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ausencias', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_he_mensual_puesto');
            $table->char('tipo',4)->nullable($value=false); //Valor máximo de caracteres 4
            $table->boolean('sustituto')->nullable(false);
            //$table->timestamps();
            $table->date('created_at')->default(Carbon::now());
            $table->date('updated_at')->default(Carbon::now());
            $table->date('deleted_at')->nullable(true);

            $table->foreign('id_he_mensual_puesto')->references('id')->on('he_mensual_puestos');
        });
        DB::statement("ALTER TABLE ausencias ADD CONSTRAINT chk_ausencias_tipo CHECK (tipo IN ('pcgs','psgs','inca'));");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ausencias');
    }
}
