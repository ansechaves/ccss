<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaHeAnuales extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('he_anuales', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_puesto');
            $table->unsignedInteger('horas')->nullable(false);
            $table->unsignedInteger('anyo')->nullable(false);
            //$table->timestamps();
            $table->date('created_at')->default(Carbon::now());
            $table->date('updated_at')->default(Carbon::now());
            $table->date('deleted_at')->nullable(true);

            $table->foreign('id_puesto')->references('id')->on('puestos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('he_anuales');
    }
}
