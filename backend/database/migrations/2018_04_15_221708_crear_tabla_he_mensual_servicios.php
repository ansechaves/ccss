<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaHeMensualServicios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('he_mensual_servicios', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_servicio');
            $table->text('observaciones')->nullable();
            $table->text('retroalimentacion')->nullable();
            $table->string('jefe_servicio',70)->nullable(false);
            $table->date('fecha')->nullable(false);
            //$table->timestamps();
            $table->char('estado',2)->nullable(false);
            $table->date('created_at')->default(Carbon::now());
            $table->date('updated_at')->default(Carbon::now());
            $table->date('deleted_at')->nullable(true);

            $table->foreign('id_servicio')->references('id')->on('servicios');
        });
        DB::statement("ALTER TABLE he_mensual_servicios ADD CONSTRAINT chk_he_mensual_servicios_estado CHECK(estado IN('PA','PP','AP','AA','A','D'));");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('he_mensual_servicios');
    }
}
