<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaAccesoUsuarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acceso_usuarios', function (Blueprint $table) {
            //Tabla sin llave primaria (no tiene sentido agregar una)
            $table->unsignedInteger('id_usuario')->unique();
            $table->string('STEPI',50)->nullable(false)->default("0"); // <0: tiene permiso, 0: lo contrario
            $table->unsignedInteger('SER')->nullable(false)->default(0);
            //$table->timestamps();
            $table->date('created_at')->default(Carbon::now());
            $table->date('updated_at')->default(Carbon::now());
            $table->date('deleted_at')->nullable(true);

            $table->foreign('id_usuario')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acceso_usuarios');
    }
}
