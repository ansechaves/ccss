<?php

use App\User;
use App\Funcionario;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Faker\Factory as Faker;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */

    public function run()
    {
        $this->borrarTablas();
        //crea la base de datos. Utiliza la librería Faker, donde utiliza datos fake para llenarlo.
        $this->insertarServicios();
        $this->insertarPuestos();
        $this->insertarHEAnuales();
        $this->insertarNombramientos();
        $this->insertarFuncionarios();
        $this->insertarFuncionariosNombramientos();
        $this->insertarUsuarios();
        $this->insertarAccesoUsuarios();
        $this->insertarHEMensualServicios();
        $this->insertarHEMensualPuestos();
        $this->insertarHeAdicionales();

//        Model::unguard();
//        DB::table('users')->delete();
//        DB::table('funcionarios')->delete();
//        $Funcionarios = array(
//            ['cedula' => '200000001', 'nombre' => 'Nombre',
//                'apellido1' => 'Apellido1','apellido2' => 'Apellido2',
//                'fecha_nacimiento' => '1990/01/01 23:28:34', 'fecha_ingreso' => '2018/04/02 20:28:34',
//                'numero_tarjeta' => 123456, 'correo' => 'email@email.com'],
//        );
//
//        $users = array(
//            ['nombre_usuario' => 'test', 'password' => Hash::make('secret'),
//                'cedula'=>'200000001','nivel_acceso'=>1],
//        );
//
//        // Loop through each funcionario and user above and create the record for them in the database
//        foreach ($Funcionarios as $Funcionario)
//        {
//            Funcionario::create($Funcionario);
//        }
//        foreach ($users as $user)
//        {
//            User::create($user);
//        }
//        Model::reguard();
//        $this->call(UsersTableSeeder::class);
    }

    public function borrarTablas()
    {
        DB::table('he_mensual_puestos')->delete();
        DB::table('he_mensual_servicios')->delete();
        DB::table('users')->delete();
        DB::table('funcionarios_nombramientos')->delete();
        DB::table('funcionarios')->delete();
        DB::table('nombramientos')->delete();
        DB::table('he_anuales')->delete();
        DB::table('puestos')->delete();
        DB::table('servicios')->delete();
    }

    public function insertarServicios(){
        $faker = Faker::create();
        for ($i=1; $i < 40; $i++) {
            \DB::table('servicios')->insert(array(
                'nombre' => 'Servicio '.$i,
                'codigo' => $i,
                'descripcion' => 'Descripcion Servicio '.$i
            ));
        }
    }

    public function insertarPuestos(){
        $faker = Faker::create();
        for ($i=1; $i < 40; $i++) {
            \DB::table('puestos')->insert(array(
                'id_servicio'=>$i,
                'nombre' => $faker->name,
                'codigo' => $i,
                'promedio_salarial'=> 100,
                'descripcion' => $faker->company
            ));
        }
    }

    public function insertarHEAnuales(){
        $faker = Faker::create();
        for ($i=1; $i < 40; $i++) {
            \DB::table('he_anuales')->insert(array(
                'id_puesto'=>$i,
                'horas' => 20,
                'anyo' => 2018
            ));
        }
    }
//voy por aquí
    public function insertarNombramientos(){
        $faker = Faker::create();
        for ($i=1; $i < 40; $i++) {
            \DB::table('nombramientos')->insert(array(
                'id_puesto'=>$i,
                'fecha_inicio' => '2018-04-24',
                'fecha_fin' => '2019-04-26',
                'numero_dias' => 40,
                'tiempo' => 'tiempo',
                'tipo'=> 'tipo',
                'sustituye'=> $faker->name,
                'motivo'=> $faker->email,
                 'estado'=> true
            ));
        }
    }


    public function insertarFuncionarios(){
        $faker = Faker::create();
        for ($i=1; $i < 40; $i++) {
            \DB::table('funcionarios')->insert(array(
                'cedula' => $i,
                'nombre' => $faker->name,
                'apellido1' => $faker->lastName,
                'apellido2' => $faker->lastName,
                'fecha_nacimiento' => '2010-04-24',
                'fecha_ingreso'=> '2018-04-20',
                'numero_tarjeta' => $i,
                'correo' => $faker->email
            ));
        }
    }

    public function insertarFuncionariosNombramientos(){
        $faker = Faker::create();
        for ($i=1; $i < 40; $i++) {
            \DB::table('funcionarios_nombramientos')->insert(array(
                'cedula' => $i,
                'id_nombramiento' => (($i-1)%10)+1,
                'cedula' => $i,
                'fecha_nombramiento' => $faker->date()
            ));
        }
    }

    public function insertarUsuarios(){
        $faker = Faker::create();
        for ($i=1; $i < 40; $i++) {
            \DB::table('users')->insert(array(
                'cedula' => $i,
                'nombre_usuario' =>$i,
                'password' =>  Hash::make($i)
            ));
        }
    }
    public function insertarAccesoUsuarios(){
        $faker = Faker::create();
        for ($i=1; $i < 7; $i++) {
            \DB::table('acceso_usuarios')->insert(array(
                'id_usuario' => $i,
                'STEPI' =>$i,
                'SER' =>$i
            ));
        }
    }

    public function insertarHEMensualServicios(){
        $faker = Faker::create();
        for ($i=1; $i < 40; $i++) {
            \DB::table('he_mensual_servicios')->insert(array(
                'id_servicio' => $i,
                'observaciones' => 'observaciones fake',
                'jefe_servicio' => $faker->name,
                'fecha' => $faker->date(),
                'estado'=> 'PA',
            ));
        }
    }

    public function insertarHEMensualPuestos(){
        for ($i=1; $i < 40; $i++) {
            \DB::table('he_mensual_puestos')->insert(array(
                'id_he_mensual_servicio'=> $i,
                'id_puesto' => $i,
                'justificacion' => 'justificacion fake',
                'horas_autorizadas' => 12,
                'horas_solicitadas' => 15,
                'monto_reservado' => 100,
                'retroalimentacion' => 'retroalimentacion fake',
                'cantidad_empleados' => 4,
            ));
        }
    }

    public function insertarHEAdicionales(){
        for ($i=1; $i < 40; $i++) {
            \DB::table('he_adicionales')->insert(array(
                'id_puesto' => $i,
                'id_he_mensual_servicio' =>  $i,
                'horas' => 19,
                'justificacion' => 'justificacion',
                'retroalimentacion'=> 'retroalimentacion',
                'monto_reservado'=> 100,
                'cantidad_empleados'=>10
            ));
        }
    }


}
