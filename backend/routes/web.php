<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('vista','NombramientoController@index');



/*Funcion que inhabilita un nombramiento determinado*/
Route::get('inhabilitarNombramiento/{idNombramientoInhabilitado}','NombramientoController@InhabilitarNombramiento');

/*Muestra los elegibles correspondientes a un puesto determinado*/
Route::get('puestoPorElegible/{nombrePuestoBuscado}','NombramientoController@buscarPuestoPorElegible');
Route::get('ver', 'NombramientoController@index');
Route::get('mod/{id}/{idPuesto}/{fechaInicio}/{fechaFin}/{tiempo}/{tipo}/{sustituye}/{motivo}', 'NombramientoController@modificarNombramiento');
