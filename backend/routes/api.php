<?php

use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function ($router) {
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
});
/*Funcion que inhabilita un nombramiento determinado*/
/*Muestra los elegibles correspondientes a un puesto determinado*/
Route::get('puestoPorElegible/{nombrePuestoBuscado}', 'NombramientoController@buscarPuestoPorElegible');

//Rutas Vista realizar
Route::get('realizarHorasExtra/{idUsuario}', 'HorasExtraController@mostrarRealizarHorasExtra');
Route::post('realizarHorasExtra/insertar', 'HorasExtraController@insertarRealizarHorasExtra');
Route::get('realizarHorasExtra/prueba/{idUsuario}', 'HorasExtraController@realizaSolicitudEnMesActual');
//Rutas Vista Listar administración
Route::get('listarAdministracion', 'HorasExtraController@listarServiciosSolicitaHorasExtraAdministracion');
Route::get('listarAdministracion/administracion/{id_HE_Mensual_Servicio}', 'HorasExtraController@mostrarInformacionAdministracion');
Route::put('listarAdministracion/administracion/aprobar', 'HorasExtraController@actualizarAprobarSolicitud');
Route::put('listarAdministracion/administracion/rechazar', 'HorasExtraController@actualizarRechazarSolicitud');
//Vista Presupuesto
Route::get('listarPresupuesto/presupuesto/{id_HE_Mensual_Servicicio}', 'HorasExtraController@muestraInformacionPresupuesto');
Route::put('presupuesto/actualizar', 'HorasExtraController@actualizarPresupuesto');
//Vista Listar Presupuesto
Route::get('listarPresupuesto', 'HorasExtraController@listarServiciosSolicitaHorasExtraPresupuesto');

//Vista Aprobación
Route::get('aprobacion', 'HorasExtraController@listarServiciosSolicitaHorasExtraAprobacion');
Route::put('aprobacion/actualizar', 'HorasExtraController@actualizarEstadoAprobacion');
//Vista correcciones
Route::get('correcciones/{idUsuario}', 'HorasExtraController@muestraCorrecciones');
Route::put('correcciones/actualizar', 'HorasExtraController@actualizarCorrecciones');


//realizar horas adicionales.... Vista realizar
Route::get('realizarHorasAdicionales/{idUsuario}', 'HorasAdicionalesController@mostrarRealizarHorasAdicionales');
Route::post('realizarHorasAdicionales/insertar', 'HorasAdicionalesController@insertarRealizarHorasAdicionales');
//Rutas Vistar listar administración adicionales..
Route::get('listarAdministracion/horasAdicionales', 'HorasAdicionalesController@mostrarListaServiciosSolicitanHorasAdicionales');
Route::get('listarAdministracion/administracion/horasAdicionales/{id_HE_Mensual_Servicio}', 'HorasAdicionalesController@mostrarInformacionAdministracionAdicional');
Route::put('listarAdministracion/administracion/adicionales/actualizar', 'HorasAdicionalesController@actualizarRechazarOAprobarSolicitudAdicionales');
//Vista presupuesto
Route::get('listarPresupuesto/horasAdicionales', 'HorasAdicionalesController@mostrarListaServiciosSolicitanHorasAdicionalesPresupuesto');
Route::get('presupuesto/horasAdicionales/{id_HE_Mensual_Servicio}', 'HorasAdicionalesController@mostrarInformacionPresupuestoAdicional');
Route::put('presupuesto/horasAdicionales/actualizar', 'HorasAdicionalesController@actualizarPresupuestoAdicionales');
//Vista listar presupuesto
Route::get('presupuesto/horasAdicionales', 'HorasAdicionalesController@listarServiciosSolicitaHorasAdicionalesPresupuesto');
//Vista Aprobación adicionales
Route::get('aprobacion/horasAdicionales', 'HorasAdicionalesController@listarServiciosSolicitaHorasAdicionalesAprobacion');
Route::put('aprobacion/horasAdicionales/actualizar', 'HorasAdicionalesController@actualizarEstadoAprobacionAdicionales');
//Vista correcciones
Route::get('correcciones/horasAdicionales/{idUsuario}', 'HorasAdicionalesController@muestraCorreccionesAdicionales');
Route::put('correcciones/horasAdicionales/actualizar', 'HorasAdicionalesController@actualizarCorreccionesAdicionales');

//Route::get('pruebaEloquent/{idUsuario}', 'HorasExtraController@mostrarNombreServicioRealizar');


//rutas para probar uno por uno. El general está en el apiResource realizarHorasExtra.
Route::get('realizarHorasExtra/muestraListaPuestos/{cedula}', 'RealizarSolicitudHorasExtraController@muetraListaPuestosDeServiciosDeUsuarios');
Route::get('realizarHorasExtra/muestraNombreServicio/{cedula}', 'RealizarSolicitudHorasExtraController@muestraNombreServicio');
Route::get('realizarHorasExtra/muestraHorasAutorizadasMesAnterior/{cedula}', 'RealizarSolicitudHorasExtraController@prueba');
Route::get('realizarHorasExtra/muestraTotalHorasPorCadaPuesto/{idPuesto}', 'RealizarSolicitudHorasExtraController@muestraTotalHorasAnualesPorCadaPuesto');
Route::get('realizarHorasExtra/muestraPromedioHorasSolicitadas/{idPuesto}', 'RealizarSolicitudHorasExtraController@muestraPromedioHorasSolicitadasMes');

Route::get('realizarHorasExtra/administracionHorasExtra/{idUsuario}', 'RealizarSolicitudHorasExtraController@administracionHorasExtra');

//Funcionarios
Route::get('funcionarios/consultar', 'FuncionariosController@consultar');
Route::post('funcionarios/insertar', 'FuncionariosController@insertar');
Route::delete('funcionarios/borrar/{cedula}', 'FuncionariosController@borrar');
Route::put('funcionarios/editar', 'FuncionariosController@editar');

Route::get('funcionariosPorPuesto/{codigoPuesto}', 'FuncionariosController@informePorPuesto');
Route::get('nombreFuncionario', 'FuncionariosController@obtenerNombreFuncionario');
Route::get('cedulaFuncionarios', 'FuncionariosController@cedulaFuncionarios');


//Route::get('realizarHorasExtra/muestraHorasAutorizadasMesAnterior/cedula', 'RealizarSolicitudHorasExtraController@prueba');

//Route::post('realizarHorasExtra/insertar/{id_puesto}/{}', 'RealizarSolicitudHorasExtraController@guardar');
//Route::post('realizarHorasExtra/{}{}{}', 'RealizarSolicitudHorasExtraController@crearHE_Mensual_Puestos');

// Rutas nombramiento.
Route::get('mostrarNombramientos', 'NombramientoController@index');
Route::get('mostrarFuncionarioNombre/{nombre}', 'FuncionariosController@funcionarioPorNombre');
Route::get('mostrarFuncionarioNumero/{numero}', 'FuncionariosController@funcionarioPorNumeroTarjeta');
Route::get('mostrarFuncionarioCedula/{cedula}', 'FuncionariosController@funcionarioPorCedula');
Route::get('mostrarFuncionarioNombramientos/{id}', 'NombramientoController@funcionarioNombramiento');
Route::get('mostrarNombramientosServicio/{id}', 'NombramientoController@nombramientosPorServicio');
Route::put('editarNombramiento/{id}', 'NombramientoController@editarNombramiento');
Route::post('crearNombramiento', 'NombramientoController@crearNombramiento');
Route::delete('eliminarNombramiento/{id}', 'NombramientoController@inhabilitarNombramiento');
//Route::post('crearNombramiento','NombramientoController@crearNombramiento');

Route::get('obtenerPuestos', 'NombramientoController@obtenerListaPuestos');
Route::get('obtenerFuncionarios', 'NombramientoController@obtenerListaCedulasFuncionarios');
Route::get('obtenerNombreFuncionarios', 'NombramientoController@obtenerListaNombresFuncionarios');
Route::get('obtenerCodigoPuestos', 'NombramientoController@obtenerListaCodigoPuestosNombrar');

//Servicios
Route::get('mostrarServicios', 'ServicioController@mostrarServicios');
Route::put('modificarServicios/{id}', 'ServicioController@modificarServicios');
Route::delete('/eliminarServicios/{id}', 'ServicioController@eliminarServicios');
Route::post('crearServicios', 'ServicioController@crearServicios');
Route::get('obtenerFuncionarios', 'FuncionariosController@getFuncionarios');
Route::get('obtenerListaPuestosPersona/{cedulaBuscada}', 'NombramientoController@llenarListaPuestosPorPersona');
Route::get('buscarInformacionPuesto/{cedulaBuscada}/{idPuesto}', 'NombramientoController@buscarInformacionPuesto');

Route::get('buscarFuncionarioNombramiento/{cedula}/{fecha_fin}', 'NombramientoController@buscarFuncionarioNombramiento');
//Route::get('buscarFuncionarioNombramiento/{cedula}', 'NombramientoController@buscarFuncionarioNombramiento');

//////API de pruebas
Route::get('desarrollo/{id}', 'NombramientoController@prueba');

Route::get('puestosPorServicio/{codigoServicio}', 'ServicioController@informePorServicio');
Route::get('/obtenerServiciosPuestos', 'ServicioController@obtenerServiciosPuestos');
Route::get('obtenerNombreYCodigoServicios', 'ServicioController@obtenerNombreYCodigoServicio');

//Puestos
Route::get('/obtenerPuestos', 'PuestosController@obtenerListaPuestos');
Route::get('/obtenerInfoPuesto/{id}', 'PuestosController@obtenerInfoPuesto');
Route::post('/crearPuesto', 'PuestosController@crearPuesto');
Route::post('/editarPuesto/{id}', 'PuestosController@editarPuesto');
Route::delete('/eliminarPuesto/{id}', 'PuestosController@eliminarPuesto');
Route::get('obtenerNombreYCodigoPuestos', 'PuestosController@obtenerNombreYCodigoPuesto');


//Usuarios
Route::get('usuarios/obtener', 'UsersController@obtener');
Route::post('usuarios/insertar', 'UsersController@insertar');
Route::put('usuarios/modificar', 'UsersController@modificar');
Route::get('usuarios/funcionarios', 'UsersController@funcionarios');
Route::delete('usuarios/eliminar/{id}', 'UsersController@eliminar');


//Conteo
Route::post('conteo/insertar', 'ConteosController@insertar');
Route::delete('conteo/delete', 'ConteosController@destroy');
Route::get('conteo/porpuesto/{id}', 'ConteosController@mostarPorPuesto');

// Reportes
Route::get('conteo/reportes/porcentajeMotivoPorServicioPorMes/{idServicio}/{mes}/{annio}', 'ConteosController@porcentajeMotivoPorServicioPorMes');
Route::get('conteo/reportes/horasUsadasPorServicio/{idServ}/{mes}/{annio}', 'ConteosController@HorasUsadasPorServicio');
Route::get('conteo/reportes/horasAprobadas/{mes}/{annio}/{idServ}', 'ConteosController@horasAprobadas');
Route::get('conteo/reportes/ausenciasPorPersona/{idPersona}','ConteosController@ausenciasPorPersona');
Route::get('conteo/reportes/HorasExtraPorPersona/{idPersona}','ConteosController@HorasExtraPorPersona');

//Route::get('/conteo/obtener/{id}','ConteoHorasExtraController@muestraListaDeFuncionarios');
Route::get('conteo/prueba/obtenerServicio/{cedula}', 'ConteoHorasExtraController@obtenerIDServicioPorCedula');
Route::get('conteo/prueba/obtenerServicio2/{cedula}', 'ConteoHorasExtraController@obtenerIDServicioPorCedula_2');
//Route::get('conteo/prueba/muestraListaDeFuncionarios/{id}','ConteoHorasExtraController@muestraListaDeFuncionarios');
Route::get('conteo/prueba/ListaDeFuncionarios/{id}', 'ConteoHorasExtraController@obtenerListaDeFuncionariosPorServicioPorCedula');
Route::get('conteo/prueba/ListaDeFuncionarios2/{id}', 'ConteoHorasExtraController@obtenerListaDeFuncionariosPorServicioPorCedula_2');
Route::get('conteo/prueba/ListaRegistroConteo/{cedula}', 'ConteoHorasExtraController@obtenerListaRegistroConteoHorasExtraPorCedula');
Route::post('conteo/prueba/insertarRegistro', 'ConteoHorasExtraController@insertarHorasExtraFuncionario');
Route::put('conteo/prueba/actualizarRegistro', 'ConteoHorasExtraController@actualizarHorasExtraFuncionario');
Route::put('conteo/prueba/eliminarRegistro', 'ConteoHorasExtraController@eliminarHorasExtraFuncionario');

// Elegibles
Route::get('/obtenerListaElegibles/{idPuesto}/{fechaInicial}/{fechaFinal}', 'ElegiblesController@obtenerListaElegibles');
Route::get('/obtenerListaNombresFuncionarios', 'ElegiblesController@obtenerListaNombresFuncionarios');
Route::get('/obtenerServicioFuncionario/{cedula}', 'ElegiblesController@obtenerServicioFuncionario');
Route::get('/obtenerPuestosSegunServicio/{idServicio}', 'ElegiblesController@obtenerPuestosSegunServicio');
Route::get('/obtenerServicios', 'ElegiblesController@obtenerServicios');
Route::get('/elegibles/{idPuesto}','ElegiblesController@obtenerElegiblesPasivosActivos');

//exportar e importar datos Excel
Route::get('/api/import', 'ExcelController@Import');
Route::post('/import', 'ExcelController@Import');